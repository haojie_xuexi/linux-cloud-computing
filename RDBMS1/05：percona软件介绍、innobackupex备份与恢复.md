## 05:percona软件介绍、innobackupex备份与恢复



### 1 案例1：数据完全备份与恢复

#### 1.1 问题

- 安装percona软件包
- 备份所有数据到/allbak目录下
- 搭建新的数据库服务器，使用备份文件恢复数据
- 验证数据恢复



#### 1.2 步骤

实现此案例需要按照如下步骤进行。

##### 步骤一：安装XtraBackup软件包

###### 1）安装软件

```shell
[root@host50 ~]# rpm -ivh libev-4.15-1.el6.rf.x86_64.rpm
[root@host50 ~]# yum -y  install percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm
警告：percona-xtrabackup-24-2.4.6-2.el7.x86_64.rpm: 头V4 DSA/SHA1 Signature, 密钥 ID cd2efd2a: NOKEY
准备中...                          ################################# [100%]
正在升级/安装...  
1:percona-xtrabackup-24-2.4.6-2.el7################################# [ 33%]   
2:percona-xtrabackup-test-24-2.4.6-################################# [ 67%]   
3:percona-xtrabackup-24-debuginfo-2################################# [100%]
```

###### 2）确认安装的主要程序/脚本

```shell
[root@host50 ~]# rpm -qa  | grep -i percona
percona-xtrabackup-24-2.4.7-1.el7.x86_64
[root@host50 ~]# rpm -ql percona-xtrabackup-24
/usr/bin/innobackupex
/usr/bin/xbcloud
/usr/bin/xbcloud_osenv
/usr/bin/xbcrypt
/usr/bin/xbstream
/usr/bin/xtrabackup
/usr/share/doc/percona-xtrabackup-24-2.4.7
/usr/share/doc/percona-xtrabackup-24-2.4.7/COPYING
/usr/share/man/man1/innobackupex.1.gz
/usr/share/man/man1/xbcrypt.1.gz/usr/share/man/man1/xbstream.1.gz
/usr/share/man/man1/xtrabackup.1.gz
[root@host50 ~]#
[root@host50 ~]# innobackupex --help  //查看简单帮助
[root@host50 ~]#[root@host50 ~]# man  innobackupex //查看详细帮助
/usr/share/man/man1/xtrabackup.1.gz
```

##### 步骤二：备份所有数据到/allbak目录下

###### 1）备份所有数据

```shell
[root@host50 ~]# innobackupex --user root --password 123456 /allbak --no-timestamp //执行备份命令
170425 11:05:44 innobackupex: Starting the backup operation

IMPORTANT: Please check that the backup run completes successfully.           
		At the end of a successful backup run innobackupex           
		prints "completed OK!".
Unrecognized character \x01; marked by <-- HERE after <-- HERE near column 1 at - line 1374.
170425 11:05:45 Connecting to MySQL server host: localhost, user: root, password: set, port: not set,socket: not set
Using server version 5.7.17
innobackupex version 2.4.6 based on MySQL server 5.7.13 Linux (x86_64) (revision id: 8ec05b7)
xtrabackup: uses posix_fadvise().
xtrabackup: cd to /var/lib/mysql
xtrabackup: open files limit requested 0, set to 1024
xtrabackup: using the following InnoDB configuration:
xtrabackup:   innodb_data_home_dir = .
xtrabackup:   innodb_data_file_path = ibdata1:12M:autoextend
xtrabackup:   innodb_log_group_home_dir = ./
xtrabackup:   innodb_log_files_in_group = 2
xtrabackup:   innodb_log_file_size = 50331648
InnoDB: Number of pools: 
1170425 11:05:45 >> log scanned up to (2543893)
xtrabackup: Generating a list of tablespaces
InnoDB: Allocated tablespace ID 2 for mysql/plugin, old maximum was 0
170425 11:05:45 [01] Copying ./ibdata1 to /backup/ibdata1170425 11:05:45 [01]        ...done
170425 11:05:46 [01] Copying ./mysql/plugin.ibd to /backup/mysql/plugin.ibd
170425 11:05:46 [01]        ...done
170425 11:05:46 [01] Copying ./mysql/servers.ibd to /backup/mysql/servers.ibd
170425 11:05:46 [01]        ...done
170425 11:05:46 [01] Copying ./mysql/help_topic.ibd to /backup/mysql/help_topic.ibd
170425 11:05:46 [01]        ...done
170425 11:05:46 >> log scanned up to (2543893)
.. ..
170425 11:06:00 [01] Copying ./sys/x@0024waits_global_by_latency.frm to
/backup/sys/x@0024waits_global_by_latency.frm
170425 11:06:00 [01]        ...done
170425 11:06:00 [01] Copying ./sys/session_ssl_status.frm to /backup/sys/session_ssl_status.frm
170425 11:06:00 [01]        ...done
170425 11:06:00 [01] Copying ./db1/db.opt to /backup/db1/db.opt
170425 11:06:00 [01]        ...done
170425 11:06:00 [01] Copying ./db1/tb1.frm to /backup/db1/tb1.frm
170425 11:06:00 [01]        ...done
170425 11:06:00 Finished backing up non-InnoDB tables and files
170425 11:06:00 Executing FLUSH NO_WRITE_TO_BINLOG ENGINE LOGS...xtrabackup: The latest check point (for incremental): '2543884'xtrabackup: Stopping log copying thread..
170425 11:06:00 >> log scanned up to (2543893)
170425 11:06:00 Executing UNLOCK TABLES
170425 11:06:00 All tables unlocked
170425 11:06:00 [00] Copying ib_buffer_pool to /backup/ib_buffer_pool
170425 11:06:00 [00]        ...done
170425 11:06:00 Backup created in directory '/backup/'
170425 11:06:00 [00] Writing backup-my.cnf
170425 11:06:00 [00]        ...done
170425 11:06:00 [00] Writing xtrabackup_info
170425 11:06:00 [00]        ...donextrabackup: Transaction log of lsn (2543884) to (2543893) was copied.
170425 11:06:01 completed OK
```

###### 2) 确认备份好的文件数据：

```shell
[root@host50 ~]# ls /allbakbackup-my.cnf  
ib_buffer_pool  mysql      sys                   xtrabackup_info
db1  ibdata1      performance_schema  xtrabackup_checkpoints  xtrabackup_logfile
```

###### 3）把备份文件传递给 目标服务器51

```shell
[root@host50 ~]#
[root@host50 ~]# scp -r /allbak root@192.168.4.51:/root/
[root@host50 ~]#
```

步骤三：在51主机，使用备份文件恢复数据

1）安装软件包，提供恢复命令

```shell
[root@host51 ~]#  rpm -ivh  libev-4.15-1.el6.rf.x86_64.rpm
[root@host51 ~]# yum -y  install percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm
```

2）恢复数据

```mysql
[root@host51 ~]# systemctl  stop mysqld
[root@host51 ~]# ls /var/lib/mysql
[root@host51 ~]# rm -rf /var/lib/mysql/* //清空数据
[root@host51 ~]#innobackupex--apply-log  --redo-only /root/allbak //恢复数据
...
[root@host51 ~]#
[root@host51 ~]#innobackupex --copy-back /root/allbak //拷贝数据
...
xtrabackup_info170425 11:43:09 [00]        ...done170425 11:43:10 completed OK!
[root@host50 ~]# chown  -R mysql:mysql /var/lib/mysql //修改所有者与组
```

步骤四：验证数据恢复

###### 1）启动服务

```mysql
[root@host51 ~]# systemctl  start mysqld
[root@host51 ~]# mysql -uroot -p123456
mysql> show databases;
mysql> select * from db3.user2;
mysql>select count(*) from db3.user;
mysql>   
```

###### 2）查看数据

```mysql
[root@host51 ~]# mysql -uroot -p123456
mysql> show databases;
mysql> select * from db3.user2;
mysql> select count(*) from db3.user;
```



### 2 案例2：恢复单张表

#### 2.1 问题

- 执行删除数据命令
- 使用备份目录/allbak 恢复表数据
- 验证数据恢复



#### 2.2 步骤

实现此案例需要按照如下步骤进行。

##### 步骤一：安装XtraBackup软件包

###### 1）执行删除数据命令

```mysql
[root@host50 ~]# mysql –uroot  -p123456
mysql> delete from db3.user2; //误删除数据操作   
mysql>
```

###### 2) 删除表空间

```mysql
mysql> alter table db3.user2 discard  tablespace;
```

###### 3) 导出表信息

```mysql
[root@host50 ~ ]# innobackupex --apply-log --export  /allbak
```

###### 4) 拷贝表信息文件到数据库目录下

```shell
 [root@host50 ~]# cp /allbak/db3/user2.{cfg,exp,ibd} /var/lib/mysql/db3/
```

###### 5) 修改表信息文件的所有者及组用户为mysql

```shell
[root@host50 ~]# chown mysql:mysql /var/lib/mysql/db3/user2.*    
```

###### 6) 导入表空间

```mysql
mysql> alter  table db3.user2   import  tablespace;
```

###### 7) 删除数据库目录下的表信息文件

```shell
[root@host50 ~]# rm -rf /var/lib/mysql/db3/user2.cfg
[root@host50 ~]# rm -rf /var/lib/mysql/db3/user2.exp
```

###### 8) 查看表记录

```mysql
mysql> select  * from db3.user2;
```



### 3 案例3：增量备份与恢复

#### 3.1 问题

- 具体要求如下：
- 备份所有数据
- 备份新产生的数据
- 删除数据
- 使用备份文件恢复数据



#### 3.2 步骤

实现此案例需要按照如下步骤进行。

##### 步骤一：备份所有数据,在50主机执行

###### 1）完全备份 （备份所有数据到/fullbak目录）

```shell
[root@host50 ~]# innobackupex --user root --password 123456  /fullbak --no-timestamp
```

##### 步骤二：增量备份 （每次执行备份，值备份新数据,在50主机执行）

###### 1) 插入新记录，并做增量备份

```mysql
mysql> insert into db3.user2 values(5,"jack");// 插入新记录,多写几条
[root@host50 ~]# innobackupex --user root --password 123456 --incremental /new1dir --incremental-basedir=/fullbak  --no-timestamp //第1次增量备份 ，数据存储目录/new1dir
```

###### 2) 插入新记录，并做增量备份

```mysql
mysql> insert into db3.user2 values(6,"jack");// 插入新记录,多写几条
[root@host50 ~]# innobackupex --user root --password 123456 --incremental /new2dir --incremental-basedir=/newdir1 --no-timestamp //第2次增量备份 ，数据存储目录/new2dir
```



###### 3) 把备份文件拷贝给目标主机51

```shell
[root@host50 ~]# scp -r /fullbak  root@192.168.4.51:/root/
[root@host50 ~]# scp -r /new1dir/  root@192.168.4.51:/root/
[root@host50 ~]# scp -r /new2dir/  root@192.168.4.51:/root/
```

##### 步骤三：在主机51 恢复数据

###### 1) 停止服务，并清空数据

```shell
[root@host51 ~]# systemctl  stop  mysqld
[root@host51 ~]# rm -rf /var/lib/mysql/*
```

###### 2) 合并日志

```shell
[root@host51 ~ ]# innobackupex --apply-log --redo-only /root/fullbak //准备恢复数据
[root@host51 ~ ]# innobackupex --apply-log --redo-only /root/fullbak --incremental-dir=/root/new1dir  //合并日志[root@host51 ~ ]# innobackupex --apply-log --redo-only /root/fullbak --incremental-dir=/root/new2dir //合并日志 [root@host51 ~ ]# rm -rf  /root/new2dir  //恢复后，可以删除了 
[root@host51 ~ ]# rm -rf  /root/new1dir  //恢复后，可以删除了
```

###### 3) 恢复数据

```mysql
[root@host51 ~ ]# innobackupex --copy-back /root/fullbak   //拷贝文件到数据库目录下
[root@host51 ~ ]# chown  -R mysql:mysql /var/lib/mysql //修改所有者与组用户
[root@host51 ~ ]# systemctl  start mysqld //启动服务
[root@host51 ~ ]# mysql -uroot -p123456 //登录
mysql> select  count(*)  from db3.user; //查看数据
```



# 