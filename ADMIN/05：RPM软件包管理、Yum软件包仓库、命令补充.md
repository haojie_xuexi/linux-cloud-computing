## 05：RPM软件包管理、Yum软件包仓库、命令补充

![img](./05image/001.png)

### RPM包管理机制

#### 常见的软件封包类型

![img](./05image/002.jpg)

#### RPM的含义

•    RPM Package Manager

由红帽公司提出，RedHat、SUSE等系列采用

建立集中数据库，记录软件包安装/卸载等变化信息，分析软件包依赖关系 

• RPM包文件名特征

–  软件名-版本信息.操作系统.硬件架构.rpm

 firefox-52.7.0-1.el7.centos.x86_64.rpm

#### 常见安装位置

- RPM包的一般安装位置（分散）

| **文件类别**         | **默认安装位置**                  |
| -------------------- | --------------------------------- |
| 普通执行程序         | /usr/bin/ 、/bin/                 |
| 服务器程序、管理工具 | /usr/sbin/ 、/sbin/               |
| 配置文件             | /etc/ 、/etc/软件名/              |
| 日志文件             | /var/log/、/var/log/软件名/       |
| 程序文档、man手册页  | /usr/share/doc/ 、/usr/share/man/ |

### 查询软件信息

#### 查询已安装的软件

- **查询已安装的RPM软件包的信息**

  **-格式： rpm -q[子选项] [软件名称]**

- **常用的子选项**

  - **-a : 列出已安装的所有软件包**
  - **-i : 查看指定软件的详细信息**
  - **-l : 查看指定软件的文件安装清单**

- ### 确认软件包的安装情况

  ```shell
  ]# rpm  -qa    #当前系统中所有已安装的软件包
  
  ]# rpm  -q   firefox    #查看firefox是否安装
  firefox-52.7.0-1.el7.centos.x86_64
  ]# rpm  -q   httpd    
  未安装软件包 httpd
  ]# rpm  -q   bash
  bash-4.2.46-30.el7.x86_64
  ```

- **了解软件包在详细安装信息**

```shell
]# rpm  -qi   firefox       #查询软件信息
]# rpm  -ql   firefox       #查询软件安装了哪些内容(安装清单)
]# rpm  -ql    firefox   |   less
```

- **查询某个目录/文件是哪个RPM包带来的**

  **–  格式：rpm -qf [文件路径]…**

  **–  即使目标文件被删除，也可以查询**

  ```shell
  [root@localhost ~]# which  vim   #查询命令对应的程序文件
  /usr/bin/vim
  [root@localhost ~]# rpm -qf  /usr/bin/vim
  vim-enhanced-7.4.160-4.el7.x86_64
  [root@localhost ~]# rpm -q vim-enhanced
  vim-enhanced-7.4.160-4.el7.x86_64
  ```

- **查询未安装软件包**

  **格式：rpm -q [子选项]  [RPM 包文件**

  **常用的子选项**

  > **-pi: 查看指定软件的详细信息**
  >
  > **-pl: 查看指定软件的文件安装清单**

```shell
]# rpm  -q   vsftpd     #查询vsftpd软件是否安装
未安装软件包 vsftpd 
]# ls /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm

查询软件包的安装清单：
]# rpm -qpl  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm
查询软件包信息
]# rpm -qpi /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm

```

#### 导入红帽签名信息（了解）

```shell
]# rpm  --import    /mnt/RPM-GPG-KEY-CentOS-7
查询软件包信息
]# rpm -qpi  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm
```

### 安装/卸载软件包

#### **安装**

- **安装RPM软件**

  **-格式：rpm -i RPM包文件**

- **辅助选项**

  **–   -v：显示细节信息**

  **–   -h：以#号显示安装进度**

  **–   --force：强制安装、覆盖安装**

  **–   --test：测试安装，不做真实安装动作**

```shell
]# rpm  -q   vsftpd        #查询当前的系统是否安装了该软件
未安装软件包 vsftpd 

]# rpm  -ivh  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm 
]# rpm  -q  vsftpd    #查询当前的系统是否安装了该软件
vsftpd-3.0.2-22.el7.x86_64

]# rpm -e vsftpd            #卸载软件
]# rpm -q vsftpd            #查询当前的系统是否安装了该软件
未安装软件包 vsftpd
```

#### **--force：强制安装、覆盖安装**

```shell
]# which   hostname
/usr/bin/hostname
]# rm   -rf   /usr/bin/hostname     
]# hostname
bash: hostname: 未找到命令...

]# rpm -qf   /usr/bin/hostname     #查看由哪个软件包产生
hostname-3.13-3.el7.x86_64

]# rpm  -ivh   --force   /mnt/Packages/hostname-3.13-3.el7.x86_64.rpm

```

#### **解决依赖关系**

> –  先安装/卸载要求的包
>
> 如果RPM包齐全但比较多，可以用通配符 *
>
> •   忽略依赖关系（不推荐）
>
> –  可能会导致软件运行异常
>
> –  辅助选项 --nodeps
>
> 常见依赖关系的报错：
>
> ```shell
> [root@localhost ~]# rpm -ivh /mnt/Packages/bind-chroot-9.9.4-61.el7.x86_64.rpm 
> ```
>
> 错误：依赖检测失败：bind = 32:9.9.4-61.el7 被 bind-chroot-32:9.9.4-61.el7.x86_64 需要

#### **卸载**

- **卸载RPM软件**

  **-格式 ： rpm -e 软件名.. ..**

  ```shell
  ]# rpm -e vsftpd
  ]# rpm -q vsftpd
  未安装软件包 vsftpd
  ```

  

### 构建Yum软件包仓库

![img](./05image/003.png)

**作用：自动解决依赖关系安装软件**

**服务：自动解决依赖关系安装软件**

 

**服务端(本机): 1.众多的软件  2.仓库数据文件（repodata）**

​           **3.FTP协议  或  http 协议**    

**本地Yum仓库：服务端需要有光盘内容即可**

****

**客户端(本机)：指定服务端位置**

**–  仓库配置：/etc/yum.repos.d/*.repo**

**–  错误的文件会影响正确的文件**

 

#### **客户端文件配置内容：**

**–  [源名称] ：自定义名称，具有唯一性**

**–  name：本软件源的描述字串**

**–  baseurl：指定YUM服务端的URL地址**

**–  enabled：是否启用此频道**

**–  gpgcheck：是否验证待安装的RPM包**

**–  gpgkey：用于RPM软件包验证的密钥文件**

```shell
完整示例：
]# vim   /etc/yum.repos.d/mydvd.repo 
[nsd2008]
name=hahaxixi
baseurl=file:///mnt
enabled=1
gpgcheck=1        
gpgkey=file:///mnt/RPM-GPG-KEY-CentOS-7

]# ls    /etc/yum.repos.d/
]# mkdir     /etc/yum.repos.d/bak
]# mv   /etc/yum.repos.d/*.repo      /etc/yum.repos.d/bak
]# ls   /etc/yum.repos.d/
bak

]# vim     /etc/yum.repos.d/mydvd.repo
[nsd2008]                #仓库的名称
name=hahaxixi         #仓库描述信息
baseurl=file:///mnt   #指定服务端位置file://表示本地为服务端
enabled=1           #本文件启用
gpgcheck=0        #不检测红帽签名

]# yum  repolist      #列出仓库信息

```

####  **总结：本地Yum仓库构建方法**

**1.服务端：显示光盘的内容，挂载光驱设备**

**2.客户端：书写客户端配置文件，指定服务端位置**

**3.执行流程: yum命令--->/etc/yum.repos.d/*.repo--->baseurl=file:///mnt**

------



### Yum的使用

#### **安装软件**

```shell
[root@localhost ~]# yum -y install httpd
[root@localhost ~]# rpm -q httpd

[root@localhost ~]# yum -y install bind-chroot
[root@localhost ~]# rpm -q  bind-chroot

[root@localhost ~]# yum -y install sssd
[root@localhost ~]# rpm -q  sssd

[root@localhost ~]# yum -y install gcc
[root@localhost ~]# rpm -q  gcc

[root@localhost ~]# yum -y install  xorg-x11-apps
[root@localhost ~]# rpm -q  xorg-x11-apps

[root@localhost ~]# rpm  -ql   xorg-x11-apps   |   less
[root@localhost ~]# xeyes
```

#### **卸载软件**

```shell
[root@localhost ~]# yum   remove   gcc
[root@localhost ~]# yum   remove   httpd
```

#### **查询**

```shell
[root@localhost ~]# yum list  ftp    #查询仓库是否有ftp软件
可安装的软件包  #表示当前系统没有安装该软件
ftp.x86_64        0.17-67.el7         nsd2008
[root@localhost ~]# yum  list  httpd

[root@localhost ~]# yum  search  ftp   #包含ftp就匹配

]# yum   provides   /usr/bin/xeyes  
]# yum  provides  /etc/passwd  #仓库中那个软件包产生该文件
```

#### **清空缓存**

```shell
[root@localhost ~]# yum  clean   all
[root@localhost ~]# yum   repolist
```

### **命令补充**

#### **获取命令帮助**

> **方式一：命令  --help**
>
> ```shell
> [root@localhost ~]# cat  --help
> ```
>
> **方式二：man  命令**  
>
> ```shell
> [root@localhost ~]# man  cat    #按q退出
> [root@localhost ~]# man   passwd    #显示passwd命令帮助
> [root@localhost ~]# man  5  passwd
> ```

#### **历史命令**

##### **管理/调用曾经执行过的命令**

> –  history：查看历史命令列表
>
> –  history -c：清空历史命令
>
> –  !n：执行命令历史中的第n条命令
>
> –  !str：执行最近一次以str开头的历史命令

```shell
[root@svr7 ~]# vim  /etc/profile
HISTSIZE=1000      #默认记录1000条

[root@localhost ~]# history          #显示历史命令列表
[root@localhost ~]# history   -c    #清空历史命令
[root@localhost ~]# history                 
[root@localhost ~]# cat   /etc/redhat-release 
[root@localhost ~]# ls   /root
[root@localhost ~]# history

[root@localhost ~]# !cat  #指定最近一条以cat开头的历史命令
[root@localhost ~]# !ls  #指定最近一条以ls开头的历史命令
```

#### **du**, **统计文件的占用空间**

> **–  du [选项]... [目录或文件]...**
>
> **–  -s：只统计每个参数所占用的总空间大小**
>
> **–  -h：提供易读容量单位（K、M等）**

```shell
[root@localhost ~]# du  -sh   /root
[root@localhost ~]# du  -sh   /etc
[root@localhost ~]# du  -sh   /
```

#### **date，查看/调整系统日期时间**

> **–  date +%F、date +%R**
>
> **–  date +"%Y-%m-%d %H:%M:%S"**
>
> **–  date -s "yyyy-mm-dd HH:MM:SS"** 

```shell
]# date
]# date  -s    "2008-9-6   11:11:11"     #修改系统时间
]# date

]# date   -s    "2020-9-5   15:37:11"   
]# date

[root@localhost ~]# date   +%Y     #显示年
[root@localhost ~]# date   +%m   #显示月
[root@localhost ~]# date   +%d   #显示日期

[root@localhost ~]# date   +%H   #显示时
[root@localhost ~]# date   +%M   #显示分
[root@localhost ~]# date   +%S    #显示秒

[root@localhost ~]# date   +%F   #显示年-月-日
[root@localhost ~]# date   +%R   #显示时:分

```

#### **制作连接(链接)文件（制作快捷方式）**

> **格式：ln -s  /路径/源数据   /路径/快捷方式的名称   #软连接**

```shell
]# ln  -s    /etc/sysconfig/network-scripts/   /ns
]# ls   /
]# ls   -l   /ns    #查看快捷方式的信息

]# touch   /ns/haha.txt
]# touch   /ns/maohehaozi.txt
]# touch   /ns/shukehebeita.txt
]# ls   /etc/sysconfig/network-scripts/
```

> 软连接优势：可以针对目录与文件制作快捷方式，支持跨分区
>
> 软连接缺点：源数据消失，快捷方式失效

> **格式：ln   /路径/源数据   /路径/快捷方式的名称   #硬链接**
>
> 硬链接优势：源数据消失，快捷方式仍然有效
>
> 硬链接缺点：只能针对文件制作快捷方式，不支持支持跨分区

```shell
[root@localhost ~]# rm  -rf   /opt/*
[root@localhost ~]# echo  123   >   /opt/A.txt
[root@localhost ~]# ln  -s   /opt/A.txt    /opt/B.txt   #软连接
[root@localhost ~]# ls /opt/

[root@localhost ~]# ln    /opt/A.txt    /opt/C.txt   #硬链接
[root@localhost ~]# ls    /opt/
[root@localhost ~]# cat    /opt/B.txt 
[root@localhost ~]# cat    /opt/C.txt 

[root@localhost ~]# rm  -rf   /opt/A.txt 
[root@localhost ~]# ls   /opt/
[root@localhost ~]# cat  /opt/B.txt      #软连接失效
cat: /opt/B.txt: 没有那个文件或目录
[root@localhost ~]# cat   /opt/C.txt     #硬链接仍然有效
```

#### **zip归档工具，跨平台**

•   **归档+压缩操作: zip [-r]  备份文件.zip   被归档的文档...** 

[-r]: 被归档的数据有目录，必须加上此选项

```shell
]# zip   -r     /opt/abc.zip        /etc/passwd     /home
]# ls   /opt/
```

•   **释放归档+解压操作s: unzip  备份文件.zip  [-d  目标文件夹]** 

```shell
]# mkdir   /nsd20
]# unzip       /opt/abc.zip       -d    /nsd20
]# ls   /nsd20
]# ls   /nsd20/etc/
]# ls   /nsd20/home/
```

------

