## 02：VlAN技术及应用、TRUNK、网络层解析

### **广播域**

广播域指接收同样广播消息的节点的集合，如：在该集合中的任何一个节点传输一个广播帧，则所有其他能收到这个帧的节点都被认为是该广播帧的一部分

交换机的所有接口默认属于同一个广播域

 

### **Vlan**

#### • 什么是VLAN

 Virtual LAN（虚拟局域网）是物理设备上连接的不受物理位置限制的用户的一个逻辑组。

#### • 为什么引入VLAN

 交换机的所有接口默认属于同一个广播域

 随着接入设备的增多，网络中广播增多，降低了网络的效率

 为了分割广播域，引入了VLAN

 

### **VLAN的作用**

 广播控制

 增加安全性

 提高带宽利用

 降低延迟

 

### **VLAN的配置**

```
[Huawei]vlan 2		//创建vlan2

[Huawei-vlan2]display vlan  //查看vlan列表

[Huawei-vlan2]quit   

[Huawei]undo vlan 2  //删除vlan2

 

https://gitee.com/nsd-tedu/nsd2008  笔记连接地址

 

[Huawei-vlan2]interface Ethernet0/0/3  //进入3口

[Huawei-Ethernet0/0/3]port link-type access  //将3接口定义为接入

链路，接入链路表示该接口即将为某一个vlan传递数据

[Huawei-Ethernet0/0/3]port default vlan 2 //将3接口加入VLAN2,如

果加入了错误的vlan, 就在加一次（再输入一次加vlan的命令）正

确的vlan即可

[Huawei-Ethernet0/0/3]display vlan  //查看结果

 

[Huawei]undo vlan batch 10 to 20

[Huawei]vlan batch 2 3
```

 

### 使用接口组

```
[Huawei]port-group 1  //创建1号接口组

[Huawei-port-group-1]group-member Ethernet 0/0/3  Ethernet 0/0/4   # 多个使用to

//在该组添加成员，3口和4口

[Huawei-port-group-1]port link-type access  //将该组中所有接口配置为接

入链路

[Huawei-port-group-1]port default vlan 2  //将该组中所有接口加入VLAN2
```

 

```
[Huawei]port-group 2  //创建2号接口组

[Huawei-port-group-1]group-member Ethernet 0/0/5 Ethernet 0/0/6  

在该组添加成员，5口和6口

[Huawei-port-group-1]port link-type access  

[Huawei-port-group-1]port default vlan 3
```

 

**access  接入链路 可以承载1个vlan的数据**

**链路  可以承载多个vlan的数据**

 

两台交换机都按以下方式配置，即可实现仅仅使用一条线缆传递多个

Vlan的数据

```
[Huawei-Ethernet0/0/7]port link-type trunk  将7号接口配置为中继链路

[Huawei-Ethernet0/0/7]port trunk allow-pass vlan all  允许该接口放行所
```

有vlan的数据

注意！中继链路的线缆要在2台交换机都配置

排错：首先检查ip，再看vlan以及对应的接口，再检查两台交换机之间

的线路是否为trunk

最后测试，全网相同vlan的主机之间均可ping通即可

### **链路聚合**

多条线路负载均衡，带宽提高

容错，当一条线路失效时，不会造成全网中断

由于链路聚合配置需要在接口默认状态配置，所以要先清空

两台交换机的7号接口所有配置，相当于恢复默认

```
[Huawei]clear configuration interface Ethernet 0/0/7  //清空

7号接口的所有配置，途中输入y确认

[Huawei]interface Ethernet0/0/7  //进入7号口

[Huawei-Ethernet0/0/7]undo shutdown   //开启7号接口
```

 

然后在两台交换机做以下链路聚合的配置：

```
[Huawei]interface Eth-Trunk 1  //创建(进入)1号链路聚合接口

[Huawei-Eth-Trunk1]trunkport Ethernet 0/0/7 0/0/8  //将7号接

口与8号接口合并捆绑成一个接口，该接口的名字就叫做Eth-Trunk 1

[Huawei-Eth-Trunk1]port link-type trunk  //将链路聚合接口

配置为中继链路

[Huawei-Eth-Trunk1]port trunk allow-pass vlan all  //放行所有

Vlan的数据
```

###  **使用路由器连接不同网段：**

```
[Huawei]undo info-center enable  //关日志

[Huawei]interface gigabitEthernet0/0/0  //进入0号接口

[Huawei-GigabitEthernet0/0/0]ip address 192.168.1.254 24  //配置ip

[Huawei-GigabitEthernet0/0/0]in g0/0/1

[Huawei-GigabitEthernet0/0/1]ip address 192.168.2.1 24

[Huawei]display ip interface brief  //检查设备所有的ip地址配置情况

之后每个pc再配置对应网段的网关即可全网互通

192.168.1.1的网关是192.168.1.254

192.168.2.2的网关是192.168.2.1
```

 

### **ICMP  英特网控制报文协议**，

可以反馈网络中比如是否联通，花

费时间等信息

Ping  -t  持续ping

Ping  -l  1000  将ping包大小修改为1000字节

 

### **常见的ping反馈结果**

– 连接建立成功，Reply from 目标地址 .. ..

– 目标主机不可达，Destination host unreachable.

– 请求时间超时，Request timed out.

 

 

### **路由器是依靠路由表转发数据**

**路由表的产生方式：**

1， 直连路由，路由器接口配置好ip并开启则自动产生

2， 静态路由，由管理员手工配置，添加所要前往的地址

语法格式：ip route-static 目标网段地址 子网掩码  下一跳

 

第一台路由器：

```
[Huawei]ip route-static 192.168.3.0 24 192.168.2.2  //添加静态路由

使该路由设备可以前往3网段，下一跳地址是2.2

[Huawei]ip route-static 192.168.4.0 24 192.168.2.2

[Huawei]display ip routing-table | include /24  //查看路由表，已经

多出了3.0网段的路由

<Huawei>display  ip  interface  brief   //查看ip配置情况
```

 

第二台路由器：

```
[Huawei]ip route-static 192.168.1.0 24 192.168.2.1  //添加静态路由

使该路由设备可以前往1网段，下一跳地址是2.1
```



### **练习：**

1，VLAN的作用是什么？

```
广播控制（避免广播泛滥），增加安全性，提高带宽利用，降低延迟
```

2，TRUNK的作用是什么？

```
为数据帧打上VLAN标识，使不同VLAN数据可以用一条链路传递（单一链路可以承载多个vlan的数据）
```

3，链路聚合的作用是什么？

```
提供更高的带宽和增加可靠性
```

4，网络层的功能有哪些？

```
定义了基于IP协议的逻辑地址
连接不同的媒介类型
选择数据通过网络的最佳路径
```

5，ping工具与哪个协议有关？

```
ICMP
```

6，静态路由配置语法格式是？

```
ip route-static 目标网段 子网掩码 下一跳地址
```

7，路由设备依靠什么转发数据？

```
路由表
```





