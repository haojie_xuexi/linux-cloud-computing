import sys
import os

if __name__ == '__main__':
    if len(sys.argv) < 3:
       print("\033[0;31;40mNeed 2 paramters\033[0m ")
       exit(1)
    
    if len(sys.argv) == 4:
        dir_fd = sys.argv[3]
    else:
        dir_fd = os.getcwd()

    old_end = sys.argv[1]
    new_end = sys.argv[2]
    if old_end == new_end:
        print("\033[1;31;40mSame value,no change.\033[0m")
        exit(2)
    for path, folders, files in os.walk(dir_fd):
        for file in files:
            try:
                new_name = file.rstrip(old_end) + new_end
            except  
            os.rename(file, new_name)
    print("\033[1;31;40mOK\033[0m")
    print(f"\033[1;31;40m{os.listdir()}\033[0m")
