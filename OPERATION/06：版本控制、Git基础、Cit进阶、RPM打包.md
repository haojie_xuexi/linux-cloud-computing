## 06:版本控制、Git基础、Cit进阶、RPM打包

![img](./06image/001.png)



### **使用Varnish加速Web**

**有时网站服务器搭建好了客户会因为距离较远而访问效果不好，出现这种情况就可以用varnish工具在距离客户比较近的地区搭建缓存服务器，然后客户访问缓存服务器即可，缓存服务器会从原始站点获得数据并缓存，随着被访问与被缓存的数据越来越多，客户的访问速度就可以加快了，但由于这种方式不是一般企业可以做到，通常有需求时可以去购买cdn （内容分发网络）服务。**

 

**以下实验仅用来感受该服务效果，了解理念即可**

#### **1，安装varnish服务**

首先使用web1主机开启httpd服务

```shell
[root@web1 ~]# ss -ntulp | grep :80   //可以先查询80端口是否被占用

killall nginx   //如果nginx占用就杀掉

[root@web1 ~]# systemctl start httpd  //仅仅启动httpd
```

在使用proxy测试web1的页面

```shell
[root@proxy ~]# curl 192.168.2.100

[root@proxy ~]# ss -ntulp | grep :80

[root@proxy ~]# killall nginx  //如果有服务占用80端口就杀死

[root@proxy ~]#cd  ~/ lnmp_soft

[root@proxy lnmp_soft]# tar -xf varnish-5.2.1.tar.gz

[root@proxy lnmp_soft]# cd varnish-5.2.1/

yum -y install gcc readline-devel pcre-devel python-docutils  //安装varnish所需依赖包

./configure

make

make install

useradd -s /sbin/nologin varnish   //创建varnish所需账户
```

 

#### **2，修改配置**

```shell
cp etc/example.vcl /usr/local/etc/default.vcl  //拷贝配置文件

vim /usr/local/etc/default.vcl  //修改配置文件17、18行

  .host = "192.168.2.100";  //原始服务器的ip

  .port = "80";					//原始服务器的端口号

varnishd  -f  /usr/local/etc/default.vcl  //指定配置文件路径并启动varnish服务
```

 

#### **3，测试效果**

```shell
[root@proxy varnish-5.2.1]# curl 192.168.2.5  #测试访问proxy的页面，可以看到原始服务器web1的内容
```

 

\------------------------------------------------------

### **版本管理工具：**

**svn 集中式用户使用该服务时，需要时刻与服务器保持在线状态，数据统一保存在svn服务器中**

**git 分布式  用户使用该服务时，不需要时刻与服务器保持在线状态，仅仅传递数据时需要联网，数据保存在git服务器与git客户端**

 

https://gitee.com/

https://github.com/

 

![img](./06image/002.png) 

 

 

#### **使用git管理数据**

##### **1，基本操作**

首先在web1与web2安装git工具

```shell
[root@web1 ~]# yum -y install git

[root@web2 ~]# yum -y install git

[root@web1 ~]# mkdir /var/lib/git

[root@web1 ~]# git init /var/lib/git/project --bare  //创建空仓库，叫Project

[root@web2 ~]#git clone 192.168.2.100:/var/lib/git/project  //在客户机克隆服务器的仓库

[root@web2 ~]#cd  project  //进入仓库

[root@web2 project]# echo "web2_01" > web2_01.txt  //创建测试文件  

[root@web2 project]# git add .  //提交到暂存区

[root@web2 project]# git commit -m "web2_01.txt"  //将文件保存到仓库中，-m后面的内容是日志提示信息，首次保存会失败，按照下面2条命令输入邮箱和用户名即可

 git config --global user.email "you@example.com"

 git config --global user.name "Your Name"

[root@web2 project]# git commit -m "web2_01.txt"  再次提交文件保存到仓库中

[root@web2 project]#git push 将本地仓库中的数据推送到远程服务器

[root@web2 project]# git config --global push.default simple  配置使用习惯

[root@web2 project]#git push
```

 

**所有通过commit提交的记录都可以通过日志查看**

**git log  //查看完整日志**

**git log --pretty=oneline  //查看精简日志**

**git log --oneline  //查看最精简日志**

**git reflog  //看本机操作记录**

 

##### **2，head指针**

**如果需要还原到之前的版本(时间节点)，可以利用head指针对应日志记录中的随机字符串指向需要的版本**

```shell
[root@web2 project]# git log --oneline  //查看日志，开头的部分就是不同版本的随机字符串

[root@web2 project]# git  reset  xxxx  --hard  //回到过去的某个记录，
其中 xxxx是日志中显示的时间节点信息，要根据实际修改

[root@web2 project]# git reflog  //查看回复记录之后的日志记录，head@{0}代表当前所在版本位置
```

 

**回复到过去的时间节点，找回数据思路：**

1，	git log --oneline  查看日志，找到旧数据所在时间节点

2，	git  reset  xxxx  --hard  回到过去，xxxx是时间节点的记录

3，	把需要找回的数据，从仓库中拷贝到另外一个目录

4，	git  reset  xxxx  --hard  回到现在

5，	在之前的目录找回旧数据

 

\--------------------------------------------------------------

##### **3，git分支**

**当项目内容比较多时，可以在git中使用分支，不同分支的文件可以互不干扰而不用创建多个仓库**

```shell
[root@web2 project]# git branch  //查看当前分支，*是所在位置

[root@web2 project]# git branch hotfix  //创建hotfix分支

[root@web2 project]# git checkout hotfix  //切换到hotfix分支

[root@web2 project]# echo "hotfix_01" > hotfix_01.txt   //编写测试文件

[root@web2 project]# git add .   //提交到暂存区

[root@web2 project]# git commit -m "hotfix_01.txt"   //提交到仓库保持

[root@web2 project]# git checkout master  //切换到master分支

[root@web2 project]# echo "master_01" > master_01.txt   //编写测试文件

[root@web2 project]# git add .   //提交到暂存区

[root@web2 project]# git commit -m "hotfix_01.txt"  //提交到仓库保持

git merge hotfix   //把hotfix分支的文件合并到当前分支
```

 

**如果分别在不同分支，创建同名文件，内容不同，再进行合并时，会发生冲突，此时需要手工修改冲突文件，修改完之后，就可以解决冲突**

```shell
git checkout hotfix  //先切换到hotfix分支

echo abc > abc.txt  //创建文件

git add .  //提交到暂存区

git commit -m "abc.txt"  //提交到仓库保存

git checkout master  //再切换到主分支

echo xyz > abc.txt  //创建同名文件，但内容不同

git add .   //提交到暂存区

git commit -m "abc.txt"   //提交到仓库保存

git merge hotfix   //合并时会发生冲突，此时修改abc.txt文件之后即可解决冲突
```

 

**练习：把目前仓库删除**

**1，** **在web1重新创建仓库，名称自定义**

**2，** **在web2克隆仓库，并多次创建文件添加commit记录**

**3，** **将web2中的数据同步到web1服务器中**

 

\----------------------------------------------------------------------------

##### **配置ssh秘钥，实现无密码访问git服务器**

```shell
ssh-keygen -f /root/.ssh/id_rsa -N ''  //在客户端创建秘钥秘钥没有密码

ssh-copy-id 192.168.2.100  //传入服务器

git clone 192.168.2.100:/var/lib/git/abc  //克隆git服务器的仓库，已经无需密码

cd abc  //进入仓库

echo abc > abc.txt   //创建测试文件 

git add .  //提交到暂存区

git commit -m "abc"  //提交到仓库保存

git push  //推送到git服务器，也不需要密码了
```

**git服务的使用方式：**

**1，使用ssh方式，上次课程一直在用，这里不多描述**

**2，使用git协议**

Web1主机：

```shell
yum -y install git-daemon //安装git协议软件包

systemctl start git.socket  //开启服务

setenforce 0

vim /usr/lib/systemd/system/git@.service

[root@web1 ~]# vim /usr/lib/systemd/system/git@.service 修改git服务的配置，在第7行末尾加--enable=receive-pack选项，允许对仓库服务器写入数据，否则是只读

[root@web1 ~]# systemctl restart git.socket //开启服务

[root@web1 ~]# setenforce 0 //关闭selinux，(防火墙也关)

[root@web1 ~]#chmod -R 777 /var/lib/git/abc //开放仓库权限

[root@web2 ~]#rm -rf project  //删除原有仓库

[root@web2 ~]#git clone git://192.168.2.100/abc //使用git协议从新克隆

[root@web2 ~]#cd abc //进入仓库，可以正常使用仓库

[root@web2 abc]# echo 123 > abc //创建测试文件

[root@web2 abc]# git add .  //提交到暂存区

[root@web2 abc]# git commit -m "abc" //提交到仓库

[root@web2 project]# git push //推送到远程服务器
```

 

**3，使用http访问git(只读)**

```shell
[root@web1 ~]# yum -y install httpd gitweb 安装软件包

[root@web1 ~]# vim +10 /etc/gitweb.conf 进入配置文件的第10行删除注释即可，其中/var/lib/git路径必须是仓库的上级目录，如果不一致要修改

[root@web1 ~]# systemctl restart httpd 启动网站服务

关闭selinux与防火墙

使用火狐访问http://192.168.2.100/git/  
```

\------------------------------------------------------

### **将源码包转换为rpm包**

**rpm**  **使用便利**  **更新速度慢**

**源码**  **使用繁琐**  **更新速度快**

```shell
yum -y install rpm-build //安装rpm制作工具

[root@web1 ~]# rpmbuild -ba nginx.spec  //制作rpm包，但是没有配置文件会报错，报错也需要敲，会产生所需的目录

[root@web1 ~]#vim rpmbuild/SPECS/nginx.spec  //创建配置文件，并按下描述修改

Name:nginx   软件包名

Version:1.17.6     软件包版本号

Release:1          发布rpm包的版本号

Summary:test  简单描述

\#Group:

License:GPL        授权协议

URL:www.abc.com  网站地址

Source0:nginx-1.17.6.tar.gz  源码包完整文件名

\#BuildRequires: 

\#Requires:

%description  详细描述

test test test

 
%post             可选项，分配额外执行的任务

useradd nginx  安装rpm包同时做哪些操作

yum -y install openssl-devel pcre-devel

 
%prep

%setup -q

%build

./configure  --user=nginx  修改%

make %{?_smp_mflags}

%install

make install DESTDIR=%{buildroot}

%files

%doc

/usr/local/nginx/*            定义打包的文件所在目录

%changelog


cp lnmp_soft/nginx-1.17.6.tar.gz  rpmbuild/SOURCES/

rpmbuild -ba rpmbuild/SPECS/nginx.spec //制作rpm包

rpm -qpi rpmbuild/RPMS/x86_64/nginx-1.17.6-1.x86_64.rpm //查信息

rpm -ivh rpmbuild/RPMS/x86_64/nginx-1.17.6-1.x86_64.rpm //装包
```





