## 02:Linux系统简介、安装Linux系统、Linux基本操作



![图1](./02image/001.png)



### 什么是Linux？



#### Linux系统的简介

- Linux****是一种操作系统！！面向服务端设计

操作系统：可以让计算机硬件正常工作，一堆软件

- **Unix/Linux发展史**

UNIX诞生，1970-1-1

-  **Linux之父，Linus Torwalds**

–  1991年10月，发布0.02版（第一个公开版）内核

–  1994年03月，发布1.0版内核

–  标准读音：“哩呐科斯”

–  内核：调配计算机硬件

   用户--->内核---->计算机硬件

   版本号：主版本.次版本.修订号

-  **发行版的名称/版本由发行方决定**

–  Red Hat Enterprise Linux（RHEL） 5/6/7/8

–  Suse Linux Enterprise 12

–  Debian Linux 7.8

–  Ubuntu Linux 14.10/15.04



-  **CentOS，社区企业操作系统**

–  Community Enterprise Operating System

–  http://www.centos.org/





### 利用虚拟化软件，进行安装Linux操作系统



虚拟化软件（VMware）:虚拟计算机的硬件

- 新建虚拟机

![pic2](./02image/002.png)



![pic3](./02image/003.png)



![image-20200903095032263](./02image/004.png)

![image-20200903095043125](./02image/005.png)

![image-20200903095236246](./02image/006.png)

![image-20200903095253663](./02image/007.png)

![image-20200903095303632](./02image/008.png)

![image-20200903095310183](./02image/009.png)

![image-20200903095319018](./02image/010.png)

![image-20200903095326650](./02image/011.png)

![image-20200903095335200](./02image/012.png)

![image-20200903095342013](./02image/013.png)

![image-20200903095349001](./02image/014.png)

![image-20200903095354026](./02image/015.png)

### 为虚拟机安装Linux操作系统

#### 将光盘镜像文件放入虚拟的光驱设备

![image-20200903095510096](./02image/016.png)

![image-20200903095515600](./02image/017.png)

![image-20200903095524306](./02image/018.png)

虚拟机开机：前提虚拟化功能支持（CPU虚拟化功能打开）

•运行虚拟机需要真机开启虚拟化功能：重启真机系统进入BIOS进行设置

•开启虚拟化功能参考： https://blog.csdn.net/Blueberry521/article/details/104240762

Ctrl+Alt=鼠标回到真机

![image-20200903095604140](./02image/019.png)

![image-20200903095613129](./02image/020.png)

![image-20200903095619721](./02image/021.png)

![image-20200903095626742](./02image/022.png)

![image-20200903095632787](./02image/023.png)

![image-20200903095641361](./02image/024.png)

![image-20200903095649273](./02image/025.png)

![image-20200903095657882](./02image/026.png)

**双击完成**

![image-20200903095712075](./02image/027.png)

![image-20200903095802513](./02image/028.png)

**双击完成**

![image-20200903095811562](./02image/029.png)

![image-20200903095817451](./02image/030.png)

![image-20200903095825317](./02image/031.png)

![image-20200903095830788](./02image/032.png)

![image-20200903095838137](./02image/033.png)

![image-20200903095844643](./02image/034.png)

![image-20200903095850053](./02image/035.png)

![image-20200903095855828](./02image/036.png)

![image-20200903095900679](./02image/037.png)

![image-20200903095907624](./02image/038.png)

![image-20200903095913752](./02image/039.png)

![image-20200903095921112](./02image/040.png)

![image-20200903095927023](./02image/041.png)

![image-20200903095931952](./02image/042.png)

#### 修改系统的时间

![image-20200903100018159](./02image/043.png)

![image-20200903100022988](./02image/044.png)

![image-20200903100030722](./02image/045.png)

#### 关闭系统的节能功能

![image-20200903100044971](./02image/046.png)





### **Linux**系统的目录结构

> 根目录(利用/表示)：Linux系统的起点（所有的数据都在此目录下）
>
> /dev：设备(键盘、鼠标、硬盘、光驱…..)相关的数据

> /dev/abc/1.txt：一个完整的路径，只有开头的/才表示为根目录，其他的/表示为分隔符

![image-20200903100209324](./02image/047.png)

![image-20200903100215457](./02image/048.png)

###  **Linux**系统的硬盘(磁盘)表示

![image-20200903100249412](./02image/049.png)

### Linux哲学的理念: 一切皆文件

![image-20200903100313382](./02image/050.png)

> /dev/sda:表示SCSI接口的硬盘第一块 
>
> /dev/sdb:表示SCSI接口的硬盘第二块

> /dev/sdc:表示SCSI接口的硬盘第二块
>
> ……….

### Linux基本操作

#### 获得命令行

> 方式一：虚拟控制台切换（ Ctrl + Alt + Fn 组合键）
>
> –  tty1：图形桌面
>
> –  tty2~tty6：字符控制台
>
> 方式二：在图形桌面

![image-20200903100450644](./02image/051.png)

![image-20200903100457847](./02image/052.png)

![image-20200903100503513](./02image/053.png)

###  **Linux**命令行操作

#### 命令行提示符

```
[root@localhost ~]#
```

> [当前登录的用户@主机名 当前所在的位置] #
>
> 如果以#结尾：表示当前登录的身份为超级管理员root
>
> 如果以$结尾：表示当前登录的身份为普通用户
>
> 放大: Ctrl  shift +
>
> 变小: Ctrl -

#### pwd — Print Working Directory

> 用途：查看当前工作目录（显示当前所在的位置）



#### cd — Change Directory

> 用途：切换工作目录
>
> 格式：cd [目标文件夹位置]



#### ls — List

> 格式：ls  [目录或文件名]…



> ```shell
> [root@localhost ~]# pwd      #显示当前所在的位置
> [root@localhost ~]# cd    /    #切换到根目录
> [root@localhost /]# pwd
> [root@localhost /]# ls            #显示当前目录的内容
> [root@localhost /]# cd   /home
> [root@localhost home]# pwd
> [root@localhost home]# ls
> 
> [root@localhost home]# cd   /opt
> [root@localhost opt]# pwd
> [root@localhost opt]# ls
> 
> [root@localhost opt]# cd  /root
> [root@localhost ~]# pwd
> [root@localhost ~]# ls
> 
> 蓝色：目录
> 黑色：文件
> ```

#### ls — List：显示指定目录内容

```shell
[root@localhost ~]# ls  /opt

[root@localhost ~]# ls  /home

[root@localhost ~]# ls  /

[root@localhost ~]# ls  /tmp

[root@localhost ~]# ls  /dev

[root@localhost ~]# ls  /etc

[root@localhost ~]# ls  /boot
```

#### cd — Change Directory：切换目录

> 绝对路径：以根目录开头的路径
>
> 相对路径：以当前所在目录，为参照的路径

```shell
]# cd   /etc/pki/             #绝对路径
]# pwd 
]# ls 
]# cd   /etc/pki/CA       #绝对路径，与当前所在位置无关
]# pwd
]# ls

]# cd   /etc/pki/         #绝对路径
]# pwd
]# ls
]# cd  CA        #相对路径，与当前所在位置有关
]# pwd

```

#### .. ：上一级目录

```shell
[root@localhost CA]# cd   /etc/pki/CA
[root@localhost CA]# pwd
/etc/pki/CA
[root@localhost CA]# cd   ..           #返回上一级目录
[root@localhost pki]# pwd
/etc/pki
[root@localhost pki]# cd  ..              #返回上一级目录
[root@localhost etc]# pwd
/etc
[root@localhost etc]# cd   ..            #返回上一级目录
[root@localhost /]#
```

#### 查看文本文件内容命令:cat 适合查看内容较少的文件

```shell
[root@localhost /]# cat   /root/anaconda-ks.cfg
[root@localhost /]# cat   /etc/redhat-release   #显示系统版本
CentOS Linux release 7.5.1804 (Core)

[root@localhost /]# cat   /etc/passwd
[root@localhost /]# cat   /etc/fstab
[root@localhost /]# cat   /etc/hosts
[root@localhost /]# cat   /etc/shells 
```

#### 查看文本文件内容命令:less 适合查看内容较多的文件

```shell
[root@localhost /]# less   /etc/passwd
 按上下键进行滚动，按q进行退出

```

#### 查看文本文件部分内容命令head、tail 命令   

> –  格式：head -n 数字 文件名   #头几行
>
> ​            tail -n 数字 文件名   #尾几行

```shell
[root@localhost /]# head -1   /etc/passwd

[root@localhost /]# head  -2  
/etc/passwd
[root@localhost /]# head  -3   /etc/passwd
[root@localhost /]# head  -13   /etc/passwd

[root@localhost /]# tail   -1   /etc/passwd
[root@localhost /]# tail   -2   /etc/passwd
```

#### 过滤包含指定字符串的行  

```shell
[root@localhost /]# grep  root  /etc/passwd
[root@localhost /]# grep  a    /etc/passwd
[root@localhost /]# grep  bash  /etc/passwd
[root@localhost /]# grep  dog   /etc/passwd
```

#### 修改文本文件内容：vim（文本编辑器）

>  三个模式：命令模式、插入模式(输入模式)、末行模式
>
>  ```
>  ]# vim  /opt/nsd.txt  #当文件不存在，会新建文件
>  ```
>
>  命------i键 或者 o键------>插入模式(按Esc回到命令模式)
>
>  令
>
>  模
>
>  式------输入 ： ----------->末行模式(按Esc回到命令模式)
>
>  末行模式  :wq   #保存并退出
>
>  末行模式  :q！   #强制不保存并退出
>
>  ]# cat  /opt/nsd.txt
>
>  

#### 新建目录：mkdir

```shell
[root@localhost /]# mkdir    /opt/nsd01
[root@localhost /]# ls    /opt/
[root@localhost /]# mkdir   /root/nsd02    /opt/nsd03
[root@localhost /]# ls    /root/
[root@localhost /]# ls    /opt/
```

#### 新建文件：touch

```shell
[root@localhost /]# touch    /opt/a.txt
[root@localhost /]# ls   /opt/

[root@localhost /]# touch     /opt/b.txt
[root@localhost /]# ls    /opt/
```

#### 查看以及设置主机名的命令：hostname

```shell
[root@localhost /]# hostname
localhost.localdomain
[root@localhost /]# hostname   hahaxixi    #设置主机名
[root@localhost /]# hostname                    #查看主机名
开启一个新的命令行
[root@hahaxixi ~]# hostname  nb.tedu.cn
[root@hahaxixi ~]# hostname
开启一个新的命令行
[root@nb ~]# hostname
```

#### 查看网卡IP地址命令：ifconfig

```shell
[root@nb ~]# ifconfig
```

lo: 回环测试接口，专门用于测试，本机访问自己

​    127.0.0.1：特殊IP地址，永远代表本机

 

​    virbr0：虚拟网卡

```
[root@nb ~]# ping 127.0.0.1
```

**按 Ctrl+c结束正在运行的指令**



#### 查看cpu信息

```shell
[root@nb ~]# lscpu
…….
型号名称：        Intel(R) Core(TM) i7-7700 CPU @ 3.60GHz
…….

```

####  查看内存信息

```shell
[root@nb ~]# cat  /proc/meminfo

MemTotal:    1865284 kB   #内存一共大小

…….
```

#### 关机poweroff

#### 重启reboot



### VMware拍摄快照

![image-20200903102453013](./02image/054.png)

![image-20200903102458290](./02image/055.png)

#### **还原快照：**

![image-20200903102517405](./02image/056.png)

![image-20200903102523997](./02image/057.png)

### 课后练习：

案例：ls命令练习

1. 查看根目录下内容

```
[root@localhost ~]# ls  /
```

2. 显示/etc目录内容 

3. 显示/boot目录内容的

4. 显示/root的内容

5. 显示/bin/bash程序

[root@localhost ~]# ls /bin/bash

6. 显示/opt目录内容

 

案例：查看文件内容练习

 1.查看/etc/passwd文件内容

 2.查看/etc/default/useradd文件内容

 3.查看内存信息

 4.查看/etc/hosts文件内容

 5.显示文件/etc/passwd文件内容的头3行内容

 6.显示文件/etc/passwd文件内容的尾4行内容

 7.显示文件/etc/passwd文件内容的头12行内容

 8.利用less分屏阅读/etc/passwd文件内容

 9.利用grep命令显示/etc/passwd文件内容中，包含root的行

 

案例：cd命令练习

 1.切换到根目录，利用pwd命令查看当前位置

 2.切换到/root，利用pwd命令查看当前位置

 3.切换到/boot，利用pwd命令查看当前位置

 4.切换到/opt，利用pwd命令查看当前位置

 5.切换到/tmp，利用pwd命令查看当前位置

 6.切换到/var，利用pwd命令查看当前位置

 7.切换到/home，利用pwd命令查看当前位置

 8.切换到/etc，利用pwd命令查看当前位置

 9.切换到/proc，利用pwd命令查看当前位置

 10.切换到/etc/pki，利用pwd命令查看当前位置,再利用cd ..进行返回上一层目录

 

案例：主机名与查看网卡命令练习

 1.显示当前系统主机名

 2.修改当前系统的主机名为svr.tedu.cn

 3.查看当前系统网卡IP地址信息

 

案例：创建命令练习

 1.请在/root创建三个目录分别为student、file、nsd18

 2.请在/opt创建三个文本文件分别为1.txt、a.txt、nsd.txt

 

案例：vim练习

 1.利用vim编辑/opt/Linux.txt，写入内容“I Love Studying Linux” 并用cat查看文件内容验证结果

 2.利用vim编辑/etc/myhost文件，写入内容“I Love Dc” ，并用cat查看文件内容验证结果

 3.利用vim编辑/etc/mystu.txt文件，写入内容“好好学习，天天向上” ，并用cat查看文件内容验证结果

