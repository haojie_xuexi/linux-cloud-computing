## 04:正则表达式、sed基本用法、sed文本块处理



### **1，** **字符串的截取**

### **2，字符串的替换**

```shell
${变量名称/被替换内容/新内容}

a=1234		//定义素材

echo ${a/3/6}   //把3换成6

a=11223344   //定义素材

echo ${a/3/6}   //把3换成6,默认只换一个

echo ${a//3/6}  //使用两个/可以实现把所有3换成6

a=333444   //定义素材

echo ${a//3/}  //把所有3换成空，相当于删除
```

 

### **3，** **字符串的删除**

**${变量名称#被删除的内容}从左往右删除**

**${变量名称%被删除的内容} 从右往左删除**

```shell
a=abcdefghijklmn

echo ${a#abcdefgh}	 //从左往右删除到h

echo ${a#*h}		//效果同上，更精简

echo ${a#ab}  //从左往右删除到b

echo ${a#*b}  //效果同上，更精简

a=abcxyzabcxyz  

echo ${a##*b}  //从左往右删除到最后一个b
```

 

```shell
a=abcxyzabcxyz

 echo ${a%abcxyz}  //从右往左删除abcxyz

 echo ${a%a*}   //效果同上，更精简

 echo ${a%%a*}  //从右往左删除到最后一个a

 echo ${a%%y*}  //从右往左删除到最后一个y
```

编写脚本，实现批量修改文件的扩展名

```shell
touch abc{1..10}.txt  //创建10个文件作为素材
#!/bin/bash

for i in $(ls *.txt)		//找到所有txt文件交给for循环

do

    n=${i%.*}  //先用去尾删除扩展名

    mv $i $n.doc  //再将原文件修改为doc扩展名

done
```

 

**变量初值（备用值）的定义**

${变量名:-初值}   //如果变量有值则使用本身的值，如果变

量为空，则使用初值

编写脚本，可以创建用户与配置密码，密码可以自定义，也

可以使用默认的123456

```shell
#!/bin/bash

read -p "请输入用户名：" n

useradd $n

read -p "请输入密码："  p

echo ${p:-123456} | passwd --stdin $n  //当用户没有输入密码时

密码就是123456 				
```



\--------------------------------------------------

### **正则表达式，使用若干符号配合某工具实现对文档的过滤、查、找、修改等功能**

```shell
head -5 /etc/passwd > user  //准备素材

 grep bin user  //找有bin的行

 grep ^bin user	//找以bin开头的行

 grep bash user  //找有bash的行

 grep bash$ user  //找以bash结尾的行

 vim user   //编辑文档添加空行

 grep  -n  ^$  user  //找空行，加n选项可以显示行号
```

 

```shell
grep "[root]" user  //找root四个字符任意一个

 grep "[rot]" user	 //效果同上，找rot任意一个字符

 grep "rot" user  //找连续的rot字符串

 grep "[a-z]" user  //找所有小写字母

 grep "[A-Z]" user  //找所有大写字母

 grep "[a-Z]" user  //找所有字母

 grep "[0-9]" user  //找所有数字

 grep "[^a-Z]" user  //找字母之外的内容，^写在[]里是取反效果
```

 

```shell
 grep "r..t" user  //找rt之间有2个任意字符的行

 grep "r...t" user  //找rt之间有3个任意字符的行		

 grep "*" user  //*号是匹配前一个字符任意次，不能单独使用

 grep "ro*t" user  //找rt，中间的o有没有都行

 grep "." user  //找任意单个字符

 grep ".*" user  //找任意  
```

 

```shell
grep "ro\{2,4\}t" user  //找rt，中间的o可以是2~4个

 grep "ro\{2,\}t" user	//找rt，中间的o可以是2个以及2个以上

 grep "ro\{3,7\}t" user //找rt，中间的o可以是3~7个，没有匹

配条件就没有任何显示

 grep "ro\{3,\}t" user  //找rt，中间的o可以是3个以及3个以

上

 grep "ro\{2\}t" user  //找rt，中间的o必须是2个
```

 

```shell
 grep "ro\{1,\}t" user   //找1次以及1次以上的o

 egrep "ro{1,}t" user  //效果同上

 egrep "ro+t" user   //效果同上，最精简

grep "roo\{0,1\}t" user  //第二个o要出现0~1次

egrep "roo{0,1}t" user  //效果同上

egrep "roo?t" user  //效果同上，最精简

grep "ro\{2\}t" user   //找o出现2次的

egrep "ro{2}t" user   //效果同上
```

 

```shell
 egrep "^root|^bin" user  //找root或者以bin开头的行

 egrep "^(root|bin)" user  //效果同上

 egrep "\bthe\b" abc   //找the，前后不允许出现数字，字母，下划线
```

 

\-------------------------------------------------------

### **sed 流式编辑器，可以对文档进行非交互式增删改查，逐行处理**

**适用方式：**

**1，前置指令 | sed 选项  定址符 指令**

**2，sed 选项  定址符 指令  被处理文档**

**选项 -n 屏蔽默认输出 -i写入文件  -r支持扩展正则**

**指令 p输出  d删除   s替换**

```shell
 sed -n '1p' user		//输出第1行

  sed -n '2p' user   //输出第2行

  sed -n '3p' user   //输出第3行

  sed -n '2,4p' user  //输出第2~4行

  sed -n '2,+1p' user  //输出第2行以及后面1行

  sed -n '2p;4p' user  //输出第2行，第4行
```

\------------------------------------------

```shell
sed -n '/^root/p' user  //在sed中使用正则表达式输出以root开

头的行

  grep "^root" user  //效果同上

 egrep "^root|^bin" user  //找root或者bin开头的行  

 sed -nr '/^root|^bin/p' user  //在sed中查找，-r使用扩展正

则，效果同上

  sed -n '=' user   //查看所有行号

 sed -n '$=' user  //查看最后一行的行号，相当于查看文档

总共有几行
```

\---------------------------------------------

```shell
 sed  '1d' user  //删第1行

 sed  'd' user  //删所有

 sed  '3d' user  //删第3行

 sed  '2,5d' user  //删2~5行

 sed  '4,+2d' user  //删4行以及后面2行

 sed  '1d;3d' user  //删第1行，第3行

sed  '$d' user  //删除最后一行

 sed -n '$p' user  //查看最后一行

sed -n '1!p' user  //查看除了第1行以外的行, !代表取反
```

 

\---------------------------------------------

练习：

regular_express.txt  是素材，拷贝到linux中

regular.pdf是题目，按题目使用正则表达式完成

题目要求

https://gitee.com/nsd-tedu/nsd2008



 

\------------------------------------------------

练习：

1 简述grep工具的-q选项的含义（egrep同样适用）。

2 正则表达式中的+、？、*分别表示什么含义？

3 如何编写正则表达式匹配11位的手机号？

4 简述sed定址符的作用及表示方式。

 

 

参考答案：

1 简述grep工具的-q选项的含义（egrep同样适用）。

```
选项-q的作用是静默、无任何输出，效果类似于正常的grep操作添加了&> /dev/null来屏蔽输出
```

2 正则表达式中的+、？、*分别表示什么含义？

```
这三个字符用来限制前面的关键词的匹配次数，含义分别如下：

•	+：最少匹配一次，比如a+可匹配a、aa、aaa等

•	？：最多匹配一次，比如a?可匹配零个或一个a

•	*：匹配任意多次，比如a*可匹配零个或任意多个连续的a
```

3 如何编写正则表达式匹配11位的手机号？

准备测试文件：

```shell
1.	[root@svr5 ~]# cat tel.txt 

2.	01012315

3.	137012345678

4.	13401234567

5.	10086

6.	18966677788

提取包含11位手机号的行：

1.	[root@svr5 ~]# egrep '^1[0-9]{10}$' tel.txt 

2.	13401234567

3.	18966677788
```

4 简述sed定址符的作用及表示方式。

```
作用：定址符（执行指令的条件）控制sed需要处理文本的范围；不加则逐行处理所有行

表示方式：定址符可以使用行号或正则表达式
```













