## 07：VPN服务器、systemd

![img](./07image/001.png)

### **vpn** **虚拟专用网络**

#### 方式一：使用gre技术实现vpn(两台linux)

Web1主机配置：

```shell
modprobe ip_gre  //在内核中开启gre模块功能

[root@web1 ~]# lsmod | grep ip_gre //查询

[root@web1 ~]#ip tunnel add tun0 mode gre remote 192.168.2.200 local 192.168.2.100 //创建vpn隧道，名字叫tun0，使用gre技术，与2.200连接，自身ip是2.100

[root@web1 ~]#ip link show //查看

[root@web1 ~]#ip addr add 10.10.10.10/24 peer 10.10.10.5/24 dev tun0 //在tun0隧道中使用私有ip地址，本机是10.10，远程主机是10.5

[root@web1 ~]# ip addr show //查看

[root@web1 ~]# ip link set tun0 up //激活之前的配置

[root@web1 ~]# ip tunnel del tun0 //如果配置错误，要删除重来
```

 

web2主机也按照上述配置创建vpn，但是ip地址要反着写

 

测试：

selinux都要关闭，相互ping 10网段的ip

 

\-----------------------------------------------------------------------

#### 方式二：搭建pptp vpn

```shell
cd ~/lnmp_soft/vpn

yum -y install pptpd-1.4.0-2.el7.x86_64.rpm

[root@web1 vpn]# vim /etc/pptpd.conf 修改102、103行

localip 192.168.2.100   本机ip

remoteip 10.10.10.10-18  客户机分配的ip范围

[root@web1 vpn]# vim /etc/ppp/options.pptpd  在66行定义用户的dns服务器

地址

ms-dns 8.8.8.8

[root@web1 vpn]# vim /etc/ppp/chap-secrets

tom * 123456 *


systemctl restart pptpd

[root@web1 vpn]# ss -ntulp | grep pptpd
```

 

之后使用windows系统连接服务器

用户名tom下面的密码是123456

![image-20201023173350042](./07image/002.png)

![image-20201023173356745](./07image/003.png)

#### 方式三：L2TP+IPSec

```shell
[root@web1 vpn]# yum -y install libreswan

[root@web1 vpn]# cp myipsec.conf /etc/ipsec.d/

[root@web1 vpn]# vim /etc/ipsec.d/myipsec.conf

修改第16vim行为本机ip

left=192.168.2.100

[root@web1 vpn]# vim /etc/ipsec.secrets

192.168.2.100  %any:  PSK "randpass"

2.100是本机ip，%any:是任何客户端可以连接本机，PSK预共享

秘钥，密码是randpass

[root@web1 vpn]# yum -y install xl2tpd-1.3.8-2.el7.x86_64.rpm

[root@web1 vpn]# vim /etc/xl2tpd/xl2tpd.conf  //修改32、33行

ip range = 9.9.9.9-9.9.9.18 //分配给客户端的IP池

local ip = 192.168.2.100 

[root@web1 vpn]# vim /etc/ppp/options.xl2tpd  //修改配置，把

10与16行注释掉，21行删除注释与空格

[root@web1 vpn]# vim /etc/ppp/chap-secrets //如果添加账户，修改

该文件

tom * 123456 *

[root@web1 vpn]# systemctl start ipsec

[root@web1 vpn]# systemctl start xl2tpd

 ss -ntulp | grep xl2tpd

 ss -ntulp | grep :500
```

 

然后使用Windows链接，预共享密钥使用randpass，后面的用

户名和密码使用tom的即可

![image-20201023173437488](./07image/004.png)

\---------------------------------------------------------------------

![img](./07image/005.png)

### systemd

```shell
vim /usr/lib/systemd/system/crond.service

复制内容

vim /usr/lib/systemd/system/test.service

粘贴到新文件中

[Unit]

Description=test  //描述

After=time-sync.target //在哪个服务启动之后再启动test服务

[Service]

ExecStart=/opt/test.sh //如果执行了systemctl start test之后就运

行/opt/test.sh程序，提前要设置x权限

ExecReload=/bin/kill -HUP $MAINPID //如果执行了systemctl reload 

test 之后，就利用kill程序发送不停止服务仅仅更新配置的信号，其

效果相当于重新加载配置

KillMode=process //如果关闭服务，就停止主进程，比如在输入systemctl stop test 时。

[Install]

WantedBy=multi-user.target  //支持开机自启


```

```
systemctl start test

systemctl status test
```

\-------------------------------------------------------------

#### 配置systemctl管理nginx服务

```shell
cd /usr/lib/systemd/system/

[root@web1 system]# cp httpd.service nginx.service

[root@web1 system]#vim nginx.service

[Unit]

Description=nginx  描述

After=network.target remote-fs.target nss-lookup.target  在网络服务、远程文件系统服务、域名相关服务启动之后再开启nginx

[Service]

Type=forking  由于nginx是多进程服务，要设置forking

ExecStart=/usr/local/nginx/sbin/nginx 当执行systemctl start nginx时，所执行的命令

ExecReload=/usr/local/nginx/sbin/nginx -s reload 当执行systemctl reload nginx时，所执行的命令

ExecStop=/bin/kill -s QUIT ${MAINPID} 当执行systemctl stop nginx时，所执行的命令,-s QUIT是发送退出信号，${MAINPID}是nginx的主进程号

[Install]

WantedBy=multi-user.target   支持开机自启
```

\----------------------------------------------

```shell
[root@web1 system]# systemctl start nginx  开启nginx

如果不好用就重启服务器

[root@web1 system]# systemctl start nginx 开启

[root@web1 system]# systemctl status nginx  查看状态
```



# 

