## 02：Web基础应用、NFS服务基础、触发挂载

### 一、 两台虚拟机进行环境准备

#### 1.SELinux设置宽松模式

```shell
[root@svr7 ~]# setenforce  0  #设置当前系统SELinux为宽松

[root@svr7 ~]# getenforce     #查看当前系统SELinux模式

Permissive

[root@svr7 ~]# vim  /etc/selinux/config

SELINUX=permissive
```

2.防火墙设置

```shell
]# firewall-cmd  --set-default-zone=trusted
```



### 二、 Web服务器

• 基于 B/S （Browser/Server）架构的网页服务

– 服务端提供网页

– 浏览器下载并显示网页

• 实现Web服务的软件:httpd（Apache）、Nginx、tomcat

• Hyper Text Markup Language(html)，超文本标记语言

• Hyper Text Transfer  Protocol(http)，超文本传输协议



### 三、 构建Web服务

虚拟机A：

#### 1.安装httpd软件(Apache基金会)

```shell
[root@svr7 ~]# yum  -y  install  httpd
[root@svr7 ~]# rpm -q  httpd
```

#### 2.书写一个页面文件

```shell
]# ls  /var/www/html/
]# echo NSD2008 Web > /var/www/html/index.html
```

#### 3.重启httpd服务

```shell
]# > /etc/resolv.conf     #清空文件内容，加快httpd启动

]# systemctl restart httpd

]# curl  http://192.168.4.7     #测试访问

NSD2008 Web
```

### 四、 Web服务器配置

三步走：装包、配置、启服务

配置文件: /etc/httpd/conf/httpd.conf

排错思路：

```shell
[root@svr7 ~]# systemctl restart httpd    

Job for httpd.service failed because the control process exited with error code. See "systemctl status httpd.service" and "journalctl -xe" for details.

[root@svr7 ~]# journalctl  -xe
-- Unit httpd.service has begun starting up.
9月 16 10:22:10 svr7.tedu.cn httpd[12652]: ***AH00526: Syntax error on line 3 of /etc/httpd/conf/httpd.conf:***
9月 16 10:22:10 svr7.tedu.cn httpd[12652]: Invalid command 'configuration', perhaps misspelled or defined by a module not
9月 16 10:22:10 svr7.tedu.cn systemd[1]: httpd.service: main process exited, code=exited, status=1/FAILURE
9月 16 10:22:10 svr7.tedu.cn kill[12653]: kill: cannot find process ""
9月 16 10:22:10 svr7.tedu.cn systemd[1]: httpd.service: control process exited, code=exited status=1
9月 16 10:22:10 svr7.tedu.cn systemd[1]: Failed to start The Apache HTTP Server.
-- Subject: Unit httpd.service has failed
```

#### • 提供的默认配置

> – Listen：监听地址:端口（80）**
>
> – ServerName：本站点注册的DNS名称（空缺）**
>
> – DocumentRoot：网页根目录（/var/www/html）**
>
> – DirectoryIndex：起始页/首页文件名（index.html）

 

####  DocumentRoot：网页文件根目录（/var/www/html）

网页文件的根目录：存放网页文件的路径，也是httpd软件寻找网页文件的路径

虚拟机A

```shell
]# vim   /etc/httpd/conf/httpd.conf

此处省略一万字……
119行   DocumentRoot   "/var/www/myweb"
此处省略一万字……

]# mkdir  /var/www/myweb
]# echo wo shi myweb > /var/www/myweb/index.html
]# systemctl restart httpd  #重启httpd服务
]# curl http://192.168.4.7
wo shi myweb
```



#### 针对网页文件存放路径，具有访问控制

默认情况下子目录默认继承父目录的访问控制

除非针对子目录有单独的访问控制规则

默认情况下只有/var/www/下是允许所有人访问

```shell
<Directory  />          #针对/进行访问控制
  Require all denied   #拒绝所有客户端访问
</Directory>
```



```
<Directory  "/var/www">  #针对/var/www进行访问控制
  Require all granted   #允许所有客户端访问
</Directory>
```

 

虚拟机A：

```shell
[root@svr7 ~]# mkdir    /abc
[root@svr7 ~]# echo wo shi abc  >  /abc/index.html

[root@svr7 ~]# vim   /etc/httpd/conf/httpd.conf
此处省略一万字……
119行   DocumentRoot   "/abc"
此处省略一万字……
<Directory   "/abc">          #针对/abc进行访问控制
    Require  all  granted      #允许所有人访问
</Directory>
此处省略一万字……
[root@svr7 ~]# systemctl   restart   httpd
[root@svr7 ~]# curl  192.168.4.7
wo shi abc 
```

 

#### 实际路径与网络路径

实际路径：网页文件存放在服务器上路径

网络路径：在浏览器输入的路径



> 客户端 firefox  192.168.4.7--->http协议进行访问192.168.4.7--->服务端 由httpd软件进行接收--->DocumentRoot /abc--->/abc/index.html

网络路径: firefox  192.168.4.7

实际路径：/abc/

 

网络路径: firefox  192.168.4.7/nsd

实际路径：/abc/nsd

 

网络路径: firefox  http://192.168.4.7/abc/nsd/test

实际路径：/abc/abc/nsd/test



DocumentRoot /abc

网络路径: firefox  http://192.168.4.7/abc/nsd/test

实际路径：/abc/abc/nsd/test

 

虚拟机A：

```shell
[root@svr7 ~]# mkdir  /abc/nsd

[root@svr7 ~]# echo wo shi nsd > /abc/nsd/index.html

[root@svr7 ~]# curl  192.168.4.7/nsd/
```

 

####  Listen：监听地址:端口（80）

端口：数字编号，标识作用。标识程序与协议

 达外理发店：珠市口大街44号(IP地址)

​     理发师：10（端口）

​     http协议默认端口：80



```shell
客户端 firefox  192.168.4.7--->http协议进行访问192.168.4.7--->服务端 由httpd软件进行接收--->DocumentRoot /abc--->/abc/index.html
```



虚拟机A

```shell
[root@svr7 ~]# vim /etc/httpd/conf/httpd.conf
此处省略一万字……
42行  Listen  80
43行  Listen  8000
此处省略一万字……
[root@svr7 ~]# systemctl restart httpd
[root@svr7 ~]# curl  192.168.4.7
wo shi abc
[root@svr7 ~]# curl  192.168.4.7:8000    #指定端口号
wo shi abc
```



### 五、 Web服务器配置-虚拟Web主机

#### • 虚拟Web主机

– 由同一台服务器提供多个不同的Web站点

#### • 区分方式

– 基于域名的虚拟主机       

– 基于端口的虚拟主机

– 基于IP地址的虚拟主机

 

#### • 为每个虚拟站点添加配置

```shell
<VirtualHost  IP地址:端口>

  ServerName  此站点的DNS名称

  DocumentRoot  此站点的网页根目录

</VirtualHost>
```

#### • 配置文件路径

```shell
– /etc/httpd/conf/httpd.conf   #主配置文件

– /etc/httpd/conf.d/*.conf    #调用配置文件
```

虚拟机A：

1.建立调用配置文件

```shell
[root@svr7 ~]# vim   /etc/httpd/conf.d/nsd01.conf

<VirtualHost   *:80>      #启用虚拟Web主机的功能

 ServerName   www.baidu.com    #设置网站名称

 DocumentRoot   /var/www/baidu  #设置网页文件存放路径

</VirtualHost>           #配置结束

<VirtualHost   *:80>

 ServerName   www.qq.com

 DocumentRoot   /var/www/qq

</VirtualHost>

]# mkdir  /var/www/baidu

]# mkdir  /var/www/qq

]# echo woshi baidu > /var/www/baidu/index.html

]# echo woshi QQ > /var/www/qq/index.html

]# systemctl restart httpd
```

 

2.直接为本机提供域名解析/etc/hosts   

```shell
[root@svr7 ~]# vim   /etc/hosts

此处省略一万字……

192.168.4.7   www.qq.com   www.baidu.com

[root@svr7 ~]# curl  www.qq.com

woshi QQ

[root@svr7 ~]# curl  www.baidu.com

woshi baidu
```

 一旦使用虚拟Web主机功能，那么所有的Web页面都必须用虚拟Web主机功能进行呈现

 

### 六、 构建NFS共享服务

 

#### • Network File System，网络文件系统

– 用途：为客户机提供共享使用的文件夹

– 协议：NFS（TCP/UDP 2049）、RPC（TCP/UDP 111）

#### • 服务端与客户端所需软件包：nfs-utils

#### • 系统服务：nfs-server

 

虚拟机A

1.安装软件包

```shell
[root@svr7 ~]# rpm  -q  nfs-utils

nfs-utils-1.3.0-0.54.el7.x86_64

[root@svr7 ~]# mkdir  /public   #创建用于共享的目录

[root@svr7 ~]# touch  /public/abc.txt

[root@svr7 ~]# ls  /public/

abc.txt
```

3.修改配置文件                   

```shell
[root@svr7 ~]# vim  /etc/exports

/public     *(ro)  

共享目录    客户端的地址(权限)
```

 

4.启动服务

```shell
]# systemctl  restart  rpcbind   #动态端口，依赖的服务

]# systemctl  restart  nfs-server
```

 

虚拟机B

1.安装软件包nfs-utils

```shell
[root@pc207 ~]# rpm  -q  nfs-utils

nfs-utils-1.3.0-0.54.el7.x86_64
```

2.挂载访问

```shell
[root@pc207 ~]# mkdir   /mynfs

[root@pc207 ~]# mount  192.168.4.7:/public   /mynfs

[root@pc207 ~]# df  -h   |   tail  -1

[root@pc207 ~]# ls   /mynfs
```

3.开机自动挂载

>  **_netdev：声明网络设备，开机启动时，首先将网络参数配置之后再挂载本设备**

```shell
[root@pc207 ~]# vim   /etc/fstab

192.168.4.7:/public  /mynfs  nfs  defaults,_netdev  0 0

[root@pc207 ~]# umount  /mynfs

[root@pc207 ~]# mount -a

[root@pc207 ~]# df  -h  |  tail -1

192.168.4.7:/public    17G  3.5G  14G  21% /mynfs
```

 

### 七、触发挂载(针对于客户端设置)

 

•由 autofs 服务提供的“按需访问”机制     

–只要访问挂载点，就会触发响应，自动挂载指定设备

–闲置超过时限（默认5分钟）后，会自动卸载

 触发挂载需要两级目录：

​    第一级目录为监控目录，第二级目录为挂载点

 

 •主配置文件 /etc/auto.master

–监控点目录  挂载配置文件的路径

 /misc     /etc/auto.misc

  

•挂载配置文件，比如 /etc/auto.misc

–触发点子目录   -挂载参数   :设备名

 

 虚拟机B：  

```shell
 [root@pc207 ~]# yum -y install autofs

 [root@pc207 ~]# systemctl restart autofs

 [root@pc207 ~]# ls  /misc

 [root@pc207 ~]# ls  /misc/cd

```

 虚拟机B：监控目录为/nsd20 ,挂载点目录dc 触发挂载的设备为/dev/cdrom

 1.手动创建监控目录

```
 [root@pc207 /]# mkdir  /nsd20
```

 2.修改主配置文件

```
 [root@pc207 /]# vim  /etc/auto.master
```

 ..........此处省略一万字

```
 /nsd20   /opt/xixi.txt     #指定监控目录为/nsd20
```

 3.建立挂载配置文件

```shell
[root@pc207 /]# cp  /etc/auto.misc   /opt/xixi.txt

[root@pc207 /]# vim  /opt/xixi.txt     #该文件只需要保留一行内容

 dc    -fstype=iso9660    :/dev/cdrom

 挂载点目录    设备的文件系统类型   挂载的设备

[root@pc207 /]# systemctl  restart  autofs
```

4.测试

```
[root@pc207 /]# ls /nsd20

[root@pc207 /]# ls /nsd20/dc

[root@pc207 /]# ls /nsd20
```

二、触发挂载进阶

监控目录为/nsd20 ,挂载点目录tc 触发挂载的设备为192.168.4.7:/public

**虚拟机B：**

1.建立挂载配置文件

```shell
 [root@pc207 /]# vim  /opt/xixi.txt    

dc   -fstype=iso9660  :/dev/cdrom

tc   -fstype=nfs    192.168.4.7:/public

[root@pc207 /]# systemctl  restart  autofs


[root@pc207 /]# ls /nsd20

[root@pc207 /]# ls /nsd20/dc

[root@pc207 /]# ls /nsd20/tc

[root@pc207 /]# ls /nsd20
```

 

三、链路聚合

 网卡team（网卡组队、链路聚合）

 作用：备份网卡设备，提高可靠性

​              eth0         eth1

​         虚拟网卡：team0  192.168.1.1

1.关闭虚拟机添加两块网卡

![img](./02image/001.png) 

 

![img](./02image/002.png) 

 

 

![img](./02image/003.png) 

 

![img](./02image/004.png) 

```
[root@A ~]# ifconfig  |  less
```

2.建立虚拟网卡设备，参考 man teamd.conf  进行全文查找 /example

```shell
[root@A ~]# nmcli  connection   add   type   team  ifname  team0  con-name  team0           

 autoconnect  yes  config   '{"runner": {"name": "activebackup"}}'

[root@A ~]# nmcli   连接      添加   类型   组队  网卡名  team0   配置文件名  team0

  每次开机自启动   工作方式   活跃备份

[root@A ~]# ifconfig
```

3.添加成员

```shell
[root@A ~]# nmcli connection add type team-slave autoconnect yes ifname eth1 con-name team0-1  master team0

[root@A ~]# nmcli connection add type team-slave autoconnect yes ifname eth2 con-name team0-2  master team0

[root@A ~]# nmcli  连接     添加  类型  组队-成员  每次开机自动启用  网卡名 eth2  配置文件名  team0-2   主设备为  team0
```

4.激活，以配置文件的名进行激活

```shell
successfully（成功）

[root@A ~]# nmcli connection up team0

[root@A ~]# nmcli connection up team0-1

[root@A ~]# nmcli connection up team0-2
```

5.为team0配置IP地址

```shell
[root@A ~]# nmcli  connection  modify  team0  ipv4.method  manual  ipv4.addresses 192.168.1.1/24 connection.autoconnect  yes

[root@A ~]# nmcli  connection  up  team0

[root@A ~]# teamdctl team0 state    #专门查看team0状态的命令

[root@A ~]# ifconfig eth1 down

[root@A ~]# teamdctl team0 state 

setup:

 runner: activebackup

ports:

 eth1

  link watches:

   link summary: down

   instance[link_watch_0]:

    name: ethtool

    link: down

   down count: 1

 eth2

  link watches:

   link summary: up

   instance[link_watch_0]:

    name: ethtool

    link: up
    
    down count: 0

runner:

 active port: eth2
```



#### 课后习题：SELinux与防火墙进行关闭

#### 案例1:为虚拟机A 配置以下虚拟Web主机

\- 实现三个网站的部署

\- 实现客户端访问server0.example.com网页内容为 大圣归来

\- 实现客户端访问www0.example.com网页内容为  大圣又归来

\- 实现客户端访问webapp0.example.com网页内容为 大圣累了

虚拟机A：

```shell
[root@A ~]# firewall-cmd --set-default-zone=trusted
[root@A ~]# setenforce 0
[root@A ~]# yum -y install httpd
[root@A ~]# mkdir /var/www/nsd01
[root@A ~]# mkdir /var/www/nsd02
[root@A ~]# mkdir /var/www/nsd03
[root@A ~]# echo 大圣归来 > /var/www/nsd01/index.html
[root@A ~]# echo 大圣又归来 > /var/www/nsd02/index.html
[root@A ~]# echo 大圣累了 > /var/www/nsd03/index.html
[root@A ~]# vim /etc/httpd/conf.d/vhost.conf
<VirtualHost *:80>
ServerName server0.example.com
DocumentRoot /var/www/nsd01
<\VirtualHost>
<VirtualHost *:80>
ServerName www0.example.com
DocumentRoot /var/www/nsd02
<\VirtualHost>
<VirtualHost *:80>
ServerName webapp0.example.com
DocumentRoot /var/www/nsd03
<\VirtualHost>
[root@A ~]# systemctl restart httpd

```

 虚拟机B：测试

```shell
[root@B ~]# vim /etc/hosts
192.168.4.7 server0.example.com www0.example.com webapp0.example.com
[root@B ~]# curl server0.example.com
[root@B ~]# curl www0.example.com
[root@B ~]# curl webapp0.example.com
```



#### 案例2:为虚拟机A 使用自定Web根目录

调整 Web 站点 http://www0.example.com 的网页文件目录，要求如下：

1）新建目录 /webroot，作为此站点新的网页文件目录（Web访问控制）

2）确保站点 http://www0.example.com  仍然可访问

虚拟机A

```shell
[root@A ~]# vim /etc/httpd/conf.d/vhost.conf
...
<VirualHost *:80>
ServerName www0.example.com
DocumentRoot /webroot
</VirualHost>
...
[root@A ~]# mkdir /webroot
[root@A ~]# echo 'this is webroot' > /webroot/index.html
[root@A ~]# vim /etc/httpd/conf.d/myacl.conf
<Directory /webroot>
 Require all granted
</Directory>
[root@A ~]# systemctl restart httpd

虚拟机B:
[root@B ~]# curl www0.example.com
```



#### 案例3：为虚拟机A 部署站点

为站点 webapp0.example.com 进行配置，要求如下：

1）此虚拟主机侦听在端口8909

2）从浏览器访问 http://webapp0.example.com:8909 

虚拟机A:

```shell
[root@A ~]# vim /etc/httpd/conf.d/vhost.conf
...
Listen 8909
<VirualHost *:8909>
ServerName webapp0.example.com
DocumentRoot /var/www/nsd03
...
[root@A ~]# systemctl restart httpd
```

 虚拟机B

```shell
[root@B ~]# curl webapp0.example.com:8909
大圣累了
```



#### 案例4：普通NFS共享的实现

1.在虚拟机A上配置NFS服务，只读的方式共享目录 /public

2.在虚拟机B上访问NFS共享目录

 a）将虚拟机A 的 /public 挂到本地 /mnt/nfsmount 

 b）这些文件系统在系统启动时自动挂载

虚拟机A：

```shell
[root@A ~]# yum -y install nfs-utils
[root@A ~]# mkdir /public
[root@A ~]# touch /public/1.txt
[root@A ~]# ls /public
[root@A ~]# vim /etc/exports
/public *(ro)
[root@A ~]# systemctl restart rpcbind #先重启
[root@A ~]# systemctl restart nfs-server #后重启
[root@A ~]# 
```

 虚拟机B：

```shell
[root@B ~]# mkdir /mnt/nfsmount
[root@B ~]# vim /etc/fstab
192.168.4.7:/public /mnt/nfsmount nfs defaults,_netdev 0 0
[root@B ~]# mount -a
[root@B ~]# df -h | tail -1
192.168.4.7:/public       17G  3.5G   14G   21% /mnt/nfsmount
```

 