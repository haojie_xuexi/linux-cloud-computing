## 05：批量装机环境、配置PXE引导、Kickstart自动应答、Cobbler装机平台



### 一、 环境的准备

**关闭虚拟机A的SELinux**

```shell
[root@svr7 ~]# setenforce  0    #修改当前运行模式

[root@svr7 ~]# getenforce     #查看当前运行模式

Permissive

[root@svr7 ~]# vim  /etc/selinux/config  #永久修改

SELINUX=permissive
```

**设置虚拟机A防火墙**

```shell
[root@svr7 ~]# firewall-cmd  --set-default-zone=trusted
```



### 二、 构建DHCP服务器

• Dynamic Host Configuration Protocol

– 动态主机配置协议，由 IETF（Internet 网络工程师任务小组）组织制定，用来简化主机地址分配管理

• 主要分配以下入网参数

– IP地址/子网掩码/广播地址

– 默认网关地址、DNS服务器地址

 

• DHCP地址分配的四次会话(以广播形式进行，先到先得)

– DISCOVERY --> OFFER --> REQUEST -->ACK

• 一个网络中只能有一台DHCP服务器

 

1.安装软件包

```shell
[root@svr7 /]# yum  -y  install  dhcp
```

2.修改配置文件

```shell
[root@svr7 /]# vim  /etc/dhcp/dhcpd.conf

末行模式下  :r  /usr/share/doc/dhcp*/dhcpd.conf.example

subnet  192.168.4.0  netmask  255.255.255.0  {  #分配网段

 range  192.168.4.100   192.168.4.200;    #分配IP地址范围

 option  domain-name-servers  192.168.4.7;  #分配DNS

 option  routers  192.168.4.254;  #分配的网关地址

 default-lease-time  600;

 max-lease-time  7200;

}

[root@svr7 /]# systemctl  restart  dhcpd
```



### 三、 网络装机服务器简介

• 规模化：同时装配多台主机

• 自动化：装系统、配置各种服务

• 远程实现：不需要光盘、U盘等物理安装介质

• PXE，Pre-boot eXecution Environment

• 预启动执行环境，在操作系统之前运行

• 可用于远程安装

• 工作模式

• PXE client 集成在网卡的启动芯片中

• 当计算机引导时，从网卡芯片中把PXE client调入内存执行，获取PXE server配置、显示菜单，根据用户选择将远程引导程序下载到本机运行

 

• 网络装机服务器:

– DHCP服务，分配IP地址、定位引导程序

– TFTP服务，提供引导程序下载

– HTTP服务（或***\*FTP\****/NFS），提供yum安装源

 

![img](./05image/001.png) 



### 四、 配置DHCP服务

```shell
[root@svr7 /]# vim  /etc/dhcp/dhcpd.conf

此处省略一万字……

 default-lease-time 600;

 max-lease-time 7200;

 next-server   192.168.4.7;   #下一个服务器的IP地址

 filename  “pxelinux.0”;   #指明网卡引导文件名称

}

[root@svr7 /]# systemctl  restart  dhcpd
```

pxelinux.0：网卡引导文件（网络装机说明书）

​        二进制文件，安装一个软件可以获得该文件



### 五、 配置tftp服务，传输众多的引导文件

tftp:简单的文件传输协议  默认端口：69

​    tftp默认共享的主目录:/var/lib/tftpboot

#### **安装软件**

```shell
[root@svr7 /]# yum  -y  install  tftp-server

[root@svr7 /]# systemctl  restart  tftp
```

 

####  **部署pxelinux.0文件**

```shell
]# yum  provides  */pxelinux.0  #查询哪个包产生该文件

]# yum -y install syslinux   #安装syslinux软件包

]# rpm -ql syslinux  |  grep  pxelinux.0   #查询软件包安装清单

]# cp  /usr/share/syslinux/pxelinux.0   /var/lib/tftpboot/

]# ls  /var/lib/tftpboot/

pxelinux.0  
```

 

总结思路:

1. DHCP服务: IP地址、next-server、filename pxelinux.0

   2.tftp服务: pxelinux.0

2. pxelinux.0： /var/lib/tftpboot/pxelinux.cfg/default(默认菜单文件)

 

####  **部署菜单文件(将光盘中的菜单文件进行复制)**

```shell
[root@svr7 /]# ls  /mydvd/isolinux/

[root@svr7 /]# mkdir  /var/lib/tftpboot/pxelinux.cfg

[root@svr7 /]# ls /var/lib/tftpboot/

pxelinux.0  pxelinux.cfg

[root@svr7 /]# cp  /mydvd/isolinux/isolinux.cfg   /var/lib/tftpboot/pxelinux.cfg/default

[root@svr7 /]# ls  /var/lib/tftpboot/pxelinux.cfg/
```

 

#### **部署图形模块(vesamenu.c32)与背景图片（splash.png）**

```shell
[root@svr7 /]# cp  /mydvd/isolinux/vesamenu.c32    /mydvd/isolinux/splash.png   /var/lib/tftpboot/

[root@svr7 /]# ls  /var/lib/tftpboot/

pxelinux.0   splash.png

pxelinux.cfg  vesamenu.c32
```

 

#### **部署启动内核(vmlinuz)与驱动程序（initrd.img）**

```shell
[root@svr7 /]# cp  /mydvd/isolinux/vmlinuz  /mydvd/isolinux/initrd.img  /var/lib/tftpboot/

[root@svr7 /]# ls   /var/lib/tftpboot/

initrd.img  pxelinux.cfg  vesamenu.c32

pxelinux.0  splash.png   vmlinuz
```

 

####  **修改菜单文件内容**

```shell
[root@svr7 /]# vim  /var/lib/tftpboot/pxelinux.cfg/default

末行模式:set  nu开启行号功能

1 default  vesamenu.c32    #默认加载运行图形模块

2 timeout 600            #读秒时间60秒，1/10秒

此处省略一万字……..

10 menu background  splash.png       #背景图片

11 menu title  PXE  NSD2008  Server    #菜单界面的标题

此处省略一万字……..

61 label  linux

 62  menu label  ^Install  CentOS 7   #界面显示内容

 63  menu  default     #读秒结束后默认的选项

 64  kernel  vmlinuz      #加载内核

 65  append  initrd=initrd.img   #加载驱动程序

以下全部删除             
```

 

#### 总结思路:

1. DHCP服务: IP地址、next-server、filename pxelinux.0

   2.tftp服务: pxelinux.0

2. pxelinux.0： /var/lib/tftpboot/pxelinux.cfg/default(默认菜单文件)

3. default: 图形模块、背景图片、内核、驱动程序…..



### 六、 初步测试

####  **重启相关的服务**

```shell
[root@svr7 /]# systemctl  restart  dhcpd

[root@svr7 /]# systemctl  restart  tftp
```

####  **关闭VMware软件的DHCP服务**

![img](./05image/002.png) 

![img](./05image/003.png) 

 

![img](./05image/004.png) 

 

####  **新建虚拟机，内存2G，网络类型选项vmnet1**

![img](./05image/005.png) 

![img](./05image/006.png) 

![img](./05image/007.png) 

####  **菜单界面的显示**

![img](./05image/008.png) 

 

#### 排错思路：                

1. 查看DHCP服务配置文件

```shell
filename  "\pxelinux.0\";
```

2. 查看/var/lib/tftpboot目录内容

```shell
[root@svr7 /]# ls  /var/lib/tftpboot/

initrd.img  pxelinux.cfg  vesamenu.c32

pxelinux.0  splash.png   vmlinuz
```

3.菜单文件的名称

```shell
[root@svr7 /]# ls  /var/lib/tftpboot/pxelinux.cfg

default
```



### 七、 构建FTP服务，提供光盘内容

 FTP:文件传输协议   默认端口:21

 默认共享数据的主目录:/var/ftp

 

#### 1.安装软件包

```shell
[root@svr7 /]# yum -y  install  vsftpd

[root@svr7 /]# systemctl  restart  vsftpd
```



#### 2.建立挂载点

```shell
[root@svr7 /]# mkdir  /var/ftp/centos

[root@svr7 /]# mount  /dev/cdrom   /var/ftp/centos

mount: /dev/sr0 写保护，将以只读方式挂载

[root@svr7 /]# ls  /var/ftp/centos
```



#### 3.测试

```shell
[root@svr7 /]# curl   ftp://192.168.4.7/centos/
```



### 八、 实现无人值守安装，生成应答文件

#### **安装ystem-config-kickstart图形的工具**

```shell
[root@svr7 /]# yum -y install  system-config-kickstart

[root@svr7 /]# system-config-kickstart   #运行


system-config-kickstart程序需要Yum仓库的支持才能显示软件包的选择，必须要求Yum仓库的标识为[development]

[root@svr7 /]# vim  /etc/yum.repos.d/mydvd.repo 

[development]

name=centos7

baseurl=file:///mydvd

enabled=1

gpgcheck=0

[root@svr7 /]# system-config-kickstart 
```

#### 首先查看“软件包选择”是否可用

![img](./05image/009.png) 

 

—运行图形的工具system-config-kickstart 进行选择

**[root@svr7 ~]#  system-config-kickstart**

![img](./05image/010.png) 

 

**ftp://192.168.4.7/centos**

![img](./05image/011.png) 

 

![img](./05image/012.png) 

 

#### **重新划分新的分区**

![img](./05image/013.png) 

 

![img](./05image/014.png) 

 

![img](./05image/015.png) 

 

![img](./05image/016.png) 

 

![img](./05image/017.png) 

 

![img](./05image/018.png) 

 

 

![img](./05image/019.png) 

 

![img](./05image/020.png) 

 

![img](./05image/021.png) 

 

![img](./05image/022.png) 

 

![img](./05image/023.png) 

 

![img](./05image/024.png) 

 

```shell
[root@svr7 /]# ls  /root/ks.cfg    

/root/ks.cfg

[root@svr7 /]# vim  /root/ks.cfg
```

 

#### 2.利用FTP服务共享应答文件

```shell
[root@svr7 /]# cp  /root/ks.cfg   /var/ftp/

[root@svr7 /]# ls  /var/ftp/

centos  ks.cfg  pub

[root@svr7 /]#
```

#### 3.修改菜单文件，指定应答文件获取方式

```shell
[root@svr7 /]# vim  /var/lib/tftpboot/pxelinux.cfg/default

……..此处省略一万字

label linux

menu label ^Install CentOS 7

menu  default

kernel vmlinuz

append initrd=initrd.img  ks=ftp://192.168.4.7/ks.cfg

```

 

#### 总结思路：

1.dhcp服务---》IP地址、next-server、filename "pxelinux.0"

2.tftp服务---》 "pxelinux.0"

3.pxelinux.0---》读取菜单文件/var/lib/tftpboot/pxelinux.cfg/default

4.default---》vesamenu.c32、读秒时间、vmlinuz、initrd.img、ftp://192.168.4.7/ks.cfg

5.ks.cfg应答文件---》语言、键盘类型、分区、安装方式url --url="ftp://192.168.4.7/centos"

 

在虚拟机B构建网络装机时，关闭虚拟机A的DHCP服务，避免冲突

------

