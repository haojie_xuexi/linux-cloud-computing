# Operation

#  Nginx

### 1 .简述常见Web服务器软件有哪些

```
参考答案

Apache、 Nginx、 Lighttpd 、Tomcat、 IBM WebSphere、IIS。
```



### 2 .使用Nginx部署基于域名的虚拟主机如何修改配置文件.

参考答案

```nginx
[root@nginx ~]# vim /usr/local/nginx/nginx.conf
.. ..
server {        
	listen       80;                          //端口        
	server_name  web1.plj.com;               //域名        
	location / {             
        root   /web1;                        //网页根路径              
        index  index.html;                 //定义首页文档          
    }
}    
server {        
    listen       80;        
    server_name  web2.plj.com;        
    location / {              
        root   /web2;              
        index  index.html;           
    }
}
.. ..
```

### 3. 对网站进行SSL加密，如何生成私钥与证书

参考答案

```
[root@nginx ~]# openssl genrsa -out cert.key 2048                    //生成私钥
[root@nginx ~]# openssl req -new -x509 -key test.pem -out cert.pem    //生成证书
```



### 4. Nginx反向代理如何设置后端服务器组的状态

Nginx可以设置后台服务器组主机的状态，在括号内填写下列不同状态的作用

- down （ ）
- max_fails （ ）
- fail_timeout （ ）
- backup （ ）

```nginx
参考答案

down：表示当前server暂时不参与负载

max_fails：允许请求失败的次数（默认为1）

fail_timeout ：max_fails次失败后，暂停提供服务的时间

backup：备份服务器
```



### 5. Nginx实现TCP/UDP调度需要什么模块

```nginx
参考答案

需要ngx_stream_core_module模块，使用--with-stream可以开启该模块。
```



### 6.如何优化提升Nginx并发数量

参考答案

```nginx
[root@nginx ~]# vim /usr/local/nginx/nginx.conf
.. ..
events {
	worker_connections 65535;                //每个worker最大并发连接数
    use epoll;
}
[root@nginx ~]# vim /etc/security/limits.conf
.. ..
*             soft    nofile  100000
*             hard    nofile  100000
```

### 7.如何使用ab对Web服务器进行压力测试

要求：并发数为1024，总请求数为2048，测试页面为http://www.tarena.com/

参考答案

```
[root@localhost ~]# ab -c 2048 –n 1024 http://www.tarena.com/
```

### 8.使用Nginx如何自定义404错误页面

参考答案

```nginx
[root@nginx ~]# vim /usr/local/nginx/conf/nginx.conf
.. ..
http {
	fastcgi_intercept_errors on;         //错误页面重定向
	server {
		error_page   404  /40x.html;        //自定义404错误页面        
		location = /40x.html {            
			root   html;        
		}
		error_page   500 502 503 504  /50x.html;        
		location = /50x.html {            
			root   html;        
		}
    }
}
```

# Memcached

### 1. 简述什么是memcached

```
参考答案

memcached是高性能的分布式缓存服务器，是一个跨平台的、开源的实现分布式缓存服务的软件

用来集中缓存数据库查询结果，减少数据库访问次数，以提高动态Web应用的响应速度

memcached支持许多平台：Linux、FreeBSD、Solaris (memcached 1.2.5以上版本)、Mac OS X、Windows
```



### 2.PHP的Session会话共享

要求：PHP通过memcached实现Session会话共享，修改哪儿文件如何修改？

参考答案

```shell
vim  /etc/php-fpm.d/www.conf
修改前:
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session
修改后
php_value[session.save_handler] = memcache
php_value[session.save_path] = "tcp://192.168.2.5:11211" 
```

# LNMP

### 1 .简述什么是LNMP

参考答案

```
LNMP是网站运行平台。

L：操作系统（Linux）

N：网站服务软件（Nginx）

M：数据库服务软件（MariaDB）

P：网站开发语言（PHP、perl、python）
```



### 2. 部署LNMP时，如何修改Nginx配置文件

参考答案

```nginx
[root@nginx ~]# vim /usr/local/nginx/nginx.conf
.. ..
location / {            
    root   html;            
    index  index.php  index.html   index.htm;        
} 
location  ~  \.php$  {            
    root           html;            
    fastcgi_pass   127.0.0.1:9000;            
    fastcgi_index  index.php;           
    # fastcgi_param   SCRIPT_FILENAME  /scripts$fastcgi_script_name;            
    include        fastcgi_conf;        
}.. ..
```

### 3. 地址重写的好处

```
参考答案

缩短URL，隐藏实际路径提高安全性；

易于用户记忆和键入；

易于被搜索引擎收录。
```



### 4 .使用Nginx实现域名跳转

要求：访问www.tarena.com时自动跳转至bbs.tarena.com

参考答案

```nginx
[root@nginx ~]# cat /usr/local/nginx/conf/nginx.conf
.. ..
server {        
	listen       80;        
	server_name  www.tarena.com;
	location / {    
	root   html;index  index.html index.htm;
	rewrite ^/ http://bbs.tarena.com/;
	}
}
```



# Tomcat

### 1 .哪些参数影响了Tomcat部署网站时的路径

```
参考答案

appBase，docBase，path。
```



### 2. 使用keytool生成密钥文件的命令是什么？

参考答案

```
[root@localhost ~]keytool -genkeypair -alias tomcat -keyalg RSA \
> -keystore /usr/local/tomcat/keystore
```

### 3. 代理软件的功能

参考答案

```
Web访问加速；

IP伪装、“翻墙”。
```



### 4. Varnish有哪些优势

```
参考答案

Varnish是一款高性能且开源的反向代理服务器和http加速器。

Varnish具有性能更高、速度更快、管理更方便等诸多优点。
```

