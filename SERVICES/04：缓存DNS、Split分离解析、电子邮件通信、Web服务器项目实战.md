## 04：缓存DNS、Split分离解析、电子邮件通信、Web服务器项目实战

### 一、 环境的准备

 **关闭两台虚拟机的SELinux**

```shell
[root@svr7 ~]# setenforce  0    #修改当前运行模式

[root@svr7 ~]# getenforce     #查看当前运行模式

Permissive

[root@svr7 ~]# vim  /etc/selinux/config  #永久修改

SELINUX=permissive
```

 **设置两台虚拟机防火墙**

```shell
[root@svr7 ~]# firewall-cmd  --set-default-zone=trusted
```



### 二、 构建基本的DNS服务器

虚拟机A：负责解析lol.com

```shell
[root@svr7 ~]# yum -y  install  bind  bind-chroot

[root@svr7 ~]# cp  /etc/named.conf   /root

[root@svr7 ~]# vim  /etc/named.conf

options {

    directory    "/var/named";

};

zone  "lol.com"  IN  {   #声明负责的域名

     type  master;     #声明本机为权威主DNS服务器

     file  "lol.com.zone";   #指明地址库文件名称

};

]# cp  -p  named.localhost   lol.com.zone

]# ls  -l  lol.com.zone

]# vim  lol.com.zone

此处省略一万字………

lol.com.  NS   svr7

svr7     A    192.168.4.7

www     A    1.2.3.4

]# systemctl  restart  named
```

 

虚拟机B：测试

```shell
]# echo  nameserver  192.168.4.7  >  /etc/resolv.conf

]# cat  /etc/resolv.conf

]# nslookup  www.lol.com  
```

### 三、 DNS子域授权

虚拟机A：负责lol.com域名

虚拟机B：负责bj.lol.com域名

 

虚拟机B：构建DNS服务器

```shell
[root@pc207 ~]# yum -y install  bind  bind-chroot

[root@pc207 ~]# cp /etc/named.conf /root

[root@pc207 ~]# vim /etc/named.conf

options {

    directory    "/var/named";

};

zone "bj.lol.com" IN {

    type master;

    file "bj.lol.com.zone";

};

[root@pc207 ~]# cd /var/named/

[root@pc207 named]# cp -p named.localhost  bj.lol.com.zone

[root@pc207 named]# vim bj.lol.com.zone

此处省略一万字………

bj.lol.com.   NS    pc207

pc207  A    192.168.4.207

www   A    2.2.2.2

[root@pc207 named]# systemctl restart named
```

 

测试：

```shell
[root@pc207 /]# nslookup  www.bj.lol.com  192.168.4.207

Server:     192.168.4.207

Address:     192.168.4.207#53

Name:  www.bj.lol.com

Address: 2.2.2.2
```

虚拟机A：能够解析虚拟机B负责的域名（子域授权）

1. 修改地址库文件进行声明

此处省略一万字………

```shell
[root@svr7 /]# cd /var/named/

[root@svr7 named]# vim lol.com.zone

lol.com.   NS  svr7

bj.lol.com. NS  pc207

svr7      A   192.168.4.7

pc207     A   192.168.4.207

www     A   1.2.3.4

[root@svr7 /]# systemctl  restart  named

[root@pc207 /]# nslookup  www.bj.lol.com   192.168.4.7

Server:     192.168.4.7

Address:     192.168.4.7#53


Non-authoritative answer:   #非权威解答

Name:  www.bj.lol.com

Address: 2.2.2.2
```

 

#### 递归解析：客户端发送请求给首选DNS服务器，首选DNS服务器与其他DNS服务器交互，最终将解析结果带回来过程

 

#### 迭代解析：客户端发送请求给首选DNS服务器，首选DNS服务器告知下一个DNS服务器地址

 

### 四、 缓存DNS

作用：缓存解析结果，加快访问

![img](./04image/001.png) 

 

虚拟机A：为真正的DNS服务器

虚拟机B：缓存DNS服务器



虚拟机B：

```shell
[root@pc207 /]# yum -y  install  bind  bind-chroot

[root@pc207 /]# vim  /etc/named.conf

options  {

    directory    "/var/named";

    forwarders  {  192.168.4.7;  };\****   #转发给192.168.4.7

};

[root@pc207 /]# systemctl restart named

[root@pc207 /]# nslookup www.lol.com  192.168.4.207

Server:     192.168.4.207

Address:     192.168.4.207#53


Non-authoritative answer:

Name:  www.lol.com

Address: 1.2.3.4
```



### 五、 分离解析

 

#### • 当收到客户机的DNS查询请求的时候

– 能够区分客户机的来源地址

– 为不同类别的客户机提供不同的解析结果（IP地址）

– 为客户端提供最近的服务器

 

牛老师(首都机场)--->达外酒店--->地图软件--->机场大街88号

王老师(北京南站)--->达外酒店--->地图软件--->南站大街10号

客户端(源IP地址) --->域名--->DNS服务器--->解析结果

 

 

#### • 分离解析配置

– 客户端必须找到自己的类型

– 由上到下依次匹配，匹配及停止  

– 所有的zone都必须在view

```shell
view  "haha" {

  match-clients  {  192.168.4.207;  .. ..;   };

  zone  "12306.cn"  IN  {

   …… 地址库1;

  };   };                         

view  "xixi" { 

  match-clients  {   any;    };

  zone  "12306.cn"  IN {

    …… 地址库2;

  };   };
```

 

#### • 环境及需求

– 权威DNS：svr7.tedu.cn  192.168.4.7

– 负责区域：lol.com

– A记录分离解析 —— 以 www.lol.com为例

192.168.4.207或者192.168.7.0/24----->192.168.4.100

 any----->1.2.3.4

 

虚拟机A：

1.修改主配置文件

```shell
[root@svr7 /]# vim /etc/named.conf

options {

   directory    "/var/named";

};

view    "nsd"   {

 match-clients   {   192.168.4.207;   192.168.7.0/24;   };

 zone  "lol.com"   IN {

    type  master;

    file   "lol.com.zone";

 };

};

view    "other"   {

 match-clients    {   any;   };

 zone "lol.com"   IN {

   type   master;
   file   "lol.com.other";

 };

};
```

2．建立地址库文件

```shell
]# cd  /var/named/

]# vim   lol.com.zone

此处省略一万字……..

lol.com.   NS  svr7

svr7      A  192.168.4.7

www     A  192.168.4.100

]# cp  -p   lol.com.zone    lol.com.other

]# vim     lol.com.other

此处省略一万字……..

lol.com.   NS  svr7

svr7      A   192.168.4.7

www      A   1.2.3.4

[root@svr7 named]# systemctl  restart  named
```

 

#### • 多域名分离解析环境及需求

– 客户端必须找到自己的类型

– 由上到下依次匹配，匹配及停止  

– 所有的zone都必须在view

– 每一个view中，zone个数要一致

– 每一个view中，zone负责的域名要一致

– 

– 权威DNS：svr7.tedu.cn  192.168.4.7

– 负责区域：lol.com  qq.com

– A记录分离解析 —— 以 www.lol.com  www.qq.com为例

• 192.168.4.207 -- www.lol.com --->192.168.4.100

• any--- www.lol.com -->1.2.3.4

 

• 192.168.4.207--www.qq.com--->192.168.4.200

• any-- www.qq.com --->10.20.30.40

 

![img](./04image/002.png) 

 

虚拟机A：

```shell
[root@svr7 named]# vim /etc/named.conf

options {

   directory    "/var/named";

};

view "nsd" {

 match-clients  {  192.168.4.207;  192.168.7.0/24;  };

 zone "lol.com" IN {

    type master;

    file "lol.com.zone";

 };

 zone "qq.com" IN {

    type master;

    file "qq.com.zone";

 };

};


view "other" {

 match-clients {  any;  };

 zone "lol.com" IN {

    type master;

    file "lol.com.other";

 };

 zone "qq.com" IN {

    type master;

    file "qq.com.other";

 };

};
```

2.建立地址库文件

```shell
]# cd  /var/named/

]# cp -p  named.localhost   qq.com.zone

]# vim  qq.com.zone        

此处省略一万字……..

qq.com.  NS    svr7

svr7     A    192.168.4.7

www    A    192.168.4.200

]# cp -p qq.com.zone  qq.com.other

]# vim qq.com.other

此处省略一万字……..

qq.com. NS    svr7

svr7    A    192.168.4.7

www   A    10.20.30.40

[root@svr7 named]# systemctl  restart named
```

 

• 针对来源地址定义acl列表(了解内容)

– 若地址比较少，也可以不建立列表

```shell
options {

    directory    "/var/named";

};

acl "test"  {  192.168.4.207;   192.168.7.0/24;  };

view "nsd" {

 match-clients {  test;  };

 zone "lol.com" IN {

此处省略一万字……..
```



### 六、 NTP时间服务器

作用：提供标准时间

• Network Time Protocol（网络时间协议）

• 它用来同步网络中各个计算机的时间的协议

• 210.72.145.39 (国家授时中心服务器IP地址)

 

• Stratum（分层设计）

• Stratum层的总数限制在15以内（包括15i）

 

虚拟机A：时间服务器

1.安装软件包chrony

```
[root@svr7 /]# yum  -y  install  chrony
```

2.修改配置文件

```shell
[root@svr7 /]# vim  /etc/chrony.conf    

server  0.centos.pool.ntp.org  iburst   #与谁同步时间

server  1.centos.pool.ntp.org  iburst   #iburst表示快速同步

server  2.centos.pool.ntp.org  iburst

server  3.centos.pool.ntp.org  iburst


26行  allow  all      #开头的#去掉，修改为允许所有客户端
 

29行  local  stratum  10   #设置本机为第10层的时间服务器
```

3.重启时间服务

```
[root@svr7 /]# systemctl   restart   chronyd
```

虚拟机B：客户端

1.安装软件包chrony

```shell
[root@pc207 /]# yum  -y  install  chrony

[root@pc207 /]# rpm -q chrony

chrony-3.2-2.el7.x86_64
```

2.修改配置文件

```shell
[root@pc207 /]# vim  /etc/chrony.conf    

server  192.168.4.7  iburst    #与192.168.4.7同步时间

\#server  1.centos.pool.ntp.org  iburst   #iburst表示快速同步

\#server  2.centos.pool.ntp.org  iburst

\#server  3.centos.pool.ntp.org  iburst
```

3.重启时间服务

```
[root@pc207 /]# systemctl  restart  chronyd
```

4.测试： 

```shell
[root@pc207 /]# date  -s  "2008-10-1"

[root@pc207 /]# date

[root@pc207 /]# systemctl  restart  chronyd 

[root@pc207 /]# date

[root@pc207 /]# date

2020年 09月 18日 星期五 16:16:22 CST

[root@pc207 /]#

[root@pc207 /]#  chronyc  sources  -v   #列出时间服务器信息
```



 

### 七、 邮件服务器

 

#### • 电子邮件服务器的基本功能

– 为用户提供电子邮箱存储空间（用户名@邮件域名）

– 处理用户发出的邮件 —— 传递给收件服务器

– 处理用户收到的邮件 —— 投递到邮箱

 

![img](./04image/003.png) 

虚拟机A：DNS服务器，添加邮件解析功能

1.修改主配置文件

```shell
[root@svr7 /]# vim /etc/named.conf

options {

    directory    "/var/named";

};

Zone  "qq.com"  IN  {

     type  master;

     file  "qq.com.zone";

};
```

2.建立地址库文件

```shell
[root@svr7 /]# cd /var/named/

[root@svr7 named]# vim qq.com.zone

此处省略一万字……..

qq.com.  NS      svr7

qq.com.  MX  10  mail      #数字10为优先级，越小越优先

svr7     A    192.168.4.7

mail     A    192.168.4.7

www    A    192.168.4.200 

]# systemctl  restart  named

]# echo nameserver 192.168.4.7 > /etc/resolv.conf

]# host  -t  MX  qq.com     #测试qq.com区域邮件交换记录

qq.com mail is handled by 10 mail.qq.com.

]# host mail.qq.com        #测试域名完整解析

mail.qq.com has address 192.168.4.7
```

 

虚拟机A：邮件服务器

1.安装软件包

```shell
[root@svr7 /]# rpm -q postfix

postfix-2.10.1-6.el7.x86_64
```

2.修改配置文件

```shell
[root@svr7 /]# vim  /etc/postfix/main.cf

99 myorigin = qq.com  #默认补全，域名后缀

116 inet_interfaces =  all #本机所有IP地址均提供邮件收发功能

164 mydestination = qq.com   #判断本域邮件的依据
```

3.重启邮件服务

```shell
[root@svr7 /]# systemctl restart postfix
```

 

4.测试

• mail 发信操作:mail  -s  '邮件标题'   -r   发件人  收件人 

```shell
[root@svr7 /]# useradd yg

[root@svr7 /]# useradd xln

[root@svr7 /]# mail -s 'test01'  -r yg  xln

ahahaxixiehehelele

.       #一行只有一个点表示提交

EOT

[root@svr7 /]#
```

 

• mail 收信操作:mail  [-u  用户名]

```shell
[root@svr7 /]# mail -u xln

\>N  1 yg@qq.com       Fri Sep 18 17:24  18/510

&  1      #输入邮件编号


&  quit    #退出


[root@svr7 /]# echo 123456  |  mail  -s  'test02'  -r yg  xln

[root@svr7 /]# mail -u xln  
```



### 八、 综合实验

![img](./04image/004.png) 

要求：

1.在Web1机器上构建Web服务，实现基于域名的虚拟Web主机，提供www.163.com与www.qq.com两个网站

2.在Web2机器上构建Web服务，实现基于域名的虚拟Web主机，提供www.163.com与www.qq.com两个网站

3.客户端192.168.4.207访问www.163.com与www.qq.com两个网站，由Web1服务器提供

4.客户端192.168.4.208访问www.163.com与www.qq.com两个网站，由Web2服务器提供 

5.在192.168.4.7上实现DNS服务器分离解析，5台机器DNS服务器指向192.168.4.7

构建Web服务器
虚拟机Web1：

```shell
[root@Web1 ~]# yum -y install httpd
[root@Web1 ~]# vim /etc/httpd/conf.d/nsd.conf
<VirtualHost  *:80>
  ServerName www.qq.com
  DocumentRoot   /var/www/qq
</VirtUalHost>
<VirtualHost  *:80>
  ServerName www.163.com
  DocumentRoot   /var/www/163
</VirtUalHost>
[root@Web1 ~]# mkdir /var/www/qq  /var/www/163
[root@Web1 ~]# echo Web1 QQ > /var/www/qq/index.html
[root@Web1 ~]# echo Web1 163 > /var/www/163/index.html
[root@Web1 ~]# systemctl  restart  httpd
```

构建Web服务器
虚拟机Web2：

```shell
[root@Web2 ~]# yum -y install httpd
[root@Web2 ~]# scp root@192.168.4.10:/etc/httpd/conf.d/nsd.conf   /etc/httpd/conf.d/
[root@Web2 ~]# cat  /etc/httpd/conf.d/nsd.conf
[root@Web2 ~]# mkdir /var/www/qq  /var/www/163
[root@Web2 ~]# echo Web2 QQ > /var/www/qq/index.html
[root@Web2 ~]# echo Web2 163 > /var/www/163/index.html
[root@Web2 ~]# systemctl restart httpd
```

构建DNS服务器
虚拟机svr7：

```shell
[root@svr7 ~]# yum -y install bind  bind-chroot
[root@svr7 ~]# cp   /etc/named.conf   /root/
[root@svr7 ~]# vim   /etc/named.conf
options {
        directory       "/var/named";
};
view "haha" {
 match-clients {  192.168.4.207;   };
 zone "qq.com" IN {
        type master;
        file "qq.com.zone";
 };
 zone "163.com" IN {
        type master;
        file "163.com.zone";
 };
};
view "xixi" {
 match-clients {   any;   };
 zone "qq.com" IN {
        type master;
        file "qq.com.other";
 };
 zone "163.com" IN {
        type master;
        file "163.com.other";
 };
};
[root@svr7 ~]# cd  /var/named/
[root@svr7 named]# cp -p named.localhost    qq.com.zone
[root@svr7 named]# vim qq.com.zone
此处省略一万字……..
qq.com.  NS      svr7
svr7        A       192.168.4.7
www       A       192.168.4.10
[root@svr7 named]# cp -p qq.com.zone 163.com.zone
[root@svr7 named]# vim 163.com.zone
此处省略一万字……..
163.com.        NS      svr7
svr7      A       192.168.4.7
www     A       192.168.4.10
[root@svr7 named]# cp -p qq.com.zone qq.com.other
[root@svr7 named]# vim qq.com.other
此处省略一万字……..
qq.com. NS      svr7
svr7    A       192.168.4.7
www     A       192.168.4.20
[root@svr7 named]# cp -p 163.com.zone 163.com.other
[root@svr7 named]# vim 163.com.other
此处省略一万字……..
163.com.        NS      svr7
svr7    A       192.168.4.7
www     A       192.168.4.20
[root@svr7 named]# systemctl restart named
```

指定DNS服务器地址  

```shell
[root@pc207 ~]# echo nameserver 192.168.4.7 > /etc/resolv.conf
[root@pc208 ~]# echo nameserver 192.168.4.7 > /etc/resolv.conf
[root@svr7 named]# echo nameserver 192.168.4.7 > /etc/resolv.conf
[root@Web1 ~]# echo nameserver 192.168.4.7 > /etc/resolv.conf
[root@Web2 ~]# echo nameserver 192.168.4.7 > /etc/resolv.conf
```

