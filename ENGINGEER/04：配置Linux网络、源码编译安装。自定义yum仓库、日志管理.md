## 04：配置Linux网络、源码编译安装。自定义yum仓库、日志管理

下载今日所需软件：MobaXterm_Professinal_20.3_Preview4.zip

 

\####################################################

### 一、 构建Yum仓库，环境准备

#### 1.光盘文件放入光驱设备

![image-20200912174828852](./04image/001.png)

![image-20200912174833712](./04image/002.png)



#### 2.挂载光驱设备

```
[root@localhost ~]# mkdir  /mydvd

[root@localhost ~]# mount  /dev/cdrom  /mydvd 
[root@localhost ~]# ls  /mydvd
```

#### 3.书写客户端配置文件

```shell
[root@localhost ~]# rm  -rf  /etc/yum.repos.d/*

[root@localhost ~]# vim  /etc/yum.repos.d/mydvd.repo

[centos]

name=2008

baseurl=file:///mydvd

enabled=1

gpgcheck=0

[root@localhost ~]# yum  -y  install  xorg-x11-apps

[root@localhost ~]# rpm -q xorg-x11-apps
```

 

#### 4．完成开机自动挂载

```shell
]# blkid  /dev/cdrom  #查看 光驱设备文件系统类型

]# vim  /etc/fstab

/dev/cdrom  /mydvd   iso9660   defaults  0  0

]# umount  /mydvd

]# mount  -a

]# ls  /mydvd


]# reboot     #重启测试

]# yum -y install httpd  #测试安装软件包

```



### 二、 配置网络参数之主机名  

####  **配置永久的主机名**

```shell
[root@localhost ~]# echo svr7.tedu.cn  >  /etc/hostname

[root@localhost ~]# cat  /etc/hostname

svr7.tedu.cn

[root@localhost ~]# hostname svr7.tedu.cn  #修改当前

[root@localhost ~]# hostname

svr7.tedu.cn
```

开启一个新的终端查看提示符的变化

### 三、 配置网络参数之IP地址与子网掩码、网关地址

#### **修改网卡命令规则(eth0、eth1、eth2……)**

```shell
]# ifconfig  |   head   -2

ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500

    ether 00:0c:29:8a:72:4f  txqueuelen 1000  (Ethernet)


]# vim  /etc/default/grub  #grub内核引导程序

……..

GRUB_CMDLINE_LINUX="…….. quiet  net.ifnames=0  biosdevname=0"

……..

 

]# grub2-mkconfig  -o  /boot/grub2/grub.cfg  #让网卡命名规则生效

 

]# reboot

]# ifconfig  |  head  -2

eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500

   inet 192.168.81.132  netmask 255.255.255.0  broadcast 192.168.81.255
```

 

####  **nmcli命令的网卡命名，删除错误网卡命名**

```shell
[root@svr7 ~]# nmcli  connection  show       #查看

[root@svr7 ~]# nmcli  connection  delete  ens33

[root@svr7 ~]# nmcli  connection  show


[root@svr7 ~]# nmcli  connection  show

[root@svr7 ~]# nmcli  connection  delete  有线连接\ 1 

[root@svr7 ~]# nmcli  connection  show
```

 

#### **nmcli命令的网卡命名，添加网卡命名**   

```shell
[root@svr7 ~]# nmcli connection add type ethernet    ifname eth0  con-name eth0


[root@svr7 ~]# nmcli connection 添加  类型  以太网设备

网卡设备名为eth0   nmcli命令的命名为eth0

[root@svr7 ~]# nmcli  connection show
```



#### **修改IP地址、子网掩码、网关地址**

```shell
[root@svr7 ~]# nmcli connection modify  eth0   

ipv4.method   manual              

ipv4.addresses  192.168.4.7/24         

ipv4.gateway  192.168.4.254         

connection.autoconnect   yes

[root@svr7 ~]# nmcli connection  修改 网卡名  

ipv4.方法  手工配置             

ipv4.地址  192.168.4.7/24         

ipv4.网关  192.168.4.254         

每次开机自动启用


[root@svr7 ~]# nmcli connection up eth0   #激活

[root@svr7 ~]# ifconfig  |  head  -2
```



网卡配置文件：/etc/sysconfig/network-scripts/ifcfg-eth0

```shell
[root@svr7 ~]# route  -n     #查看网关地址信息

Kernel IP routing table

Destination   Gateway     Genmask     Flags Metric Ref   Use Iface

0.0.0.0     ***\*192.168.4.254\****  0.0.0.0     UG   100   0     0 eth0

192.168.4.0   0.0.0.0     255.255.255.0  U   100   0     0 eth0

192.168.122.0  0.0.0.0     255.255.255.0  U   0    0     0 virbr0
```



### 四、 配置网络参数之DNS服务器地址

DNS服务器：负责域名解析的机器，将域名解析为IP地址

```
[root@svr7 ~]# echo nameserver  8.8.8.8  > /etc/resolv.conf

[root@svr7 ~]# cat  /etc/resolv.conf

nameserver  8.8.8.8
```



### 五、 模板机器的修改

将UUID进行修改，修改为/dev/sda1

```
[root@svr7 ~]# vim  /etc/fstab

……. 

/dev/sda1  /boot          xfs   defaults     0 0

…….
```



### 六、 真机与虚拟机的通信

 

![img](./04image/003.png) 



#### 1.查看真机虚拟网卡

![img](./04image/004.png) 

![img](./04image/005.png) 

![img](./04image/006.png) 

#### 2.真机配置VMnet1的网卡IP地址为192.168.4.1

**双击**VMnet1网络适配器

![img](./04image/007.png) 

 ![image-20200912175756716](./04image/008.png)

![img](./04image/009.png) 

![image-20200912175819697](./04image/010.png)

#### 3.配置虚拟机网络类型

右击----》选择设置

![img](./04image/011.png) 

![image-20200912175853735](./04image/012.png)

#### 4.测试通信                  

Windows键+r快捷键

![img](./04image/013.png) 

![image-20200912175912628](./04image/014.png)

常见问题：

1.虚拟网卡vmnet1没有

![img](./04image/015.png) 

 

![img](./04image/016.png) 

![img](./04image/017.png) 

 

如果还是无法出现vmnet1，参考下列网站方法

https://jingyan.baidu.com/article/066074d6f19bd0c3c31cb048.html



 

### 七、 虚拟机拍摄快照



|      |                           |
| ---- | ------------------------- |
|      | ![img](./04image/018.png) |

[root@svr7 ~]# poweroff

|      |                           |
| ---- | ------------------------- |
|      | ![img](./04image/019.png) |

 

### 八、 克隆虚拟机A（克隆必须关闭模板机器）

 ![image-20200912183432853](./04image/020.png)



![image-20200912183437438](./04image/021.png)

![image-20200912183441382](./04image/022.png)

![image-20200912183507250](./04image/023.png)

![image-20200912183517834](./04image/024.png)

![image-20200912183607558](./04image/025.png)

 

### 九、 虚拟机B配置

虚拟机B：

```shell
[root@svr7 ~]# hostname   pc207.tedu.cn

[root@svr7 ~]# echo  pc207.tedu.cn  >  /etc/hostname

[root@svr7 ~]# hostname

pc207.tedu.cn
```

 

新开一个终端进行测试

```shell
[root@pc207 ~]# nmcli  connection  modify  eth0 ipv4.method  manual  ipv4.addresses  192.168.4.207/24 connection.autoconnect  yes

[root@pc207 ~]# nmcli connection up eth0 

[root@pc207 ~]# ifconfig  |  head  -2

[root@pc207 ~]# ping 192.168.4.7

```

### 十、 虚拟机B拍摄快照

```
[root@pc207 ~]# poweroff
```

![img](./04image/026.png) 

![img](./04image/027.png) 

### 十一、 利用真机windows进行远程管理

![image-20200912183757606](./04image/028.png)

![image-20200912183802973](./04image/029.png)

![image-20200912183811084](./04image/030.png)

Ctrl+滚轮=可以放大或变小字体

### 十二、 源码编译安装

RPM包：rpm  -ivh    yum  install

源码包---开发工具--->可以执行的程序------>运行安装

• 主要优点

– 获得软件的最新版，及时修复bug

– 软件功能可按需选择/定制，有更多软件可供选择

– 源码包适用各种平台

– ……

#### **将真机的tools.tar.gz传递数据到虚拟机A**

![image-20200912183935392](./04image/031.png)



```shell
[root@svr7 ~]# ls    /root

tools.tar.gz   下载  公共   音乐
[root@svr7 ~]#
```

 

#### 源码编译安装步骤：

##### 步骤一:安装开发工具

```shell
]# yum  -y  install  make

]# yum  -y  install  gcc

]# rpm  -q  gcc

]# rpm  -q  make
```

##### 步骤二:进行tar解包               

```shell
]# tar  -xf  /root/tools.tar.gz  -C  /usr/local/

]# ls  /usr/local/

]# ls  /usr/local/tools/

]# tar -xf  /usr/local/tools/inotify-tools-3.13.tar.gz  -C /usr/local/

]# ls  /usr/local/
```

 

##### 步骤三：运行configure脚本进行配置

作用1：检测系统是否安装gcc 

作用2：可以指定安装位置及功能

```
]# cd  /usr/local/inotify-tools-3.13/

]# ./configure  --prefix=/opt/myrpm   #指定安装位置
```

 

常见错误：没有安装gcc

```shell
checking for gcc... no

checking for cc... no

checking for cl.exe... no

configure: error: no acceptable C compiler found in $PATH

See `config.log' for more details.
```

 

##### 步骤四：make进行编译，产生可以执行的程序

```
]# cd   /usr/local/inotify-tools-3.13/

]# make
```



##### 步骤五：make  install进行安装

```shell
]# cd   /usr/local/inotify-tools-3.13/

]# make   install

]# ls /opt/

]# ls /opt/myrpm/

]# ls /opt/myrpm/bin/
```



 

### 十三、 远程管理(Linux与Linux)

####  **软件包的安装**

```shell
[root@svr7 /]# rpm  -qa   |  grep  openssh

openssh-7.4p1-16.el7.x86_64

openssh-server-7.4p1-16.el7.x86_64

openssh-clients-7.4p1-16.el7.x86_64
```

 

#### **远程登录工具 ssh**

虚拟机A：

```shell
[root@svr7 /]#  >  /etc/resolv.conf

[root@svr7 /]#  ssh   root@192.168.4.207

………necting (yes/no)? yes

root@192.168.4.207's password:    #输入密码

[root@pc207 ~]# touch  /root/hahaxixi.txt

[root@pc207 ~]# exit

登出

Connection to 192.168.4.207 closed.

[root@svr7 /]# cat /root/.ssh/known_hosts  #记录曾经远程管理的机器
```

 

####  **实现ssh远程管理无密码验证**

虚拟机A：

1.生成公钥(锁)与私钥(钥匙)进行验证

```shell
[root@svr7 ~]# ssh-keygen    #一路回车

…….save the key (/root/.ssh/id_rsa):   #保存位置

……..assphrase):   #设置密码为空

…….. again:   #设置密码为空

[root@svr7 ~]# ls /root/.ssh/

id_rsa(私钥)   id_rsa.pub(公钥)   known_hosts
```

2.将公钥(锁)传递给虚拟机B 

```shell
[root@svr7 ~]# ssh-copy-id  root@192.168.4.207  

[root@svr7 ~]# ssh  root@192.168.4.207  #测试无密码

[root@pc207 ~]# exit

登出

Connection to 192.168.4.207 closed.

[root@svr7 ~]#

虚拟机B     

[root@pc207 ~]# ls   /root/.ssh/

authorized_keys(别的机器传递过来的公钥)   known_hosts

[root@pc207 ~]#
```

 

#### **安全复制工具 scp=ssh+cp**

– scp  [-r]  用户名@服务器:路径    本地路径

– scp  [-r]  本地路径   用户名@服务器:路径

虚拟机A：

```shell
]# scp  /etc/passwd    root@192.168.4.207:/root

]# scp  -r  /home    root@192.168.4.207:/root/

]# scp  root@192.168.4.207:/etc/passwd    /mnt/
```

虚拟机B：

```
]# ls  /root
```



### 课后习题：

#### 案例1:虚拟机B上操作：实现静态网络参数配置

– 主机名:test.example.com

– IP地址:172.25.0.11

– 子网掩码:255.255.0.0

– 默认网关:172.25.0.254

– DNS服务器:172.25.254.254

```shell
[root@B ~]# hostname    test.example.com
[root@B ~]# echo    test.example.com   >   /etc/hostname 
[root@B ~]# cat   /etc/hostname

[root@B ~]# nmcli connection show 
[root@B ~]# nmcli   connection   modify    'ens33'    ipv4.method    manual     ipv4.addresses 172.25.0.11/18 ipv4.gateway   172.25.0.254    connection.autoconnect    yes

[root@B ~]# nmcli   connection   up   'ens33'    

[root@B ~]# route   -n        #查看网关地址
[root@B ~]# ifconfig   |   head -2     #查看网卡地址

[root@B ~]# echo  nameserver   172.25.254.254    >   /etc/resolv.conf     
[root@B ~]# cat   /etc/resolv.conf
```



#### 案例2:虚拟机B上操作：实现静态网络参数配置

– 主机名:B.tedu.cn

– IP地址:192.168.1.1

– 子网掩码:255.255.255.0

– 默认网关:192.168.1.254

– DNS服务器:8.8.8.8

```shell
[root@B ~]# echo B.tedu.cn > /etc/hostname
[root@B ~]# hostname B.tedu.u
[root@B ~]# cat /etc/hostname
[root@B ~]# nmcli connection show
[root@B ~]# nmcli connection modify 'ens33' ipv4.method manual
ipv4.address 192.168.1.1/24 ipv4.gateway 192.168.1.254 connection.autoconnect yes
[root@B ~]# nmcli connection up 'ens33'
[root@B ~]# route -n  #查看网关地址
[root@B ~]# ifcongig | head -2   #查看网卡地址
[root@B ~]# echo nameserver 8.8.8.8 > /etc/resolv.conf
[root@B ~]# cat /etc/resolv.conf
```



#### 案例3:虚拟机B上操作：实现静态网络参数配置

– 主机名:pc207.tedu.cn

– IP地址:192.168.4.207

– 子网掩码:255.255.255.0

– 默认网关:192.168.4.254

– DNS服务器:1.1.1.1

```shell
[root@B ~]# hostname pc207.tedu.cn
[root@B ~]# echo pc207.tedu.cn > /etc/hostname
[root@B ~]# cat /etc/hostname
[root@B ~]# nmcli connection modify 'ens33' ipv4.method manual ipv4.address 192.168.4.207/24 ipv4.gateway 192.168.4.254 connection.autoconnect yes

[root@B ~]# nmcli connection up 'ens33'

[root@B ~]# route -n  #查看网关地址
[root@B ~]# echo nameserver 1.1.1.1 > /etc/resolv.conf
[root@B ~]# cat /etc/resolv.conf
```



#### 案例4:虚拟机A上操作：实现静态网络参数配置

– 主机名:svr7.tedu.cn

– IP地址:192.168.4.7

– 子网掩码:255.255.255.0

– 默认网关:192.168.4.254

– DNS服务器:1.1.1.1

```shell
[root@A ~]# hostname svr7.tedu.cn
[root@B ~]# echo svr7.tedu.cn > /etc/hostname
[root@B ~]# cat /etc/hostname
[root@B ~]# nmcli connection modfiy 'ens33' ipv4.method manual ipv4.address 192.168.4.7/24 ipv4.gateway 192.168.4.254 connection.autoconnect yes
[root@B ~]# rount -n  #查看网关地址
[root@B ~]# ifconfig | head-2 #查看网卡地址
[root@B ~]# echo nameserver 1.1.1.1 > /etc/resolv.conf
[root@B ~]# cat /etc/resolv.conf
```

 

#### 案例5：虚拟机B

 1.源码编译安装 inotify-tools 软件工具

 2.安装位置为/usr/local/tools

```shell
[root@B ~]# yum -y install gcc make
[root@B ~]# tar -xf /root/tools.tar.gz -C /
[root@B ~]# tar -xf /tools/inotify-tools-3.13.tar.gz -C /
[root@B ~]# ls /
[root@B ~]# ls /inotify-tools-3.13/
[root@B ~]# cd /inotify-tools-3.13/
[root@B inotify-tools-3.13]# ./configure --prefix=/usr/local/tools #指定安装位置
[root@B inotify-tools-3.13]# make 
[root@B inotify-tools-3.13]# make install 
[root@B inotify-tools-3.13]# ls /usr/local/tools
```

 

#### 案例6：虚拟机B：传递数据

 1.将本机/usr/local/tools/other目录传递到虚拟机A，放在虚拟机A的/usr/目录下

```
[root@B ~]# scp -r /usr/local/tools/other root@192.168.4.7:/usr/
```

 2.将本机/etc/gshadow文件传递到虚拟机A，放在虚拟机A的/root目录下

```
[root@B ~]# scp /etc/gshadow root@192.168.4.7:/root/
```

 3.将本机/etc/skel目录传递到虚拟机A，放在虚拟机A的/tmp目录下

```shell
[root@B ~]# scp -r /etc/skel root@192.168.4.7:/tmp

[root@A ~]# ls /usr/
[root@A ~]# ls /root
[root@A ~]# ls /tmp
```



#### 案例7：虚拟机B：远程无密码验证

 1.实现虚拟机B远程管理虚拟机A，无需密码验证

```shell
1.生成公钥  私钥
[root@pc207 ~]# ssh-keygen   #一路回车
[root@pc207 ~]# ls /root/.ssh/

2.传递公钥 到虚拟机A
[root@pc207 ~]# ssh-copy-id root@192.168.4.7

3.虚拟机A：查看
[root@svr7 ~]# ls /root/.ssh/

4.虚拟机B：测试无密码
[root@pc207 ~]# ssh root@192.168.4.7
```





