## 01:Shell 概述、编写及执行脚本、Shell变量、数值运算



### **什么是shell**

在Linux内核与用户之间的解释器程序

通常指 /bin/bash

负责向内核翻译及传达用户/程序指令

相当于操作系统的“外壳”

 

### **Shell的使用方式**

交互式  —— 命令行

人工干预、智能化程度高

逐条解释执行、效率低

非交互式	—— 脚本

需要提前设计、智能化难度大

批量执行、效率高

方便在后台静悄悄地运行

 

### **什么是Shell脚本**

提前写好可执行语句，能够完成特定任务的文件

顺序、批量化处理

 

```shell
[root@svr7 ~]# cat /etc/shells  //查看所有解释器

[root@svr7 ~]# sh  //切换成sh解释器

sh-4.2# ls   //利用sh解释器输入命令

sh-4.2# exit  //退出sh解释器

[root@svr7 ~]#yum -y install ksh  //安装新解释器

[root@svr7 ~]#ksh  //进入新解释器
```

 

Bash的优点 : tab键、历史命令、快捷键、支

持别名、管道、重定向

 

### **脚本的编写规范**

1， 声明解释器（使用哪种解释器）

```shell脚本
#!/bin/bash
```

2， 注释，内容可以是脚本的功能、作用等介绍（或者是步骤、思路、用途、变量含义等）

\#这是一个测试脚本

3， 执行指令 

```shell
echo nb
```

  所有需要脚本执行的任务都可以逐行写在这里

 

### **脚本的执行方式**

1， 使用路径指向一个具有x权限的文件

2， 使用解释器，无需x权限，新开启解释器子进程

```shell
[root@svr7 opt]# bash test01.sh

nb
```

3， 使用source命令，无需x权限，不会开启解释器

子进程，而使用默认解释器进程

```shell
[root@svr7 opt]# source test01.sh

nb
```

 

[root@svr7 opt]# cat test01.sh   //可以利用该脚本测试，bash执行时看不到进入abc的效果，因为bash开启了子进程，执行完任务就退出了，父进程并没有进入abc目录，如果使用source执行该脚本就可以看到进入abc目录的效果，因为是父进程亲自进入没有开启子进程。  另外开一个命令行窗口使用pstree命令可以看到具体过程

```shell
\#!/bin/bash
```

\#这是一个测试脚本

```shell
echo nb

mkdir abc

cd abc

sleep 1000
```



### **编写部署网站的脚本，测试时需要临时关闭防火墙**

\#!/bin/bash

\#部署网站服务

```shell
yum -y install httpd

echo "web~test~~~~~!!" > /var/www/html/index.html

systemctl restart httpd
```



### **常量  固定不变的内容**

**变量  以固定名称存放，可能会变化的值**

​	 **提高脚本对任务需求、运行环境变化的适应能力**

​	 **方便在脚本中重复使用**

 

### **变量的种类**

1， 自定义变量，定义名称时可以用数字、大小写字母

下划线，不允许使用数字开头，不允许使用特殊字符

变量名称=变量的值

```shell
[root@localhost opt]# a=10  定义变量(赋值)

[root@localhost opt]# echo $a  调用变量

[root@localhost opt]# unset a 取消变量的定义

echo ${a}RMB  容易发生混淆的情况使用大括号
```



**2，环境变量，由系统提前定义好，使用时直接调用，无需用户定义**

USER当前用户名  HOME当前用户的家目录  

UID当前用户的id号  SHELL当前用户的解释器  

PWD当前位置  PATH 存储了执行指令的路径

PS1  一级提示符  PS2  二级提示符

 

3 预定义变量与位置变量

```shell
$*  $#  $$  $?  $1  $2  $3  …..
```

 

\#!/bin/bash

echo $1  执行脚本名称后面的第1个位置参数

echo $2  执行脚本名称后面的第2个位置参数

echo $3  执行脚本名称后面的第3个位置参数

echo $*  所有的位置参数

echo $#  所有的位置参数的个数

echo $$  随机的进程号

echo $?  判断上一条指令是否执行成功，0是成功

​		 非0是失败



**编写脚本，可以创建用户abcd，并且配置密码123456**

```shell
#!/bin/bash

useradd abcd  

echo 123456 | passwd --stdin abcd  
```

 

**改良版本，使用变量**

```shell
#!/bin/bash

useradd $1  //创建用户时调用第1个位置变量

echo "$2" | passwd --stdin $1  //配置密码时调用第2个位置变量
```

### **变量的扩展知识**

1.单引号  界定范围  可以屏蔽特殊符号

```shell
Touch ‘a b’  可以创建a空格b的文件

a=10

echo ‘$a’  无法调用变量，$是调用变量的特殊符号

其效果被屏蔽
```

双引号  界定范围

```shell
Touch “a b”  也可以创建a空格b的文件

echo “$a” 可以调用变量

` `反撇号  获取指令的执行结果 ，或者使用$()

a=date   定义变量时如果使用命令则不会直接识别

a=`date`  使用反撇号可以将命令的执行结果赋值给变量

a=$(date)  效果同上
```

2，使用read指令，将脚本变成交互式，使脚本可以从用户那里得到信息

用法：read -p “提示的信息”  变量名

```shell
#!/bin/bash

read -p "请输入用户名："  u

useradd $u

read -p "请输入密码："  p

echo $p | passwd --stdin $u
```

 

**stty  -echo  屏蔽回显**

**stty  echo  恢复回显**

**再次改良后的脚本，输入密码时屏蔽回显**

```shell
#!/bin/bash

read -p "请输入用户名："  u

useradd $u

stty -echo

read -p "请输入密码："  p

stty echo

echo "$p" | passwd --stdin $u
```

**1，export  定义全局变量，可以让子进程使用父进程定义的变量**

```shell
export b=20  创建变量b的同时发布为全局效果

export  a  将已有的变量发布为全局效果

export  -n  a  取消变量的全局效果

 
env 查看所有环境变量

set 查看所有变量
```

 

```shell
shell中的运算

1， 使用expr工具

expr  可以计算并输出，运算符号两边要有空格

expr 3 – 1  减法

expr 2  + 2   加法

expr 2  '*'  2   乘法

expr 2  \*  2   使用乘法时，这里使用\代表

转义符号，可以屏蔽之后1个字符的特殊效果

expr 4  / 2  除法

expr 10  % 3   取余数
```

 

```shell
2，方法二用$[ ]结构，配合echo输出，同样可以实

现加 减 乘 除 取余

echo $[1+1]

echo $[2-1]

echo $[2*2]

echo $[4/2]

echo $[5%3]
```

 

3 , 使用bc 计算器，可以进行小数运算

```shell
echo "1.1+1" | bc   //非交互的方式使用bc工具

echo "10/3" | bc

echo "scale=3;10/3" | bc  //scale可以定义小数点

后面的长度
```

### **练习题**：

1，如何执行Shell脚本？

```shell
方法一：添加x权限，然后使用路径运行
方法二：bash 脚本文件路径
方法三：source 脚本文件路径
```

2 自定义Shell变量时，有哪些注意事项？

```
可以包括数字、字母、下划线，不能以数字开头
赋值时等号两边不要有空格
尽量不要使用关键字和特殊字符
给同一个变量多次赋值时，最后一次的赋值生效
```

3 编写一个Shell脚本程序，用来报告当前用户的环境信息。

```shell
新建脚本文件report.sh，执行后能够输出当前的主机名、登录用户名、所在的文件夹路径。

#!/bin/bash
echo "当前的主机名是：$HOSTNAME"
echo "登录用户是：$USER"
echo "当前位于 $PWD 文件夹下"
```

4 简述预定义变量$$、$?、$#、$*的作用。

```shell
$$ 保存当前运行进程号
$? 保存命令执行结果的（返回状态）0是成功，非0是失败
$# 保存位置变量的（个数）
$* 保存所有位置变量的（值）
```

5 编写一个Shell脚本程序，能够部署ftp服务。

```shell
#!/bin/bash
yum -y install vsftpd &> /dev/null
systemctl restart vsftpd
systemctl enable vsftpd
chmod 777 /var/ftp/pub
```

6 简述单引号、双引号、反撇号在变量赋值操作中的特点。

```shell
双引号 " "：可以界定范围
单引号 ' '：可以界定范围，还可以屏蔽特殊符号，即便 $ 也视为普通字符
反撇号 ` `：将命令的执行输出作为变量值
```

