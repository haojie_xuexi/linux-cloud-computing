# Ansible

### 1.Ansible是基于什么语言开发的软件？

```
Python
```

### 2. Ansible主配置文件名称是什么？

```
ansible.cfg
```

### 3.Ansible读取配置文件查找顺序？

```
首先检测ANSIBLE_CONFIG变量定义的配置文件
其次检查当前目录下的./ansible.cfg文件
再次检查当前用户家目录下~/ansible.cfg文件
最后检查/etc/ansible/ansible.cfg文件
```



###  4.通过sudo给普通用户授权时使用什么关键词可以免密码执行sudo？

```
NOPASSWD
```



### 5.在YAML文件中使用什么符号支持跨行文本

```
> 或者 |
```



### 6.简单描述ansible_facts的作用？

```
ansible_facts用于采集被管理设备的系统信息
所有收集的信息都被保存在变量中
每次执行playbook默认第一个任务就是Gathering Facts
使用setup模块可以查看收集到的facts信息
```

### 7. 批量分发ssh秘钥

```
ansible all -m  authorized_key  -a "user=root key='{{ lookup('file', '/root/.ssh/id_rsa.pub') }}'"
```

