## 01：KVM构建及管理、virsh控制工具、镜像管理、虚拟机快建技术

### 一、服务管理

#### •上帝进程：systemd

#### •Linux系统和服务管理器

–是内核引导之后加载的第一个初始化进程（PID=1）

–负责掌控整个Linux的运行/服务资源组合

 

#### •一个更高效的系统&服务管理器

–开机服务并行启动，各系统服务间的精确依赖

–配置目录：/etc/systemd/system/

–服务目录：/lib/systemd/system/

–主要管理工具：systemctl

```shell
[root@svr7 ~]# systemctl  -t  service  --all  #列出所有的服务
```

 

#### •对于服务的管理

> systemctl  restart  服务名 	 #重起服务
>
> systemctl  start  服务名 		#开启服务 
>
> systemctl  stop  服务名 		#停止服务
>
> systemctl  status  服务名		 #查看服务当前的状态
>
>
> systemctl  enable  服务名 		#设置服务开机自启动
>
> systemctl  disable  服务名 		#设置服务禁止开机自启动
>
> systemctl  is-enabled  服务名	 #查看服务是否开机自启

```shell
[root@svr7 ~]# killall httpd       #杀死手动启动的httpd

[root@svr7 ~]# yum  -y  install  httpd

[root@svr7 ~]# > /etc/resolv.conf    #加快httpd服务启动

[root@svr7 ~]# systemctl  restart  httpd #重启httpd服务

[root@svr7 ~]# systemctl  status  httpd  #查看服务httpd状态

[root@svr7 ~]# systemctl  enable  httpd #设置httpd开机自启动

Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.

 

[root@svr7 ~]# systemctl is-enabled httpd #查看httpd是否是开机自启动

[root@svr7 ~]# systemctl disable httpd  #关闭httpd开机自启动

Removed symlink /etc/systemd/system/multi-user.target.wants/httpd.service.

[root@svr7 ~]# systemctl is-enabled httpd
```



####  **管理运行级别**

RHEL6:运行级别 300

0：关机    0个服务

1：单用户模式（基本功能的实现，破解Linux密码）   50个服务

2：多用户字符界面（不支持网络）    80个服务

***\*3：多用户字符界面（支持网络）服务器默认的运行级别\****  100个服务

4：未定义    0个服务

***\*5：图形界面    300个服务\****

6：重起      0个服务

> 切换运行级别：init  数字 

 



#### RHEL7：运行模式（运行级别）  

> 字符模式：multi-user.target
>
> 图形模式：graphical.target

 

 当前直接切换到字符模式 

```shell
[root@svr7 /]# systemctl  isolate  multi-user.target   #相当于原来的init  3
```

 当前直接切换到图形模式

```shell
[root@svr7 /]# systemctl  isolate  graphical.target   #相当于原来的init  5
```

 

查看每次开机默认进入模式

```shell
[root@svr7 /]# systemctl  get-default
```

设置永久策略，每次开机自动进入multi-user.target 

```shell
[root@svr7 /]# systemctl  set-default  multi-user.target 

[root@svr7 /]# reboot 
```

 

###  二、虚拟机A环境准备



#### 1.关机虚拟机A

#### 2.修改内存大小，建议6G

![img](./01image/001.png) 

![img](./01image/002.png) 

 

#### 3.添加一块新的硬盘50G

![img](./01image/003.png) 

![img](./01image/004.png) 

![img](./01image/005.png) 

![img](./01image/006.png) 

![img](./01image/007.png) 

![img](./01image/009.png) 

![img](./01image/010.png) 

![img](./01image/011.png) 

 

#### 4.CPU开启虚拟化功能

![img](./01image/012.png) 

![img](./01image/013.png) 

#### 5.开启虚拟机



### 三 、空间规划

#### 情况一：根设备为逻辑卷

```shell
[root@svr7 ~]# vgextend centos /dev/sdb  #扩展卷组

 Physical volume "/dev/sdb" successfully created.

 Volume group "centos" successfully extended

[root@svr7 ~]# vgs  #查看卷组信息

 

[root@svr7 ~]# lvextend -L 40G /dev/centos/root  #扩展空间

[root@svr7 ~]# lvs   #查看逻辑卷空间大小

[root@svr7 ~]# xfs_growfs  /dev/centos/root  #刷新文件系统

[root@svr7 ~]# df -h  #查看文件系统的大小
```

 

#### 情况二：根设备为基本分区

```shell
[root@svr7 ~]# fdisk  /dev/sdb

命令(输入 m 获取帮助)：n

Partition type:

p primary (0 primary, 0 extended, 4 free)

e extended

Select (default p): #回车

Using default response p

分区号 (1-4，默认 1)： #回车

起始 扇区 (2048-83886079，默认为 2048)： #回车

将使用默认值 2048

Last 扇区, +扇区 or +size{K,M,G} (2048-83886079，默认为 83886079)： #回车

将使用默认值 83886079

分区 1 已设置为 Linux 类型，大小设为 50 GiB

命令(输入 m 获取帮助)：w #保存并退出

The partition table has been altered!

Calling ioctl() to re-read partition table.

正在同步磁盘。

[root@svr7 ~]# lsblk 

[root@svr7 ~]# mkfs.xfs  /dev/sdb1 #格式化文件系统

[root@svr7 ~]# mkdir  /iso

[root@svr7 ~]# mount  /dev/sdb1  /iso

[root@svr7 ~]# vim  /etc/fstab 

/dev/sdb1  /iso  xfs  defaults  0 0

[root@svr7 ~]# umount  /iso #卸载设备

[root@svr7 ~]# df  -h

[root@svr7 ~]# mount -a #检测/etc/fstab是否书写正确

[root@svr7 ~]# df -h
```

### 四、 传递系统光盘镜像文件    

```
[root@svr7 ~]# mkdir  /iso       
```

![img](./01image/014.png) 

```shell
[root@svr7 ~]# du -sh  /iso/

8.8G	 /iso/

[root@svr7 ~]# ls  /iso/

CentOS7-1804.iso

[root@svr7 ~]#
```

 

### 五、Linux平台构建虚拟化

 

#### • KVM虚拟化主要软件包

> – qemu-kvm :为 kvm 提供底层仿真支持
>
> – libvirt-daemon :libvirtd 守护进程，管理虚拟机
>
> – libvirt-client :用户端软件，提供客户端管理命令
>
> – libvirt-daemon-driver-qemu :libvirtd 连接 qemu 的驱动
>
> – virt-manager :图形管理工具

```shell
[root@svr7 ~]# yum -y install qemu-kvm

[root@svr7 ~]# yum -y install libvirt-daemon

[root@svr7 ~]# yum -y install libvirt-client #***

[root@svr7 ~]# yum -y install libvirt-daemon-driver-qemu

[root@svr7 ~]# yum -y install virt-manager #***

 

[root@svr7 ~]# virt-manager  #开启虚拟机图形管理工具
```

![img](./01image/015.png) 

 

### 六、新建虚拟机

```
[root@svr7 ~]# virt-manager  #开启虚拟机图形管理工具
```

![img](./01image/016.png) 

![img](./01image/017.png) 

![img](./01image/018.png) 

![img](./01image/019.png) 

![img](./01image/020.png) 

![img](./01image/021.png) 

安装系统

![img](./01image/022.png) 

 

![img](./01image/023.png) 

 

![img](./01image/024.png) 

 

![img](./01image/025.png) 

 

![img](./01image/026.png) 

 

设置root的密码

![img](./01image/027.png) 

 

### 七、管理虚拟机的命令

虚拟机A：KVM服务器

#### • 查看KVM节点（服务器）信息：virsh  nodeinfo

#### • 列出虚拟机: virsh  list  [--all]

```shell
[root@svr7 ~]# virsh list 

[root@svr7 ~]# virsh  shutdown  nsd01  #关闭虚拟机nsd01

[root@svr7 ~]# virsh list       #仅列出正在运行的虚拟机

[root@svr7 ~]# virsh list --all   #列出运行与关闭的所有虚拟机

[root@svr7 ~]# virsh  start nsd01   #开启虚拟机nsd01

[root@svr7 ~]# virsh list 
```

#### • 查看指定虚拟机的信息:virsh  dominfo  虚拟机名称

```shell
[root@svr7 ~]# virsh dominfo nsd01

[root@svr7 ~]# virsh autostart  nsd01   #设置开机自启动

[root@svr7 ~]# virsh dominfo nsd01
```

 

```
[root@svr7 ~]# virsh autostart  nsd01  --disable  #禁止开启自启动

[root@svr7 ~]# virsh dominfo nsd01
```

#### • 强制关闭指定的虚拟机: virsh  destroy  虚拟机名称

```
[root@svr7 ~]# virsh  destroy  nsd01

域 nsd01 被删除

[root@svr7 ~]# virsh  list  --all
```



### 八、KVM虚拟机的组成

> – xml配置文件：定义虚拟机的名称、UUID、CPU、内存、虚拟磁盘、网卡等各种参数设置
>
> /etc/libvirt/qemu：xml配置文件默认存放路径
>
> 
>
> – 磁盘镜像文件：保存虚拟机的操作系统及文档数据，镜像路径取决于xml配置文件中的定义
>
> /var/lib/libvirt/images/:磁盘镜像文件默认存放路径

### 九、命令行手动克隆虚拟机

#### 1.产生新的磁盘镜像文件

```shell
[root@svr7 ~]# virsh  destroy  nsd01  #强制关闭虚拟机

[root@svr7 ~]# virsh  list  --all

[root@svr7 ~]# cd  /var/lib/libvirt/images/

[root@svr7 images]# cp  nsd01.qcow2  nsd02.qcow2

[root@svr7 images]# ls

nsd01.qcow2   nsd02.qcow2

[root@svr7 images]#
```

#### 2.建立xml配置文件 

```shell
[root@svr7 /]# cd  /etc/libvirt/qemu/

[root@svr7 qemu]# cp  nsd01.xml  nsd02.xml

[root@svr7 qemu]# ls

autostart  networks  nsd01.xml  nsd02.xml

[root@svr7 qemu]# vim  /etc/libvirt/qemu/nsd02.xml

 虚拟机的名字：<name>nsd02</name>

 虚拟机UUID整行删除：<uuid>1b0f……….535</uuid>

 磁盘镜像文件路径：<source file='/var/lib/libvirt/images/nsd02.qcow2'/>

 网卡的MAC地址整行删除： <mac address='52:5……….:cb'/>
```

#### 3．导入xml配置文件 

```shell
]# virsh define  /etc/libvirt/qemu/nsd02.xml   #导入

]# virsh list --all    #显示系统所有虚拟机

]# virsh start nsd02  #开启nsd02虚拟机

]# virt-manager     #开启图形虚拟机管理工具

]# virsh destroy nsd02  #强制关闭   
```



### 十、命令行手动克隆虚拟机（再来一遍）

#### 1.产生新的磁盘镜像文件

```shell
[root@svr7 ~]# virsh  destroy  nsd01  #强制关闭虚拟机

[root@svr7 ~]# cd  /var/lib/libvirt/images/

[root@svr7 images]# cp  nsd01.qcow2  dc01.qcow2

[root@svr7 images]# ls
```

#### 2.建立xml配置文件 

```shell
[root@svr7 /]# cd  /etc/libvirt/qemu/

[root@svr7 qemu]# cp  nsd01.xml  dc01.xml

[root@svr7 qemu]# vim  /etc/libvirt/qemu/dc01.xml

 虚拟机的名字：<name>dc01</name>

 虚拟机UUID整行删除：<uuid>1b0f……….535</uuid>

 磁盘镜像文件路径:<source file='/v………/es/dc01.qcow2'/>

 网卡的MAC地址整行删除：<mac address='52:5……….:cb'/>
```

#### 3．导入xml配置文件 

```shell
]# virsh define  /etc/libvirt/qemu/dc01.xml   #导入

]# virsh list --all    #显示系统所有虚拟机

]# virsh start dc01  #开启dc01虚拟机

]# virt-manager     #开启图形虚拟机管理工具

]# virsh destroy dc01  #强制关闭
```



### 十一、命令行手动克隆虚拟机（再再来一遍）

#### 1.产生新的磁盘镜像文件

```shell
[root@svr7 ~]# virsh  destroy  nsd01  #强制关闭虚拟机

[root@svr7 ~]# cd  /var/lib/libvirt/images/

[root@svr7 images]# cp  nsd01.qcow2  stu01.qcow2

[root@svr7 images]# ls
```

#### 2.virsh edit 三合一命令（建立、修改、导入）

```shell
[root@svr7 images]# virsh  edit  nsd01    #以nsd01为模板

 虚拟机的名字：<name>stu01</name>

 虚拟机UUID整行删除：<uuid>1b0f……….535</uuid>

 磁盘镜像文件路径:<source file='/v………/es/stu01.qcow2'/>

 网卡的MAC地址整行删除：<mac address='52:5……….:cb'/>

[root@svr7 images]# virsh  list  --all  
```

### 十二、快速建立新的虚拟机

COW：写时复制

> •  Copy On Write，写时复制
>
> – 直接映射原始盘(后端盘)的数据内容
>
> – 原始盘(后端盘)内容不变，并且不能修改原始盘内容，否则所有前端盘无法使用
>
> – 对前端盘的修改不回写到原始盘
>
> 
>
> • qemu-img 通过 -b 选项复用指定后端盘
>
> – qemu-img  create  -f  qcow2  -b  后端盘   前端盘

#### 1.产生新的磁盘镜像文件

```shell
]# cd  /var/lib/libvirt/images/

]# qemu-img create  -f   qcow2  -b  nsd01.qcow2  tc01.qcow2

]# qemu-img  info  tc01.qcow2   #查看前端盘信息

………

virtual size: 9.0G (9663676416 bytes)   #虚拟大小

disk size: 196K      #占用磁盘空间真正的大小

backing file: nsd01.qcow2     #原始盘

……….
```

 

#### 2.virsh edit 三合一命令（建立、修改、导入）

```shell
[root@svr7 images]# virsh  edit  nsd01    #以nsd01为模板

 虚拟机的名字：<name>tc01</name>

 虚拟机UUID整行删除：<uuid>1b0f……….535</uuid>

 磁盘镜像文件路径:<source file='/v………/es/tc01.qcow2'/>

 网卡的MAC地址整行删除：<mac address='52:5……….:cb'/>

[root@svr7 images]# virsh  list  --all  

[root@svr7 images]# virsh start  tc01

[root@svr7 images]# virt-manager
```



#### **离线访问虚拟机**

• 使用 guestmount 工具

– 支持离线挂载 raw、qcow2 格式虚拟机磁盘

– 可以在虚拟机关机的情况下，直接修改磁盘中的文档

– 方便对虚拟机定制、修复、脚本维护

​     !!! 需要注意 SELinux 机制的影响

 

• 基本用法

guestmount  -a  虚拟机磁盘路径  -i  /挂载点

```shell
]# guestmount

bash: guestmount: 未找到命令...

]# yum  provides   */guestmount    #查看仓库中那个包产生

]# yum  -y  install  libguestfs-tools-c

]# virsh  destroy  tc01    #关闭虚拟机tc01

]# guestmount  -a  /var/lib/libvirt/images/tc01.qcow2  -i  /mnt/

]# ls  /mnt

]# umount  /mnt  #卸载
```



### 十三、修改用户家目录

```shell
[root@svr7 ~]# useradd  tom

[root@svr7 ~]# grep tom  /etc/passwd

[root@svr7 ~]# ls /home/


[root@svr7 ~]# usermod -d  /opt/mytom  tom

[root@svr7 ~]# grep  tom  /etc/passwd

[root@svr7 ~]# ls /opt/mytom

ls: 无法访问/opt/mytom: 没有那个文件或目录

 
[root@svr7 ~]# ls -A /etc/skel/   #用户家目录配置文件来源的模板目录

[root@svr7 ~]# cp  -r  /etc/skel/    /opt/mytom

[root@svr7 ~]# ls  -A   /opt/mytom

[root@svr7 ~]# chown  -R  tom:tom  /opt/mytom  #修改归属关系

[root@svr7 ~]# ls  -ld  /opt/mytom

[root@svr7 ~]# ls  -lA  /opt/mytom
```

