# KeepAlived

### 1. 简单描述keepalived配置文件字段含义

以下是部分keepalived配置文件的声明，在下面代码的括号处写出关键字段含义：

```nginx
vrrp_instance VI_1 {  
	state MASTER                         //（     ）  
	interface eth0  
	virtual_router_id 51  
	priority 100                         //（     ）  
	advert_int 1  
	authentication {    
		auth_type pass    
		auth_pass forlvs                   //（     ）  
	}
}
```

参考答案

```
建立测试文件：

1）state MASTER：设置主服务器MASTER，辅助为SLAVE；

2）priority 100：设置优先级，主服务器优先级要比辅助的高；

3）auth_pass forlvs：设置密码，主辅服务器密码必须一致。
```



### 2.使用Keepalived实现LVS功能？

要求：虚拟服务器IP地址为192.168.1.1，采用的LVS调度算法为RR，LVS的模式为DR，Real Server的IP地址分别为192.168.1.10和192.168.1.11。只需写出虚拟服务器部分的配置文件。

参考答案

```nginx
virtual_server 192.168.1.1 80 {           //设置虚拟IP为192.168.1.1  
	delay_loop 6  
	lb_algo rr                              //设置LVS调度算法为RR  
	lb_kind DR                           //设置LVS的模式为DR  
	persistence_timeout 50  
	protocol TCP  
	real_server 192.168.1.10 80 {//设置Real Server192.168.1.10    
		weight 3                             //设置权重为3    
		TCP_CHECK {
			connect_timeout 3
			nb_get_retry 3
			delay_before_retry 3    
		}  
	} 
	real_server 192.168.1.11 80 {         //设置Real Server192.168.1.10    
		weight 1       
		TCP_CHECK {
			connect_timeout 3
			nb_get_retry 3
			delay_before_retry 3    
		} 
	}
}
```

# keepalived之vrrp_script详解

> 通常情况下，利用keepalived做热备，其中一台设置为master，一台设置为backup。当master出现异常后，backup自动切换为master。当backup成为master后，master恢复正常后会再次抢占成为master，导致不必要的主备切换。因此可以将两台keepalived初始状态均配置为backup，设置不同的优先级，优先级高的设置**nopreempt**解决异常恢复后再次抢占的问题。

## **1、vrrp_script能做什么**

keepalived只能做到对网络故障和keepalived本身的监控，即当出现网络故障或者keepalived本身出现问题时，进行切换。但是这些还不够，我们还需要监控keepalived所在服务器上的**其他业务进程，比如说nginx，keepalived+nginx实现nginx的负载均衡高可用，如果nginx异常，仅仅keepalived保持正常，是无法完成系统的正常工作的，因此需要根据业务进程的运行状态决定是否需要进行主备切换。这个时候，我们可以通过编写脚本对业务进程进行检测监控。

例如：编写个简单脚本查看haproxy进程是否存活

```shell
\#!/bin/bash 
count = `ps aux | grep -v grep | grep haproxy | wc -l` 
if [ $count > 0 ]; then    
    exit 0 
else    
    exit 1 
fi
```

在keepalived的配置文件中增加相应配置项

```nginx
vrrp_script checkhaproxy {    
	script "/home/check.sh"    
	interval 3    weight -20 
	} 
	vrrp_instance test {    
	...        
	track_script    {        
	checkhaproxy    
	}        
	... 
}
```

## **2、优先级更新策略**

keepalived会**定时执行脚本**并**对脚本执行的结果进行分析**，动态调整vrrp_instance的优先级。

如果脚本执行结果**为0**，并且**weight**配置的值**大于0**，则优先级相应的**增加**

如果脚本执行结果**非0**，并且**weight**配置的值**小于0**，则优先级相应的**减少**

**其他情况，维持原本配置的优先级**，即配置文件中priority对应的值。

这里需要注意的是：

1） 优先级不会不断的提高或者降低

2） **可以编写多个检测脚**本并为每个检测脚本设置**不同的weight**

3） 不管提高优先级还是降低优先级，最终优先级的**范围是在[1,254]**，不会出现优先级小于等于0或者优先级大于等于255的情况

这样可以做到利用脚本检测业务进程的状态，并动态调整优先级从而实现主备切换。

## **3、vrrp_script中节点权重改变算法**

在Keepalived集群中，其实并没有严格意义上的主、备节点，虽然可以在Keepalived配置文件中设置“state”选项为“MASTER”状态，但是这并不意味着此节点一直就是Master角色。控制节点角色的是Keepalived配置文件中的“priority”值，但并它并不控制所有节点的角色，另一个能改变节点角色的是在vrrp_script模块中设置的“weight”值，这两个选项对应的都是一个整数值，其中“weight”值可以是个负整数，一个节点在集群中的角色就是通过这两个值的大小决定的。

### **3.1、不设置weight**

在vrrp_script模块中，如果**不设置“weight”选项值**，那么集群优先级的选择将由Keepalived配置文件中的“priority”值决定，而在需要对集群中优先级进行灵活控制时，可以通过在vrrp_script模块中设置“weight”值来实现。

### **3.2、设置weight**

**vrrp_script 里的script返回****值为0****时认为****检测成功****，****其它值****都会当成****检测失败****；**

1. **weight 为正时**，**脚本检测成功时此weight会加到priority上**，检测失败时不加；

- 1. 主失败:

- - 1. 主 priority < 从 priority + weight 时会切换。

- 1. 主成功：

- - 1. 主 priority + weight > 从 priority + weight 时，主依然为主

1. **weight 为负时**，脚本检测成功时此weight不影响priority，**检测失败时priority – abs(weight)**

- 1. 主失败:

- - 1. 主 priority – abs(weight) < 从priority 时会切换主从

- 1. 主成功:

- - 1. 主 priority > 从priority 主依然为主

## **4、配置不抢占nopreempt带来的问题**

 

例如：A,B两台keepalived

A的配置大概为：

```nginx
vrrp_script checkhaproxy {    
	script **"/etc/check.sh"**    
	interval 3    
	weight -20 
} 
vrrp_instance test {    
....        
	state backup    
	priority 80    
	nopreempt     
	track_script    {        
		checkhaproxy    
	}     
.... 
}
```

 

B的配置大概为：

```nginx
vrrp_script checkhaproxy {    
    script **"/etc/check.sh"**    
        interval 3    
        weight -20 
} 
vrrp_instance test {    
    ....        
        state backup    
        priority 70     
        track_script    {        
        	checkhaproxy    
    }     
	.... 
}
```

 

A,B同时启动后，由于A的优先级较高，因此通过选举会成为master。当A上的业务进程出现问题时，优先级会降低到60。此时B收到优先级比自己低的vrrp广播包时，将切换为master状态。那么当B上的业务出现问题时，优先级降低到50，尽管A的优先级比B的要高，但是由于设置了nopreempt，A不会再抢占成为master状态。

所以，可以在检测脚本中增加杀掉keepalived进程（或者停用keepalived服务）的方式，做到业务进程出现问题时完成主备切换。