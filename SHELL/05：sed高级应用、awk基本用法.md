## 05：sed高级应用、awk基本用法



### **sed中的替换，使用 s/old/new/**

```shell
vim test.txt   //准备素材,写入下列内容 

2017 2011 2018

2017 2017 2024

2017 2017 2017
```

 

```shell
sed 's/2017/8888/' test.txt  //替换所有行的第1个2017为8888

sed '1s/2017/8888/' test.txt  //替换第1行的第1个2017

sed '2s/2017/8888/' test.txt  //替换第2行

sed '1,2s/2017/8888/' test.txt  //替换1~2行

sed 's/2017/8888/2' test.txt  //替换所有行的第2个2017

sed 's/2017/8888/3' test.txt  //替换所有行的第3个2017

sed '/2011/s/2017/8888/' test.txt  //替换有2011的行的第1个2017

sed 's/2017/8888/g' test.txt  //替换所有行的所有个

sed -n 's/root/6666/p'  /etc/passwd  //替换每行第1个root，并只显示被替换的行

sed -n 's/root/6666/2p'  /etc/passwd  //替换每行第2个root，并只显示被替换的行

sed -n 's/root/6666/gp'  /etc/passwd //替换每行的所有root

sed '3s/2017/8888/;3s/2017/8888/' test.txt  //替换第3行的第1个和第2个2017
```

\------------------------------------------------------------

#### **尝试将user文档中的/bin/bash替换成/sbin/sh**

```shell
sed 's//bin/bash//sbin/sh/' user   //常规手段替换，内容会与替换 冲突，替换失败

sed 's/\/bin\/bash/\/sbin\/sh/' user  //使用\转义符号，可以成功，但不方便

sed 's!/bin/bash!/sbin/sh!' user  //更改替换的间隔符号是最理想方法

sed 's(/bin/bash(/sbin/sh(' user  //效果同上
```

 

### **Sed中s替换的高级应用**

#### **1）删除文件中每行的第二个、最后一个字符**

```shell
sed 's/.//2;s/.$//' test   分两个步骤完成，中间用分号

隔开，第一步将每行的第二个任意字符替换成空，第二

步将每行的最后一个任意字符替换成空即可
```

 

#### **2）删除文件中所有的数字**  

```shell
sed -i 's/[0-9]//g' test	先找到任意数字，然后都替换成空
```

#### **3）将文件中每行的第一个、倒数第1个字符互换**

```shell
echo abc > abc			//创建素材

sed -r 's/(a)(b)(c)/\3\2\1/' abc  //替换abc时复制每一个字符，后面替换的内容用\数字粘贴，可以任意调换位置

echo xyz >> abc  //追加内容

sed -r 's/(a)(b)(c)/\3\2\1/' abc  //再用之前的方法无法替换第2行

sed -r 's/(.)(.)(.)/\3\2\1/' abc  //将具体字符用 . 替代即可将文档中的xyz修改成xyaz时，上述方案失灵

sed -r 's/^(.)(.*)(.)$/\3\2\1/' abc  //再次升级，分别找到第1个字符和最后1个字符，中间可以是任意

sed -r 's/^(.)(.*)(.)$/\3\2\1/' test  //达成需求
```

 

#### **4）为文件中每个大写字母添加括号**

```shell
sed -r 's/([A-Z])/(\1)/g' test  //先找到任意大写字母，然后保留，最后替换成带括号的状态
```

 

**练习：**

**编写脚本，安装httpd服务，使用82号端口**

```shell
#!/bin/bash

yum -y install httpd

sed -i '/^Listen 80/s/80/82/' /etc/httpd/conf/httpd.conf  //找到有Listen 80开头的行，替换80为82

systemctl restart httpd
```

 

运行脚本之后，使用curl 192.168.4.7:82测试 或者火狐浏览器192.168.4.7:82

要关闭selinux与防火墙

```shell
setenforce 0   //如果脚本执行之前没关闭selinux，会报错

systemctl stop firewalld
```

\--------------------------------------------------------------------

### **sed中除了常用的p d s 三个指令，还有 a行下追加  i行上添加\  c替换整行**

```shell
sed 'a 666' user //在所有行下追加666

sed '1a 666' user //第1行下追加666

sed 'i 666' user  //所有行上添加666

sed '3i 666' user //第3行上添加666

sed '2a 666' user  //第2行下追加666

sed 'c 666' user  //所有行替换成666

sed '/root/c 666' user  //找到有root的行替换成666 
```

 

**练习：**

**找到使用bash做解释器的用户名**

```shell
sed -n "/bash$/s/:.*//p" /etc/passwd  //找到

使用bash的用户，将用户信息中从冒号到后面

的所有内容替换成空，留下的就是用户名
```

 

**练习：**

**编写脚本，找到使用bash解释器的用户，并按照 用户名 --> 密码” 的格式保存到log文件中**

```shell
#!/bin/bash

u=$(sed -n '/bash$/s/:.*//p' /etc/passwd)  //找到使用bash的人，并且将这些用户的名字存到变量u中

for i in $u  //将这些人交给for循环处理

do

    u1=$(grep $i: /etc/shadow)  //用每个人名找到密码文件中shadow中对应的信息

    u2=${u1#*:}  //然后掐头

    u3=${u2%%:*}  //再去尾，得到纯粹的密码

    echo "$i --> $u3"   //再按格式输出,如果需要保存就用 >> log 

done
```

\----------------------------------------------------------

 

**在linux中常用的三个处理文档的工具：**

**grep 模糊搜索**

**sed 增删改查**

**awk  精确搜索**

 

### **awk使用方式：**

#### **1，前置指令 | awk 选项 条件 指令**

#### **2，awk  选项 条件 指令  被处理文档**

**选项 -F 定义分隔符**  

**指令 print 输出**

```shell
vim test.txt   //准备素材，写入下列两行内容

hello the world

welcome to beijing
```

 

**awk的内置变量：  $1第1列  $2第2列  $3第3列  $0所有列  NR 行号  NF列号**

```shell
awk '{print}' test.txt  //输出test所有行所有列

awk '{print $1}' test.txt //输出test所有行第1列

awk '{print $2}' test.txt //输出test所有行第2列

awk '/world/{print $2}' test.txt  //找到有world的行，然后输出第2列

awk '{print $0,$1}' test.txt //找所有行的所有列和第1列

awk '{print NR}' test.txt //输出每行的行号

awk -F: '{print $1" 的解释器是 "$7}'  user  //使用冒号作为分隔符,输出第1列，第7列，中间加常量，常量要使用双引号
```

 

**利用awk提取本机的网络流量信息**

```shell
ifconfig eth0 | awk '/RX p/{print "eth0网卡的接收流量是"$5"字节"}'

ifconfig eth0 | awk '/TX p/{print "eth0网卡的发送流量是"$5"字节"}'
```

 

**使用awk提取根分区剩余容量**

```shell
df -h | awk '/\/$/{print "根分区剩余容量是"$4}'
```

 

**awk处理的时机，awk除了可以执行大括号中的逐行任务，还可以安排额外的任务**

**BEGIN{  }  执行1次，相当于额外任务**

**{  } 逐行任务，执行n次**

**END{  }  执行1次，又一个额外的任务**

 

**练习：**

**按下列各式要求输出信息**

![img](E:%5CHJCloudComputingNotes%5Clinux-cloud-computing%5CSHELL%5C06image%5C001.png) 

```shell
awk -F: ‘BEGIN{  }{  }END{  }’  user  //首先分析，写出基本各式

awk 'BEGIN{print "User\tUID\tHome"}'  //第一个是BEGIN任务就是输入表头信息,\t是制表符，相当于在文档中敲tab键，可以在一定程度上让文档自动空格排列整齐

awk -F: '{print $1"\t"$3"\t"$6}' user  //第二个是逐行任务，找到用户名，id号，家目录

awk -F: 'END{print "总计"NR"行"}' user  //最后的任务是输出一共多少行，NR是变量可以显示行号，在END任务中就是显示最后一行的行号

awk -F: 'BEGIN{print "User\tUID\tHome"}{print $1"\t"$3"\t"$6}END{print "总计"NR"行"}' user  //将上述命令敲成一行，完成需求
```

\-----------------------------------------------------------------------------------------------------------------





