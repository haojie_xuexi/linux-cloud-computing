## 06：awk高级应用、综合案例

**在awk中，使用好条件是能否精确输出信息的关键，awk准备了4种条件，下面一一说明。**

**awk中的条件有：**

### **1，** **使用正则作为条件**

**~ 包含  !~ 不包含**

```shell
awk -F: '/bin/{print $1}' user   //输出有bin的行的第1列

awk -F: '$5~/bin/{print $1}' user  //输出第5列包含bin的行的第1列

awk -F: '$5!~/bin/{print $1}' user  //输出第5列不包含bin的行的第1列
```

 

### **2，** **使用数字、字符串**

**==  !=  >  <  >=  <=**

```shell
awk -F: 'NR==3{print}' /etc/passwd  //输出第3行

awk -F: '$3<10{print}' /etc/passwd  //输出UID小于10的行

awk -F: '$3>=1000{print}' /etc/passwd //输出UID大于等于1000的行

awk -F: '$7=="/bin/bash"{print}' /etc/passwd  //输出使用/bin/bash的用户

awk -F: '$7!="/bin/bash"{print}' /etc/passwd  //输出没使用/bin/bash的用户

awk -F: 'NR<11{print $1}' /etc/passwd  //查看前10行，并显示用户名

awk -F: '$7==" /bin/bash"{print $6}' /etc/passwd //查看使用/bin/bash解释器的用户，并显示家目录
```

 

### 3，**逻辑组合  &&并且  ||或者**

```shell
awk -F: '$3<10&&$7~/bash/{print}' /etc/passwd  //找第3列小于10并且第7列包含bash的行，{print}可以省略

awk -F: '$3>=10&&$3<=20{print}' /etc/passwd  //找第3列大于等于10并且小于等于20的行

awk -F: 'NR>=2&&NR<=10{print}' /etc/passwd  //找2~10行


awk -F: '$3<5||$3>1000{print}' /etc/passwd  //找id号是小于5的或者大于1000的 
```

  

### **4，使用运算作为条件**

```shell
seq 200 | awk '$1%7==0&&$1~/7/{print}'  //在一个有200行从1~200的数字序列的文档中找能被7整除的行,并且包含7
```

\---------------------------------------------------------------------------------------

**在awk中使用if分支**

**单分支，如果满足条件就执行指令，不满足就不执行任何指令**

**{if(条件){指令}}**

```shell
awk -F: 'BEGIN{x=0}{if($7~/bash/){x++}}END{print x}' /etc/passwd //首先定义变量x=0，然后使用if判断如果每找到一行的$7包含bash，就把x+1，所有逐行任务结束后，最终使用end任务输出x的值，也就是找系统中使用bash作为解释器的用户数量
```

 

**双分支， 如果满足条件就执行指令，不满足就执行else后面的指令**

**{if (条件){指令}else{指令}}**

```shell
awk -F: 'BEGIN{x=0;y=0 }{if($7~/bash/){x++}else{y++}}END{print x,y}' /etc/passwd
```

 统计系统中使用bash作为解释器的用户，和没有使用bash的用户数量,使用if判断如果每找到一行的$7包含bash，就把x+1，否则y+1，最后使用end输出x与y的值

 

**多分支**

**{if (条件){指令}else if (条件){指令}else{指令}}**

```
awk -F: '{if($7~/bash/){x++}else if($7~/nologin/){y++}else{z++}}END{print x,y,z}' /etc/passwd  //统计系统中使用bash作为解释器的用户，使用nologin的用户，还有其他用户的数量
```

\------------------------------------------------------------------------------------------------

**数组，相当于可以存储多个值的特殊变量**

**数组名[下标]=该数组下标对应的值**

```shell
a[1]=10

a[2]=20
```

**在awk中使用for循环遍历数组**

**for(变量名称 in数组名称){print 变量名称 }**

 

**基本用法示例1：**

```shell
awk 'BEGIN{a[1]=10;a[2]=20;for(i in a){print i}}'  //首先创建数组，名字叫a，并且有1与2两个下标，还有对应的10与20两个值，然后为了方便查看该数组中所有内容，使用了for循环，可以循环显示所有数组a的下标，然后输入变量i，此时的变量i就可以分别显示数组a的所有下标
```

 

**基本用法示例**2：

```shell
awk 'BEGIN{a[1]=10;a[2]=20;for(i in a){print a[i]}}'  //定义好了数组之后，循环显示数组的值

awk 'BEGIN{abc["a"]="aaa";abc["b"]="bbb";abc["c"]="ccc";for(i in abc){print i,abc[i]}}'  //定义好了数组之后循环显示数组的下标与值
```

 

**基本用法示例3：**

准备素材

```vim
vim test

abc

xyz

opq

abc

abc

xyz
```

 

\-------------------------------------

使用awk的数组进行逐行任务时收集数据的思路过程：

```shell
abc   a[abc]  a[abc]=1

xyz		a[xyz]  a[xyz]=1

opq   a[opq]  a[opq]=1

abc   a[abc]   a[abc]=2

abc   a[abc]   a[abc]=3

xyz		a[xyz]	a[xyz]=2
```

\---------------------------------------------------------------------------------

**实际应用1：**

**使用awk统计网站访问量**

```shell
yum -y remove httpd  //如果httpd有问题可以先删除

yum -y install httpd	 //安装httpd

systemctl restart httpd  //开启服务

netstat -ntulp | grep :80  //查询当前80端口状态

echo "web_test~~~~" > /var/www/html/index.html  //定义网站页面

yum -y install curl  //安装curl工具

curl 192.168.4.7  //查看网站内容，防火墙要关闭

cd /var/log/httpd  //进入httpd日志目录

tail -1 access_log  //查看httpd日志的最后一行，也就是最新记录，然后多用不同主机访问该网站，可以让该日志多产生记录
```

 

```shell
awk '{ip[$1]++}END{for(i in ip){print ip[i],i} }' access_log  //创建数组名叫ip，下标匹配日志文件中每行的第1列，然后收集数据，也就是收集那些ip地址曾经访问过你的网站，最后在END任务中使用for循环显示这些用户的访问次数与ip
```

 

**实际应用2：**

**使用awk统计多少ip曾经尝试登陆你的服务器**

```shell
awk '/Failed pas/{ip[$11]++}END{for(i in ip){print ip[i],i} }' /var/log/secure  //找到安全日志secure中含有Failed pas的行，该行表示有用户登录服务器时输入错误了密码，从该行的第11列可以筛选出对方的ip，并通过数组进行收集，然后通过for循环显示有多少ip(人)尝试登录过几次我的服务器
```

 

\----------------------------------------------------

**练习：**

**编写脚本，收集系统中各种信息，方便查看**

```shell
#!/bin/bash

while :

do

uptime | awk '{print "cpu的15分钟平均负载是"$NF}'

ifconfig eth0 | awk '/RX p/{print "eth0网卡的接收流量是"$5"字节"}'

free -h | awk '/^Mem/{print "剩余内存是"$4}'

df -h | awk '/\/$/{print "磁盘根分区剩余容量是"$4}'

awk 'END{print "账户总数是"NR"个"}' /etc/passwd

echo "当前开启的进程数量是$(ps aux | wc -l)个"

echo "当前登录的用户数量是$(who | wc -l)个"

echo "总共安装的软件包数量是$(rpm -qa | wc -l)个"

sleep 3			//休息3秒

clear 			//清屏

done
```

