## 01：基本权限和归属、附加权限、ACL策略管理

### 权限和归属关系

- #### 基本权限

  > **–  读取：允许查看内容-read 利用r表示**
  >
  > **–  写入：允许修改内容-write 利用w表示**
  >
  > **–  可执行：允许运行和切换-excute 利用x表示**

- #### 对于文本文件

  >  **读取权限(r)：cat、head、tail、less、grep**
  >
  >  **写入权限(w)：vim（保存退出）、>、>>**
  >
  >  **可执行权限(x)：Shell脚本、Python脚本**

- #### 归属关系

  > **–  所有者：拥有此文件/目录的用户-user 利用u表示**
  >
  > **–  所属组：拥有此文件/目录的组-group 利用g表示**
  >
  > **–  其他用户：除所有者、所属组以外的用户-other 利用o表示**

- #### 执行 ls -l或ls  -ld命令查看文件/目录权限

  > **以-开头：表示文本文件**
  >
  > **以d开头：表示目录**
  >
  > **以l开头:快捷方式或者链接文件**

```
[root@A ~]# ls  -l   /etc/shadow
[root@A ~]# ls  -l   /etc/passwd
[root@A ~]# ls  -ld   /etc/
[root@A ~]# ls  -ld   /root
[root@A ~]# ls  -ld   /home/zhangsan
[root@A ~]# ls  -ld   /tmp     #默认具备附加权限的目录

```



### 修改权限

#### **chmod命令**

> **–**  **格式：chmod [ugoa] [+-=] [rwx]   文件...**

- 常用命令选项

> **–  -R：递归修改权限**

```shell
[root@A /]# mkdir /nsd01
[root@A /]# ls -ld /nsd01

[root@A /]# chmod u-w /nsd01        #修改所有者权限
[root@A /]# ls -ld /nsd01

[root@A /]# chmod   u+w   /nsd01       #修改所有者权限
[root@A /]# ls -ld  /nsd01
[root@A /]# chmod   g=rwx   /nsd01    #修改所属组权限
[root@A /]# ls -ld  /nsd01

[root@A /]# chmod u=rwx,g=rx,o=---   /nsd01
[root@A /]# ls -ld /nsd01

[root@A /]# chmod  a=rwx    /nsd01     #赋予所有人rwx权限
[root@A /]# ls -ld /nsd01

[root@A /]# mkdir  -p    /opt/aaa/bbb/ccc
[root@A /]# ls -R   /opt/aaa

[root@A /]# chmod  -R   o=---   /opt/aaa    #递归设置权限
[root@A /]# ls  -ld   /opt/aaa
[root@A /]# ls  -ld   /opt/aaa/bbb/
[root@A /]# ls  -ld   /opt/aaa/bbb/ccc/

```



#### **Linux 判断用户具备的权限**

> **1.首先判断，用户对于该数据，处于的身份(角色)**
>
> **2.查看数据，相应身份的权限位置，权限内容**

•    **用户对于目录具备权限**

   **读取权限(r)：可以查看目录内容**

   **写入权限(w)：可以新建、删除、改名,目录的内容，对目录本身没有修改权限**

   **可执行权限(x)：可以进入切换到该目录下(一位用户能否切换到一个目录只和x执行权限有**关) 



#### **案例1：设置基本权限**

##### 1）以root身份新建/nsddir1/目录，在此目录下新建readme.txt文件

```
[root@localhost ~]# mkdir /nsddir1

[root@localhost ~]# echo 123456 > /nsddir1/readme.txt

[root@localhost ~]# cat /nsddir1/readme.txt
```

##### 2）使用户zhangsan能够修改readme.txt文件内容

```
[root@localhost ~]# chmod  o+w  /nsddir1/readme.txt
```

##### 3）使用户zhangsan不可以修改readme.txt文件内容

```
[root@localhost ~]# chmod  o-w  /nsddir1/readme.txt
```

##### 4）使用户zhangsan能够在此目录下创建/删除子目录

```
[root@localhost ~]# chmod  o+w  /nsddir1/

[root@localhost ~]# ls  -ld  /nsddir1/
```

##### 5）调整此目录的权限，使任何用户都不能进入，然后测试用户zhangsan是否还能修改readme.txt（测试结果不能，对父目录没有权限）

```
[root@localhost ~]# chmod  a-x  /nsddir1/
```

##### 6）为此目录及其下所有文档设置权限 rwxr-x---

```
[root@localhost ~]# chmod -R u=rwx,g=rx,o=--- /nsddir1/
```



#### **权限位的8进制数表示**

> **r、w、x分别对应4、2、1，后3组分别求和**

| **分组** |   User权限    |  Group权限  |  Other权限  |
| :------: | :-----------: | :---------: | :---------: |
| **字符** | **r   w   x** | **r  -  x** | **r  -  x** |
| **数字** |  **4  2  1**  | **4  0  1** | **4  0  1** |
| **求和** |     **7**     |    **5**    |    **5**    |

```shell
[root@A /]# mkdir /nsd03
[root@A /]# ls -ld  /nsd03
[root@A /]# chmod  700  /nsd03
[root@A /]# ls  -ld   /nsd03

[root@A /]# chmod  007  /nsd03
[root@A /]# ls  -ld   /nsd03

[root@A /]# chmod  750  /nsd03
[root@A /]# ls  -ld   /nsd03

```

#### 新建文件/目录的默认权限

> **–  一般文件默认均不给 x 执行权限**
>
> **–  其他取决于 `umask` 设置**
>
> **–  目录默认的权限为755，文件的默认权限为644**

```
[root@A /]# umask 
0022
[root@A /]# umask 077      #修改umask值
[root@A /]# umask 
0077
[root@A /]# mkdir /nsd05
[root@A /]# ls -ld /nsd05
drwx------. 2 root root 6 9月   8 14:17 /nsd05
[root@A /]# umask 022

```



### **修改归属关系**

- **chown命令**

> **–  chown 属主 文件...**
>
> **–  chown 属主:属组 文件...**
>
> **–  chown :属组 文件...**

- **常用命令选项**

> **–  -R：递归修改归属关系**

```
[root@A /]# mkdir   /nsd07
[root@A /]# ls -ld   /nsd07
[root@A /]# groupadd    tmooc    #创建tmooc组

[root@A /]# chown    zhangsan:tmooc      /nsd07
[root@A /]# ls -ld /nsd07

[root@A /]# chown   lisi     /nsd07     #单独修改所有者
[root@A /]# ls   -ld   /nsd07 
[root@A /]# chown   :root   /nsd07     #单独修改所属组
[root@A /]# ls -ld    /nsd07

```

#### Linux 判断用户具备的权限     匹配即停止原则

> **1.用户对于该数据处于的身份(角色)   所有者 > 所属组 >其他人**
>
> **2.查看数据，相应身份的权限位置，权限内容**

#### **案例2：归属关系练习**  

##### 1）利用root的身份新建/tarena目录，并进一步完成下列操作

```
[root@localhost ~]# mkdir /tarena
```

##### 2）将/tarena属主设为gelin01，属组设为tmooc组 

```
[root@localhost ~]# useradd  gelin01

[root@localhost ~]# groupadd  tmooc

[root@localhost ~]# chown  gelin01:tmooc   /tarena
```

##### 3）使用户gelin01对此目录具有rwx权限，除去所有者与所属组之外的用户对此目录无任何权限

```
[root@localhost ~]# chmod o=--- /tarena
```

##### 4）使用户gelin02能进入、查看此目录

```
[root@localhost ~]# useradd gelin02

[root@localhost ~]# gpasswd -a gelin02 tmooc 
```

##### 5）将gelin01加入tmooc组，将tarena目录的权限设为450，测试gelin01用户能否进入此目录

```
[root@localhost ~]# gpasswd -a gelin01 tmooc 

[root@localhost ~]# chmod 450 /tarena
```



#### 案例3：   实现lisi用户可以读取/etc/shadow文件内容，您有几种办法？

1. 利用其他人

```
[root@A /]# chmod o+r  /etc/shadow 
```

2. 利用所属组

```
[root@A /]# chown  :lisi  /etc/shadow 

[root@A /]# chmod  g+r  /etc/shadow
```

3. 利用所有者

```
[root@A /]# chown lisi  /etc/shadow

[root@A /]# chmod  u+r  /etc/shadow
```

4. 利用ACL策略

```
[root@A /]# setfacl -m u:lisi:r  /etc/shadow 
```



### 附加权限（特殊权限）

#### **粘滞位，Sticky Bit 权限**

> –  占用其他人（Other）的 x 位
>
> –  显示为 t 或 T，取决于其他人是否有 x 权限
>
> –  适用于目录，用来限制用户滥用写入权
>
> –  在设置了粘滞位的文件夹下，即使用户有写入权限，也不能删除或改名其他用户文档

```
[root@A /]# mkdir   /home/public
[root@A /]# chmod   777   /home/public
[root@A /]# ls -ld   /home/public

[root@A /]# chmod  o+t   /home/public
[root@A /]# ls  -ld   /home/public

```

#### **SGID 权限**

> –  占用属组（Group）的 x 位
>
> –  显示为 s 或 S，取决于属组是否有 x 权限
>
> –  对目录有效
>
> –  在一个具有SGID权限的目录下，<u>***新建***</u>  的文档会<u>***自动继承***</u>此目录的属组身份

```
[root@A /]# mkdir /nsd10
[root@A /]# ls -ld /nsd10

[root@A /]# chown   :tmooc    /nsd10
[root@A /]# ls   -ld   /nsd10

[root@A /]# mkdir  /nsd10/abc01
[root@A /]# ls -ld  /nsd10/abc01

[root@A /]# chmod  g+s    /nsd10       #赋予SetGID权限
[root@A /]# ls    -ld    /nsd10
[root@A /]# mkdir   /nsd10/abc02
[root@A /]# ls   -ld    /nsd10/abc02

[root@A /]# touch    /nsd10/1.txt
[root@A /]# ls   -l   /nsd10/1.txt

```

> groupadd  caiwu
>
> chown  :caiwu /bjtt
>
> chmod  g+s  /bjtt
>
> /bjtt： o= ---
>
> /bjtt/yg-2020-9-8.txt
>
> /bjtt/yg-2020-9-9.txt
>
> /bjtt/yg-2020-9-10.txt

### ACL策略管理（ACL权限）

#### **acl 访问策略**

> –  **<u>*能够对个别用户、个别组设置独立的权限*</u>**
>
> –  大多数挂载的EXT3/4、XFS文件系统默认已支持

**•   setfacl命令**

> –  格式：setfacl [选项] u:用户名:权限 文件...
>
> ​            setfacl [选项] g:组名:权限 文件...

•   **常用命令选项**

> –  -m：定义一条ACL策略
>
> –  -x：清除指定的ACL策略
>
> –  -b：清除所有已设置的ACL策略
>
> –  -R：递归设置ACL策略

```shell
[root@A /]# mkdir /nsd11
[root@A /]# chmod 770 /nsd11
[root@A /]# ls -ld /nsd11

[root@A /]# su - lisi
[lisi@A ~]$ cd /nsd11
-bash: cd: /nsd11: 权限不够
[lisi@A ~]$ exit

[root@A /]# setfacl   -m   u:lisi:rx     /nsd11   #设置ACL策略
[root@A /]# getfacl   /nsd11                        #查看ACL策略
[root@A /]# su  -  lisi
[lisi@A ~]$ cd     /nsd11
[lisi@A nsd11]$  pwd
[lisi@A nsd11]$   exit
[root@A /]#

```

#### **将某个用户拉黑（制作黑名单）**

```shell
[root@A /]# ls -ld  /home/public/
drwxrwxrwt. 2 root root 41 9月   8 15:53 /home/public/
[root@A /]# setfacl  -m   u:lisi:---   /home/public

```



#### **setfacl 命令练习**

```shell
[root@A /]# mkdir /nsd12
[root@A /]# setfacl -m  u:dc:rwx    /nsd12
[root@A /]# setfacl -m  u:zhangsan:rx   /nsd12
[root@A /]# setfacl -m  u:lisi:rwx   /nsd12
[root@A /]# setfacl -m  u:tom:rwx   /nsd12

[root@A /]# getfacl  /nsd12           #查看ACL策略
[root@A /]# setfacl -x  u:zhangsan  /nsd12  #删除指定ACL
[root@A /]# getfacl  /nsd12
[root@A /]# setfacl -b  /nsd12         #清除所有ACL策略
[root@A /]# getfacl  /nsd12

```



#### **-R : 递归设置 ACL 策略**

```shell
[root@A /]# ls  -R   /opt/aaa
/opt/aaa:
bbb
/opt/aaa/bbb:
ccc
/opt/aaa/bbb/ccc:
[root@A /]# setfacl  -Rm   u:lisi:rx   /opt/aaa
[root@A /]# getfacl   /opt/aaa
[root@A /]# getfacl   /opt/aaa/bbb
[root@A /]# getfacl   /opt/aaa/bbb/ccc

```



### **课后习题**

#### 案例1：chmod权限设置

  1）以root用户新建/nsddir/目录，在该目录下新建文件readme.txt

```shell
[root@Localhost ~]# mkdir /nsddir/
[root@Localhost ~]# touch /nsddir/readme.txt
```

  2）使用户zhangsan能够在/nsddir/目录下创建/删除子目录

```shell
[root@Localhost ~]# useradd zhangsan 
[root@Localhost ~]# chmod o+w /nsddir/
[root@localhost ~]# su -zhangsan
[zhangsan@localhost ~]$ mkdir /nsddir/zhangsan
[zhangsan@localhost ~]$ ls /nsddir
[zhangsan@localhost ~]$ exit
```

  3）使用户zhangsan能够修改/nsddir/readme.txt文件的容

```shell
[root@Localhost ~]# chmod o+w /nsddir/readme.txt
[zhangsan@localhost ~]$ echo xixi >> /nsddir/readme.txt
[zhangsan@localhost ~]$ cat /nsddir/readme.txt
[zhangsan@localhost ~]$ exit
```

#### 案例2：chown归属设置

  1）新建/tarena1目录

​     a）将属主设为gelin01，属组设为tarena组

```shell
[root@Localhost ~]# mkdir /tarena1
[root@Localhost ~]# useradd gelin01
[root@Localhost ~]# groupadd tarena
[root@Localhost ~]# chown gelin01:tarena /tarena1 
```

​     b）使用户gelin01对此目录具有rwx权限，其他人对此目录无任何权限

```shell
[root@Localhost ~]# ls -ld /tarena1
[root@Localhost ~]# chmod o=--- /tarena1
[root@Localhost ~]# ls -ld /tarena1
```

  2）使用户gelin02能进入、查看/tarena1文件夹（提示：将gelin02加入所属组）

```shell
[root@Localhost ~]# useradd gelin02
[root@Localhost ~]# gpasswd -a gelin02 tarena
[root@Localhost ~]# id gelin02
[root@Localhost ~]# su-gelin02
[gelin02@Localhost ~]# cd /tarena1
[gelin02@Localhost ~]# ls
[gelin02@Localhost ~]# exit
```

  3）新建/tarena2目录

​     a）将属组设为tarena

```shell
[root@Localhost ~]# mkdir /tarena2
[root@Localhost ~]# chown :tarena /tarena2
```

​     b）使tarena组的任何用户都能在此目录下创建、删除文件

```shell
[root@Localhost ~]# chmod g+w /tarena2
[root@Localhost ~]# useradd ceshi
[root@Localhost ~]# gpasswd -a ceshi tarena
[root@Localhost ~]# id ceshi
[root@Localhost ~]# su -ceshi
[ceshi@Localhost ~]# mkdir /tarena2/ceshi
[ceshi@Localhost ~]# ls /tarena2
[ceshi@Localhost ~]# exit
```

  4）新建/tarena/public目录

​     a）使任何用户对此目录都有rwx权限

```shell
[root@Localhost ~]# mkdir -p /tarena/public
[root@Localhost ~]# chmod 777 /tarena/public
```

​     b）拒绝zhangsan进入此目录，对此目录无任何权限（提示ACL黑名单）

```shell
[root@Localhost ~]# ls -ld /tarena/public
[root@Localhost ~]# setfacl -m u:zhangsan:--- /tarena/public
[root@Localhost ~]# useradd zhangsan
[root@Localhost ~]# su -zhangsan 
[zhangsan@Localhost ~]# ls /tarena/public
[zhangsan@Localhost ~]# cd /tarena/public
[zhangsan@Localhost ~]# exit
```

 

#### 案例3:权限设置

   1、创建文件夹/data/test,设置目录的访问权限，使所有者和所属组具备读写执行的权限；其他人无任何权限。

```shell
[root@Localhost ~]# mkdir -p /data/test
[root@Localhost ~]# chmod u=rwx,g=rwx,o=--- /data/test 或者  chmod 770 /data/test
[root@Localhost ~]# ls -ld /data/test
```

   2、递归修改文件夹/data/test的归属使所有者为zhangsan，所属组为tarena。

```shell
[root@Localhost ~]# chown -R zhangsan:tarena /data/test
[root@Localhost ~]# ls -ld /data/test
```

   3、请实现在test目录下，新建的所有子文件或目录的所属组都会是tarena。

```shell
[root@Localhost ~]# chmod g+s /data/test
[root@Localhost ~]# mkdir /data/test/abc
[root@Localhost ~]# ls -ld /data/test/abc
```

   4、为lisi创建ACL访问权限，使得lisi可以查看/etc/shadow文件

```shell
[root@Localhost ~]# useradd lisi
[root@Localhost ~]# setfacl -m u:liai:r /etc/shadow
[root@Localhost ~]# getfacl /etc/shadow
[root@Localhost ~]# su-lisi
[lisi@Localhost ~]# cat /etc
```

 

#### 案例4:虚拟机 上操作

   将文件 /etc/fstab 拷贝为 /var/tmp/fstab，并调整文件 /var/tmp/fstab权限 

   满足以下要求：

   – 此文件的拥有者是 root 

   – 此文件对任何人都不可执行

   – 用户 natasha 能够对此文件执行读和写操作 

   – 用户 harry 对此文件既不能读，也不能写 

```shell
[root@Localhost ~]# mkdir -p /var/tmp/fstab
[root@Localhost ~]# cp  /etc/fstab /var/tmp/fstab
[root@Localhost ~]# ls -l /var/tmp/fstab
[root@Localhost ~]# chmod a-x /var/tmp/fstab

[root@Localhost ~]# setfacl -m u:natasha:rw /var/tmp/fstab
[root@Localhost ~]# getfacl /var/tmp/fstab
[root@Localhost ~]# su -natasha
[natasha@Localhost ~]# cat /var/tmp/fstab
[natasha@Localhost ~]# echo ceshi >> /var/tmp/fstab
[natasha@Localhost ~]# cat /var/tmp/fstab
[natasha@Localhost ~]# exit
[root@Localhost ~]# setfacl -m u:harry:--- /var/tmp/fstab
[root@Localhost ~]# getfacl /var/tmp/fstab
[root@Localhost ~]# su-harry
[harry@Localhost ~]# cat /var/tmp/fstab
[harry@Localhost ~]# echo ceshi >> /var/tmp/fstab
[harry@Localhost ~]# exit
```



#### 案例5:虚拟机上操作

   创建一个共用目录 /home/admins，要求如下： 

​    – 此目录的所属组是 adminuser 

​    – adminuser 组的成员对此目录有读写和执行的权限，并且其他用户没有任何权限

​    – 在此目录中创建的文件，其所属组会自动设置为 属于 adminuser 组

```shell
[root@Localhost ~]# mkdir /home/admins
[root@Localhost ~]# groupadd adminuser
[root@Localhost ~]# chown :adminuser /home/admins
[root@Localhost ~]# ls -ld /home/admins
[root@Localhost ~]# chmod g:rwx,o:--- /home/admins
[root@Localhost ~]# chown g+s  /home/admins
[root@Localhost ~]# ls -ld /home/admins

[root@Localhost ~]# mkdir /home/admins/ceshi
[root@Localhost ~]# ls -ld/home/admins/ceshi
```



