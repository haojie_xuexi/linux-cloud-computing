```python
import wmi

# Obtain network adaptors configurations
nic_configs = wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True)

# First network adaptor
nic = nic_configs[0]

# IP address, subnetmask and gateway values should be unicode objects
ip = u'192.168.1.101'
subnetmask = u'255.255.255.0'
gateway = u'192.168.1.1'

# Set IP address, subnetmask and default gateway
# Note: EnableStatic() and SetGateways() methods require *lists* of values to be passed
nic.EnableStatic(IPAddress=[ip],SubnetMask=[subnetmask])
nic.SetGateways(DefaultIPGateway=[gateway])
```

[参考博客1](http://www.blogjava.net/qujinlong123/archive/2007/06/20/125434.html)

[参考博客2](https://www.cnblogs.com/xupanfeng/p/11537484.html)

