## 03：逻辑卷管理、VDO、RAID磁盘阵列、进程管理

### 一、 构建Yum仓库，环境准备

#### 1.光盘文件放入光驱设备

![image-20200912150945474](./03image/001.png)

![image-20200912150956324](./03image/002.png)

#### 2.挂载光驱设备

```shell
[root@localhost ~]# mkdir  /nsd50

[root@localhost ~]# mount  /dev/cdrom  / nsd50 [root@localhost ~]# ls  /nsd50
```

#### 3.书写客户端配置文件

```shell
[root@localhost ~]# rm -rf  /etc/yum.repos.d/*

[root@localhost ~]# vim  /etc/yum.repos.d/nsd.repo

[centos]

name=2008

baseurl=file:///nsd50

enabled=1

gpgcheck=0

[root@localhost ~]# yum  -y  install  httpd

[root@localhost ~]# rpm -q httpd
```

### 二、 添加新的硬盘进行分区

> 添加一块60G的硬盘并规划分区(采用MBR分区模式)
>
> 划分2个10G的主分区,1个12G的主分区,1个20G的逻辑分区。

#### 1.关闭虚拟机添加新的硬盘

```
[root@localhost ~]# poweroff
```

#### 2.进行分区的规划

```
[root@localhost ~]# fdisk  /dev/sdb

p 查看分区表

n 创建主分区----->回车----->回车---->回车----->在last结束时 +10G

n 创建主分区----->回车----->回车---->回车----->在last结束时 +10G

n 创建主分区----->回车----->回车---->回车----->在last结束时 +12G

p 查看分区表

n 创建扩展分区 ----->回车---->起始回车----->结束回车 将所有空间给扩展分区

p 查看分区表

n 创建逻辑分区----->起始回车------>结束+20G

p 查看分区表

w 保存并退出

[root@localhost ~]# lsblk
```

### 三、 逻辑卷简介(虚拟磁盘技术)

作用：1.可以整合分散的空间  2.空间可以扩大

制作过程：将众多的物理卷(PV)，组建成卷组(VG)，再从卷组中划分出逻辑卷(LV)

LVM管理工具集

| ***\*功能\**** | ***\*物理卷管理\**** | ***\*卷组管理\**** | ***\*逻辑卷管理\**** |
| -------------- | -------------------- | ------------------ | -------------------- |
| Scan 扫描      | pvscan               | vgscan             | lvscan               |
| Create 创建    | pvcreate             | vgcreate           | lvcreate             |
| Display 显示   | pvdisplay            | vgdisplay          | lvdisplay            |
| Remove 删除    | pvremove             | vgremove           | lvremove             |
| Extend 扩展    | /                    | vgextend           | lvextend             |

### 四、 逻辑卷的制作 

#### **制作卷组**  

**格式：vgcreate**  **卷组名**   **设备路径……**   

Successfully:成功

```shell
]# ls  /dev/sdb[1-2]

]# vgcreate  systemvg   /dev/sdb1   /dev/sdb2

 Physical volume "/dev/sdb1" successfully created.

 Physical volume "/dev/sdb2" successfully created.

 Volume group "systemvg" successfully created

]# vgs    #显示系统所有的卷组信息

]# pvs    #显示系统所有的物理卷信息
```

 **制作逻辑卷**

格式: lvcreate -L 逻辑卷大小   -n 逻辑卷名字   基于卷组

```shell
[root@localhost ~]# lvcreate  -L  16G   -n   vo    systemvg 

Logical volume "vo" created.
 
[root@localhost ~]# lvs   #查看逻辑卷的信息
[root@localhost ~]# vgs  #查看卷组的信息
```

**逻辑卷的使用**

```shell
]# ls  -l  /dev/systemvg/vo

]# mkfs.xfs  /dev/systemvg/vo   #格式文件系统类型

]# blkid  /dev/systemvg/vo    #查看文件系统类型

]# vim  /etc/fstab

/dev/systemvg/vo   /mylv   xfs   defaults   0  0

[root@localhost ~]# mount  -a

mount: 挂载点 /mylv 不存在

[root@localhost ~]# mkdir   /mylv   #创建挂载点目录

[root@localhost ~]# mount  -a    #测试fstab是否书写正确

[root@localhost ~]# df  -h  #查看正在挂载的设备信息

[root@localhost ~]# lsblk
```



### 五、 逻辑卷的扩展

####  **卷组有足够的剩余空间**

##### **1.扩展空间大小**

```shell
[root@localhost ~]# vgs

[root@localhost ~]# lvs

[root@localhost ~]# lvextend  -L  18G  /dev/systemvg/vo

[root@localhost ~]# lvs


[root@localhost ~]# df  -h  |  tail  -1

/dev/mapper/systemvg-vo  16G  33M  16G   1% /mylv

[root@localhost ~]# lvs

 vo  systemvg  -wi-ao----  18.00g
```

##### **2.扩展文件系统**

resize2fs:刷新ext4文件系统        

xfs_growfs: 刷新xfs文件系统

```shell
[root@localhost ~]# xfs_growfs  /dev/systemvg/vo

[root@localhost ~]# df  -h  |   tail -1

/dev/mapper/systemvg-vo  18G  33M  16G   1% /mylv
```

 

####  **卷组没有足够的剩余空间**

##### **1.扩展卷组空间**

```shell
[root@localhost ~]# vgs

[root@localhost ~]# vgextend  systemvg  /dev/sdb3

[root@localhost ~]# vgs
```

##### **2.扩展逻辑卷空间大小**

```shell
[root@localhost ~]# lvs

[root@localhost ~]# lvextend  -L  25G  /dev/systemvg/vo

[root@localhost ~]# lvs

[root@localhost ~]# df  -h  |  tail  -1

[root@localhost ~]# lvs
```

##### **3.扩展文件系统**    

```shell
[root@localhost ~]# xfs_growfs  /dev/systemvg/vo

[root@localhost ~]# df  -h  |   tail  -1

/dev/mapper/systemvg-vo  25G  33M  16G   1% /mylv
```



### 六、 逻辑卷的补充

####  **逻辑卷也可以缩减(了解)**

 

#####  **卷组划分空间单位**  **PE**

默认情况下PE大小为4M

```shell
]# vgdisplay  systemvg   #查看卷组详细信息

  PE Size        4.00 MiB

]# vgchange  -s   1M   systemvg    #修改PE的大小

]# lvcreate -L  250M  -n  test   systemvg

]# lvcreate -l 50  -n  redhat  systemvg  #利用PE个数

]# lvs
```

 

####  **删除逻辑卷**

##### **1.删除正在使用的逻辑卷**

```shell
[root@localhost ~]# lvremove  /dev/systemvg/vo

 Logical volume systemvg/vo contains a filesystem in use.

[root@localhost ~]# umount  /mylv   #卸载逻辑卷

[root@localhost ~]# lvremove  /dev/systemvg/vo

Do you really want to remove active logical volume systemvg/vo? [y/n]:***\*y\****

 Logical volume "vo" successfully removed

[root@localhost ~]# vim  /etc/fstab  #删除开机自动挂载配置
```

 

##### **2.删除卷组：基于此卷组创建的所有逻辑卷全部删除**

```shell
[root@localhost ~]# lvremove  /dev/systemvg/test

[root@localhost ~]# lvremove  /dev/systemvg/redhat

[root@localhost ~]# vgs

 VG    #PV #LV #SN Attr   VSize  VFree  

 centos    1   2  0 wz--n-  <19.00g    0 

 systemvg  3   0  0 wz--n-  <31.99g <31.99g

[root@localhost ~]# vgremove  systemvg

[root@localhost ~]# vgs

```





### 七、 进程管理

>   程序：静态没有运行的代码  占用硬盘空间
>
>   进程：正在运行的代码  占用CPU与内存的资源
>
>   进程唯一标识:PID
>
>   父进程与子进程    树型结构

 

systemd：所有进程的父进程，***\*上帝进程\****

 

#### **查看进程pstree — Processes Tree**

> – 格式：pstree  [选项]  [PID或用户名]
>
> • 常用命令选项
>
> – -a：显示完整的命令行
>
> – -p：列出对应PID编号 

```shell
[root@localhost ~]# pstree      #查看正在运行的进程信息

[root@localhost ~]# useradd  lisi

[root@localhost ~]# pstree  lisi

未发现进程。

[root@localhost ~]# pstree  lisi

bash───vim

[root@localhost ~]# pstree -p  lisi  #显示进程的PID

bash(6838)───vim(6874) 

[root@localhost ~]# pstree -a  lisi  #显示完成的命令

bash

 └─vim a.txt
```

 

####  **查看进程**

• ps  aux 操作（显示的信息非常详细）   

– 列出正在运行的所有进程

用户 进程ID  %CPU  %内存 虚拟内存 固定内存 终端 状态 起始时间 CPU时间 程序指令 

 

计算正在运行的进程的个数？

```shell
[root@localhost ~]# wc  -l  /etc/passwd

43 /etc/passwd

[root@localhost ~]# ps   aux   |   wc  -l

[root@localhost ~]# ps  -elf   |   wc  -l

[root@localhost ~]# find /etc  -name “*.conf”

[root@localhost ~]# find /etc  -name “*.conf”  |  wc  -l
```

 

• ps  -elf 操作（可以显示进程的父进程PID）

– 列出正在运行的所有进程

 

#### **进程动态排名top 交互式工具**

– 格式：top  [-d  刷新秒数]  [-U  用户名]

```shell
[root@localhost ~]# top

  输入P(大写)，按照CPU进行排序

  输入M(大写)，按照内存进行排序

  输入q退出
```

 

#### **检索进程pgrep — Process Grep**

– 用途：pgrep  [选项]...  查询条件

• 常用命令选项

– -l：输出进程名，而不仅仅是 PID

– -U：检索指定用户的进程

– -x：精确匹配完整的进程名

```
[root@localhost ~]# pgrep  -l  a

[root@localhost ~]# pgrep  -l  c
```

#### **进程的前后台调度**

> • Ctrl + z 组合键
>
> – 挂起当前进程（暂停当前的进程并转入后台）
>
> • jobs 命令
>
> – 查看后台任务列表
>
> • fg 命令
>
> – 将后台任务恢复到前台运行
>
> • bg 命令
>
> – 激活后台被挂起的任务

```shell
[root@localhost /]# ls  /nsd50

[root@localhost /]# mount   /dev/cdrom   /nsd50

[root@localhost /]# yum -y  install  xorg-x11-apps

[root@localhost /]# xeyes  &   #进程正在运行的状态放入后台

[root@localhost /]# firefox &

 
[root@localhost ~]# yum -y  install  xorg-x11-apps

[root@localhost ~]# xeyes

^Z                   #按ctrl +z  暂停放入后台

[1]+  已停止        xeyes

[root@localhost ~]# jobs     #查看后台进程的信息

[1]+  已停止        xeyes

[root@localhost ~]# bg 1    #将后台编号为1 的进程恢复运行

[1]+ xeyes &

[root@localhost ~]# jobs

[1]+  运行中        xeyes &


[root@localhost ~]# fg 1   #将后台编号为1的进程恢复到前台

xeyes

^C     #按ctrl +c 结束进程

[root@localhost ~]#            
```

 

####  **杀死进程**

• 干掉进程的不同方法

– Ctrl+c 组合键，中断当前命令程序

– kill  [-9]  PID... 、kill  [-9]  %后台任务编号

– killall  [-9]  进程名...

– pkill  [-9] 查找条件，包含就可以

```shell
[root@localhost ~]# xeyes &

[root@localhost ~]# xeyes &

[root@localhost ~]# xeyes &

[root@localhost ~]# jobs  -l    #显示后台进程信息，显示PID

[1]  8618 运行中        xeyes &

[2]-  8625 运行中        xeyes &

[3]+  8632 运行中        xeyes &

[root@localhost ~]# kill  8618    #按照PID杀死

[1]  已终止        xeyes

[root@localhost ~]# jobs  -l

[root@localhost ~]# killall   xeyes   #杀死所有xeyes进程
```

 

杀死一个用户开启的所有进程（强制踢出一个用户）

```
[root@localhost ~]# killall  -9  -u  lisi
```

 

### 八、 RAID磁盘阵列

#### • 廉价冗余磁盘阵列

– Redundant Arrays of Inexpensive Disks 

– 通过硬件/软件技术，将多个较小/低速的磁盘整合成一个大磁盘

– 阵列的价值：提升I/O效率、硬件级别的数据冗余

– 不同RAID级别的功能、特性各不相同

![img](./03image/003.png) 

 

#### • RAID 0，条带模式

– 同一个文档分散存放在不同磁盘

– 并行写入以提高效率，没有容错功能

– 至少需要两块磁盘

 

#### • RAID 1，镜像模式

– 一个文档复制成多份，分别写入不同磁盘

– 多份拷贝提高可靠性，效率无提升，有容错功能

– 至少需要两块磁盘

 

#### • RAID5，高性价比模式

– 相当于RAID0和RAID1的折中方案

– 需要至少一块磁盘的容量来存放校验数据

– 至少需要三块磁盘

 

#### • RAID6，高性价比/可靠模式

– 相当于扩展的RAID5阵列，提供2份独立校验方案

– 需要至少两块磁盘的容量来存放校验数据

– 至少需要四块磁盘

 

#### • RAID 0+1/RAID 1+0

– 整合RAID 0、RAID 1的优势

– 并行存取提高效率、镜像写入提高可靠性

– 至少需要四块磁盘

| ***\*对比项\**** | ***\*RAID 0\**** | ***\*RAID 1\**** | ***\*RAID 10\**** | ***\*RAID 5\**** | ***\*RAID 6\**** |
| ---------------- | ---------------- | ---------------- | ----------------- | ---------------- | ---------------- |
| 磁盘数           | ≧ 2              | ≧ 2              | ≧ 4               | ≧ 3              | ≧ 4              |
| 存储利用率       | 100%             | ≦ 50%            | ≦ 50%             | n-1/n            | n-2/n            |
| 校验盘           | 无               | 无               | 无                | 1                | 2                |
| 容错性           | 无               | 有               | 有                | 有               | 有               |
| IO性能           | 高               | 低               | 中                | 较高             | 较高             |



### 九、 VDO 了解内容

•Virtual Data Optimizer（虚拟数据优化器）

–一个内核模块，目的是通过重删减少磁盘的空间占用，以及减少复制带宽

–VDO是基于块设备层之上的，也就是在原设备基础上映射出mapper虚拟设备，然后直接使用即可

 

•重复数据除

–输入的数据会判断是不是冗余数据

–判断为重复数据的部分不会被写入，然后对源数据进行更新，直接指向原始已经存储的数据块即可

•压缩

–对每个单独的数据块进行处理

```
]# yum -y install vdo kmod-kvdo  #所需软件包
```

 

•制作VDO卷

•vdo基本操作：参考man vdo 全文查找/example

```
–vdo create --name=VDO卷名称 --device=设备路径 --vdoLogicalSize=逻辑大小

–vdo list

–vdo status -n VDO卷名称

–vdo remove -n VDO卷名称

–vdostats [--human-readable] [/dev/mapper/VDO卷名称]

```

•VDO卷的格式化加速（跳过去重分析）：

–mkfs.xfs –K  /dev/mapper/VDO卷名称

–mkfs.ext4 -E nodiscard /dev/mapper/VDO卷名称

 

前提制作VDO需要2G以上的内存

```shell
[root@nb ~]# vdo create --name=vdo0 --device=/dev/sdc --vdoLogicalSize=200G

[root@nb ~]# mkfs.xfs -K /dev/mapper/vdo0 

[root@nb ~]# mkdir /nsd01

[root@nb ~]# mount /dev/mapper/vdo0 /nsd01

[root@nb ~]# df -h

[root@nb ~]# vdostats --hum /dev/mapper/vdo0 #查看vdo设备详细信息

 
[root@svr7 ~]# vim /etc/fstab 

/dev/mapper/vdo0  /nsd01 xfs defaults,_netdev 0 0 
```



### 课后习题：

#### 案例1：复制、粘贴、移动

 以root用户新建/exam/目录，在此目录下新建king.txt文件，并进一步完成下列操作

 1）将“I Love hehe”写入到文件king.txt 

```shell
[root@localhost ~]# mkdir /exam
[root@localhost ~]# echo "I Love hehe" > /exam/king.txt
[root@localhost ~]# cat /exam/king.txt
```

 2）将king.txt重命名为my.txt

```
[root@localhost ~]# move /exam/king.txt  /exam/my.txt
```

 3）将/etc/passwd、/boot、/etc/group同时拷贝到/exam/目录下

```
[root@localhost ~]# cp -r /etc/passwd /boot /etc/group /exam
```

 4）将ifconfig命令的前两行内容，追加写入king.txt

```
[root@localhost ~]# ifconfig | head -2
[root@localhost ~]# ifconfig | head -2 >> /exam/king.txt
```

 5）将主机名永久配置文件(/etc/hostname)，拷贝到/exam/目录下

```
[root@localhost ~]# cp /etc/hostname /exam/
[root@localhost ~]# ls /exam
```

 6）将存放组基本信息的配置文件（/etc/group），拷贝到/exam/目录下

```
[root@localhost ~]# cp /etc/group /exam
[root@localhost ~]# ls /exam
```

 7）将开机自动挂载配置文件，拷贝到/exam/目录下

```
[root@localhost ~]# cp /etc/fstab/ /exam
[root@localhost ~]# ls /exam
```



#### 案例2:虚拟机上操作,采用GPT分区模式，利用parted规划分区

 添加一块30G的硬盘并规划分区：

  划分2个2G的主分区；1个5G的主分区;

```shell
[root@localhost ~]# parted /dev/sdb
(parted) mktable  gpt             #指定分区模式为GPT 
(parted) print
(parted) mkpart                       #划分新的分区
分区名称？  []? xixi                 #分区的名字，随意起名
文件系统类型？  [ext2]? xfs     #文件系统类型，随意写
起始点？ 0                                   #起始点
结束点？ 2G                                   #结束点
警告: The resulting partition is not properly aligned for best performance.
忽略/Ignore/放弃/Cancel? Ignore    #选择忽略，给出存放分区表信息的空间
(parted) print 
(parted) unit  GB                             #采用GB作为单位
(parted) print
(parted) mkpart 
分区名称？  []? xixi      
文件系统类型？  [ext2]? xfs
起始点？ 2G
结束点？ 4G
(parted) print
(parted) mkpart 
分区名称？  []? hehe         
文件系统类型？  [ext2]? xfs
起始点？ 4G
结束点？ 9G
(parted) print                              
(parted) quit 

[root@localhost ~]# lsblk
```



#### 案例3:虚拟机上操作,交换分区使用

1、案例2中新添加30G硬盘的第一个主分区

– 格式化成交换文件系统，实现该分区开机自动启用

```
[root@localhost ~]# mkswap /dev/sdb1
[root@localhost ~]# blkid /dev/sdb1
[root@localhost ~]# swapon
[root@localhost ~]# swapon /dev/sdb1
[root@localhost ~]# swapon
[root@localhost ~]# free -m
```

2、案例2中新添加30G硬盘的第二个主分区

– 格式化成交换文件系统，实现该分区开机自动启用

```
[root@localhost ~]# mkswap /dev/sdb2
[root@localhost ~]# blkid /dev/sdb2
[root@localhost ~]# swapon
[root@localhost ~]# swapon /dev/sdb2
[root@localhost ~]# swapon
[root@localhost ~]# free -m
```



#### 案例4:虚拟机上操作,文件扩展Swap空间

1. 使用dd命令创建一个大小为2048MB的交换文件，放在/opt/swap.db

```
[root@localhost ~]# dd if=/dev/zero of=/opt/swap.db bs=1M count=2048
[root@localhost ~]# ls -lh /opt/swap.db
```

2. 将swap.db文件格式化成swap文件系统

```
 [root@localhost ~]# mkswap -f /opt/swap.db
 [root@localhost ~]# blkid /opt/swap.db
```

3. 启用swap.db文件，查看swap空间组成

```
 [root@localhost ~]# swapon
 [root@localhost ~]# swapon /opt/swap.db
 [root@localhost ~]# swapon    #查看组成交换空间的成员信息
 [root@localhost ~]# free-m  #查看交换空间总共的大小
```

4. 停用swap.db文件，查看swap空间组成

```
 [root@localhost ~]# swapoff /opt/swap/db
 [root@localhost ~]# swapon
 [root@localhost ~]# free-m  #查看交换空间总共的大小
```



#### 案例5:虚拟机操作，构建 LVM 存储

– 新建一个名为 systemvg 的卷组 

```shell
[root@localhost ~]# parted /dev/sdb
(parted) mkpart 
分区名称？  []? xixi                                                   
文件系统类型？  [ext2]? xfs                                             
起始点？ 8G                                                             
结束点？ 100%
(parted) print
(parted) quit

[root@localhost ~]# vgcreate systemvg /dev/sdb4
[root@localhost ~]# vgs
```

– 在此卷组中创建一个名为 vo 的逻辑卷，大小为8G 

```
[root@localhost ~]# lvcreate -L 8G -n vo systemvg
[root@localhost ~]# lvs
```

– 将逻辑卷 vo 格式化为 xfs 文件系统 

```
[root@localhost ~]# mkfs.xfs /dev/systemvg/vo
[root@localhost ~]# blkid /dev/systemvg/vo  #查看文件系统类型
```

– 将逻辑卷 vo 挂载到 /vo 目录，并在此目录下建立一个测试文件 votest.txt，内容为“I AM KING.” 

```
[root@localhost ~]# mkdir /vo
[root@localhost ~]# echo "I AM KING" > /vo/votest.txt
[root@localhost ~]# mount /dev/systemvg/vo /vo
```

– 实现逻辑卷vo开机自动挂载到/vo

```shell
[root@localhost ~]# vim /etc/fstab
 /dev/systemvg/vo /vo xfs defaults 0 0
[root@localhost ~]# umount /vo
[root@localhost ~]# mount -a
[root@localhost ~]# df -h   #查看正在挂载的设备信息
```



#### 案例6:虚拟机操作，构建 LVM 存储(修改PE大小)

– 新的逻辑卷命名为 database，其大小为50个PE的大小，属于 datastore 卷组 

– 在 datastore 卷组中其PE的大小为1M

```
[root@localhost ~]# vgchange -s 1M datestore /dev/sdb3
[root@localhost ~]# vgs
[root@localhost ~]# vgdisplay datastore
[root@localhost ~]# 
[root@localhost ~]# lvcreate -l 50 -n database  datastore
[root@localhost ~]# lvs

```

– 使用 EXT4 文件系统对逻辑卷 database 格式化，此逻辑卷应该在开机时自动挂载到 /mnt/database 目录

```
[root@localhost ~]# mkfs.ext4 /dev/datastore/database
[root@localhost ~]# blkid /dev/datastore/database
[root@localhost ~]# mkdir /mnt/database
[root@localhost ~]# vim /etc/fstab
/dev/datastore/database /mnt/database ext4 defaults 0 0
[root@localhost ~]# mount -a
[root@localhost ~]# df -h
```



#### 案例7:虚拟机 server0操作，扩展逻辑卷

– 将/dev/systemvg/vo逻辑卷的大小扩展到20G

```
[root@localhost ~]# vgs
[root@localhost ~]# lvextend -L 20G /dev/systemvg/vo
[root@localhost ~]# vgs
[root@localhost ~]# lvs
[root@localhost ~]# df -h | tail -1
[root@localhost ~]# xfs_growfs /dev/systemvg/vo
[root@localhost ~]# df -h | tail -1
```



#### 案例8:进程管理

 1.查看当前系统中整个进程树信息

```
[root@localhost ~]# pstree -ap
```

 2.利用pstree查看lisi开启的进程

```
[root@localhost ~]# useradd lisi
[root@localhost ~]# pstree -ap lisi
```

 3.显示当前系统正在运行的所有进程信息

```
[root@localhost ~]# ps aux
```

 4.检索当前系统中进程，进程名包含cron的PID是多少？

```
[root@localhost ~]# pgrep cron
```

 5.开启5个xeyes放入后台运行

```
[root@localhost ~]# ls /nsd50
[root@localhost ~]# mount /dev/cdrom /nsd50
[root@localhost ~]# yum -y install xorg-x11-apps
[root@localhost ~]# xeyes &
[root@localhost ~]# xeyes &
[root@localhost ~]# xeyes &
[root@localhost ~]# xeyes &
[root@localhost ~]# xeyes &
```

 6.杀死所有xeyes进程

```
[root@localhost ~]# killall -9 xeyes
```

------



