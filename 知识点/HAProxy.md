# HAProxy

### 1. HAProxy工作模式有哪些？



```
mode http：客户端请求被深度分析后再发往服务器。

mode tcp：在客户端与服务器这间建立全双工会话，不检查第七层信息。

mode health：仅做健康状态检查，已经不建议使用。
```



### 2. HAProxy配置文件有哪些组成部分？



```
default：为后续的其他部分设置缺省参数，缺省参数可以被后续部分重置；

listen：调度服务器监听的IP和端口。

server：定义后端真实服务器的IP和端口，健康检查的策略。
```

