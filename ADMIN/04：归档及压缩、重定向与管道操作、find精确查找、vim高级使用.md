## 04：归档及压缩、重定向与管道操作、find精确查找、vim高级使用

![img](./04images/001.png)

### 归档及压缩

#### 归档和压缩

- #### 归档的含义

  **——将许多零散的文件整理为一个文件**

  **——文件总的大小基本不变**

- #### 压缩的含义

  **——按某种算法减小文件所占用空间的大小**

  **——恢复时对应的逆向算法解压**

  > **常见的压缩格式及命令工具：**
  >
  > **.gz --> gzip、gunzip**
  >
  > **.bz2 --> bzip2、bunzip2**
  >
  > **.xz --> xz、unxz**



#### tar工具的常用选项

- **tar**  **集成备份工具**

  - **—c :创建归档**

  - **—x :释放归档**

  - **—f: 指定归档文件名称**

  - **—z 、—j、—J :调用 .gz、.bz2、.xz 格式的工具进行处理**

  - **—t: 显示归档中的文件清单**

  - **—C : 指定释放路径**

    

### 备份与恢复操作

#### 制作tar备份包

- **使用 tar -c ... 命令**

  > **-tar -zcf  备份文件.tar.gz 被备份的文档......**
  >
  > **-tar  -jcf   备份文件.tar.bz2被备份的文档......**
  >
  > **-tar  -Jcf   备份文件.tar.xz备份的文档......**

  ```shell
  [root@svr7~]# tar -jcf /root/home.tar.bz2  /home
  [root@svr7~]# ls -lh /root/home.tar.bz2
  -rw-r--r--, 1 root root 1.9k Nov 11 16:41 /root/home.tar.bz2
  ```

  

#### 查看tar备份包内容

- **使用 tar -t  ... 命令**

  **-tar  - tf     备份文件,tar.gz**

  ```shell
  [root@svr7~]# tar -tf /root/home.tar.bz2
  home/
  home/student/
  home/student/.bash_logout
  home/student/.bash_profile
  ....
  ```

  

#### 从tar备份包恢复文档

- **使用 tar -x ... 命令**

  **-tar  -xf  备份文件.tar.gz  [-C  目标文件夹]**

  ```shell
  [root@svr7~]# rm -rf /home
  [root@svr7~]# tar -xf  /root/home.tar.bz2  -C /
  [root@svr7~]# ls -ld /home/
  drwxr-xr-x.5 root root 45 Nov 11 16:09 /home
  
  
  [root@svr7~]# tar -xf /root/home.tar.bz2 -C /tmp/
  tar: Removing leading '/' from member names
  
  [root@svr7~]# ls -ld /tmp/home/
  drwxr-xr-x.5 root root 45 Nov 11.16:09 /tmp/home/
  ```

  ![img](./04images/002.png)



### **重定向**

#### 重定向输出

- **将屏幕显示信息保存到文件**

  **——覆盖重定向：cmd > file**

  **——追加重定向：cmd >> file**

  ```shell
  [root@svr7~]# hostname > /opt/hn.txt
  [root@svr7~]# cat /opt/hn.txt
  server0.example.com
  [root@svr7~]# hostname >> /opt/hn.txt
  [root@svr7~]# cat /opt/hn.txt
  server0.example.com
  server0.example.com
  ```

### 管道操作

#### 管道传递

- **使用 | 管道 操作**

  **——将前一条命令的标准输出交给后一条命令处理**

  **——cmd1 | cmd2    [  | cmd3] ......**

  ```shell
  [root@svr7~]# ls --help | less
  ..
  [root@svr7~]# ifconfig | head -2
  ..
  [root@svr7~]# head -12 /etc/passwd | tail -5
  
  ```

###  **grep过滤包含指定字符串的行**

> **-v:取反过滤**
>
> **^字符串:以字符串开头**
>
> **字符串$:以字符串开头结尾**
>
> **^$:匹配空行**

```shell
]# cat    /etc/default/useradd
]# grep    ^$   /etc/default/useradd    
]# grep  -v  ^$   /etc/default/useradd     #不要空行
 在Linux系统中，大多数配置文件以#开头的行，为注释行

]# grep  -v  ^#   /etc/default/useradd
显示有效配置(去除空行与注释行)
]# grep  -v  ^#   /etc/default/useradd    |    grep   -v  ^$
]# grep  -v  ^#  /etc/default/useradd   |    grep   -v   ^$  >  /opt/nsd03.txt
]# cat   /opt/nsd03.txt

]# grep  -v  ^#   /etc/login.defs
显示有效配置(去除空行与注释行)
]# grep  -v  ^#   /etc/login.defs    |    grep   -v  ^$
]# grep  -v  ^#  /etc/login.defs   |    grep   -v   ^$  >  /opt/nsd04.txt
]# cat   /opt/nsd04.txt

```



![img](./04images/003.png)



### find基本使用

#### 常用条件

- **根据预设的条件递归查找对应的文件**

  **——find [目录]  [条件1]**

  **——常用条件表示：**

  -  **-type 类型（f、d、l)**
  -  **-name "文档名称“**
  -  **-size + | - 文件大小 （K 、M、G）**
  -  **-user   用户名**
  -  **-mtime 修改时间**

  

### find高级使用



#### 处理查找的内容

- **操作方法：**

  - **find  [范围]   [条件]  -exec  处理命令  {}  \;**\ 

  - **根据条件查找并处理结果**

    ```shell
    [root@svr7~]# find /boot -size +10M
    /boot/initramfs-2.6.32-431.e16.x86_64.img
    
    
    [root@svr7~]#  find /boot -size +10M -exec ls -lh {} \;
    -rw-------. 1 root root 17M 10月 21 15：10 /boot/initramfs-2.6.32-431.e16.x86_64.img
    ```

    

![img](./04images/004.png)



### 命令模式操作

#### 光标跳转

![img](./04images/005.jpg)



#### 复制/粘贴/删除

![img](./04images/006.jpg)

#### 查找/撤销/保存

![img](./04images/007.jpg)

### 末行模式操作

#### 保存、退出、文件操作

![img](./04images/008.jpg)



#### 字符串替换

![img](./04images/009.jpg)



#### 开关参数的控制

![img](./04images/010.jpg)

###  **常见问题：交换文件产生   名称为:\*.swp**

> ```shell
> E325: 注意
> 
> 发现交换文件 "/opt/.c.txt.swp"
> 
> ​      所有者: root  日期: Fri Sep 4 17:22:51 2020
> 
> ​      文件名: /opt/c.txt
> 
> ​      修改过: 是
> 
> ​      用户名: root   主机名: localhost.localdomain
> 
> ​      进程 ID: 17881
> 
> 正在打开文件 "/opt/c.txt"
> 
> **……..**
> 
> [root@localhost ~]# rm  -rf   /opt/.c.txt.swp  #手动删除交换文件
> 
> [root@localhost ~]# vim  /opt/c.txt
> ```

