## 05：SELinux、系统故障修复、防火墙策略管理、服务管理



### **一、** **添加一张新的网卡设备**

####  **关闭虚拟机**

[root@svr7 ~]# poweroff

####  **添加网卡设备**

![image-20200914183311583](./04image/001.png)

![image-20200914183317032](./04image/002.png)

![image-20200914183321571](./04image/003.png)

![image-20200914183327794](./04image/004.png)

```shell
[root@svr7 ~]# ifconfig  | less

eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500

​    inet 192.168.4.7  netmask 255.255.255.0  broadcast 192.168.4.255

 

eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500

​    inet 192.168.81.130  netmask 255.255.255.0  broadcast 192.168.81.255
```

 

####  **为eth1配置IP地址**

```shell
[root@svr7 ~]# nmcli connection show

[root@svr7 ~]# nmcli connection delete  有线连接\ 1 

成功删除连接 '有线连接 1'（1c99da34-de09-3f0d-9f7c-40c842036e40）。

[root@svr7 ~]# nmcli connection add type ethernet     con-name eth1  ifname eth1

连接“eth1”(4ab21424-4281-427d-b5fd-87fd0644b00a) 已成功添加。

[root@svr7 ~]# nmcli  connection  show

 
[root@svr7 ~]# nmcli connection modify eth1 

ipv4.method manual ipv4.addresses 192.168.1.1/24 connection.autoconnect  yes

[root@svr7 ~]# nmcli connection up eth1

连接已成功激活（D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/6）

[root@svr7 ~]# ifconfig  eth1  |  head -2

eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500

​    inet 192.168.1.1 netmask 255.255.255.0 broadcast 192.168.1.255
```

### 二、**网络工具**

#### **利用ip命令，查看IP地址**

```shell
[root@svr7 ~]# ip  address  show

[root@svr7 ~]# ip  a  s    #支持命令的缩写
```

####  **利用ip命令，临时设置IP地址**

```shell
[root@svr7 ~]# ip address add  192.168.8.1/24  dev eth0   [root@svr7 ~]# ip   a   s   |  less

2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000

  link/ether 00:0c:29:a1:a5:d3 brd ff:ff:ff:ff:ff:ff

  inet 192.168.4.7/24 brd 192.168.4.255 scope global noprefixroute eth0

​    valid_lft forever preferred_lft forever

  inet 192.168.8.1/24 scope global eth0

​    valid_lft forever preferred_lft forever
```

 

#### **ping测试网络通信**

 **-c:指定ping包的个数**

```shell
[root@svr7 ~]# ping  -c  2  192.168.4.7

[root@svr7 ~]# ping  -c  4  192.168.4.7
```

### 三、 源码编译安装

RPM包：rpm -ivh   yum  install

源码包----开发工具---->可以执行的程序----->运行安装

 

#### • 主要优点

– 获得软件的最新版，及时修复bug

– 软件功能可按需选择/定制，有更多软件可供选择

– 源码包适用各种平台

– ……

 

#### • 传递源码包到虚拟机B

![image-20200914183953794](./04image/005.png)

```shell
[root@pc207 /]# ls  /root/      

anaconda-ks.cfg    公共  文档

fstab         模板  下载

initial-setup-ks.cfg  视频  音乐

tools.tar.gz     图片  桌面
```

##### 步骤1：安装开发工具

```
/]# yum  -y  install  gcc

/]# yum  -y  install  make
```

##### 步骤2：tar解包，释放源代码至指定目录

```shell
]# tar -xf /root/tools.tar.gz  -C  /usr/local/

]# ls /usr/local/

]# ls /usr/local/tools/

]# tar -xf /usr/local/tools/inotify-tools-3.13.tar.gz  -C  /usr/local/

]# ls /usr/local/

]# cd /usr/local/inotify-tools-3.13/

]# ls
```

 

##### 步骤3：./configure 配置，指定安装目录/功能模块等选项

作用1：检测系统是否安装gcc  作用2：指定安装位置

```
]# ./configure     --prefix=/opt/myrpm     #当前运行脚本指定安装位置
```

##### 步骤4：make 编译，生成可执行的二进制程序文件

```
]# make
```

##### 步骤5：make install 安装，将编译好的文件复制到安装目录

```
]# make  install

]# ls  /opt/

]# ls  /opt/myrpm/

]# ls  /opt/myrpm/bin/    #查看安装好的程序
```

### 四、 自定义Yum仓库

将自己准备的众多软件包，制作成一个软件仓库

Yum仓库：1.众多的软件包  2.仓库数据文件

虚拟机A：

#### 1. .将真机tools.tar.gz传递虚拟机A

![image-20200914195730467](./04image/006.png)

#### 2.进行tar解包

```shell
]# tar  -tf  /root/tools.tar.gz    #查看包里内容

]# tar  -xf  /root/tools.tar.gz  -C   /

]# ls  /

]# ls  /tools
```



#### 3.生成仓库数据文件

```
[root@svr7 ~]# createrepo    /tools/other/
[root@svr7 ~]# ls   /tools/other/
```

#### 4.书写客户端配置文件

```shell
[root@svr7 ~]# vim   /etc/yum.repos.d/mydvd.repo
[centos]
name=2008
baseurl=file:///mydvd
enabled=1
gpgcheck=0
[myrpm]
name=2008
baseurl=file:///tools/other          
enabled=1
gpgcheck=0
[root@svr7 ~]# yum -y install sl
[root@svr7 ~]# yum -y install cmatrix
[root@pc207 /]# cmatrix

```

### 五、 日志简介

#### • 系统和程序的“日记本”

记录系统、程序运行中发生的各种事件

通过查看日志，了解及排除故障

信息安全控制的“依据”

#### • 由系统服务rsyslog统一记录/管理

– 日志消息采用文本格式

– 主要记录事件发生的时间、主机、进程、内容

#### • 常见的日志文件

| ***\*日志文件\**** | ***\*主要用途\****               |
| ------------------ | -------------------------------- |
| /var/log/messages  | 记录内核消息、各种服务的公共消息 |
| /var/log/dmesg     | 记录系统启动过程的各种消息       |
| /var/log/cron      | 记录与cron计划任务相关的消息     |
| /var/log/maillog   | 记录邮件收发相关的消息           |
| /var/log/secure    | 记录与访问限制相关的安全消息     |

 

#### • 日志分析

##### • 通用分析工具

– tail、tailf、less、grep等文本浏览/检索命令

– awk、sed等格式化过滤工具

tailf：实时跟踪日志消息

##### • users、who、w 命令

– 查看已登录的用户信息，详细度不同

##### • last、lastb 命令

– 查看最近登录成功/失败的用户信息

```shell
[root@pc207 /]# users

[root@pc207 /]# who

[root@pc207 /]# w

pts（图形命令行终端）

[root@pc207 /]#last -2  #最近登录成功的2条记录

[root@pc207 /]#lastb -2  #最近登录失败的2条记录
```

 

##### • Linux内核定义的事件紧急程度

– 分为 0~7 共8种优先级别

– 其数值越小，表示对应事件越紧急/重要

0  EMERG（紧急）     会导致主机系统不可用的情况

1  ALERT（警告）      必须马上采取措施解决的问题

2  CRIT（严重）	      比较严重的情况

3  ERR（错误）	      运行出现错误

4  WARNING（提醒）   可能会影响系统功能的事件

5  NOTICE（注意）     不会影响系统但值得注意

6  INFO（信息）	      一般信息

7  DEBUG（调试）      程序或系统调试信息等

 

##### • 提取由 systemd-journal 服务搜集的日志

– 主要包括内核/系统日志、服务日志

###### • 常见用法

– journalctl  |  grep  关键词

– journalctl  -u  服务名  [-p  优先级]

– journalctl  -n  消息条数



### 六、 SELinux安全机制

#### • Security-Enhanced Linux

– 美国NSA国家安全局主导开发，一套增强Linux系统安全的强制访问控制体系

– 集成到Linux内核（2.6及以上）中运行

– RHEL7基于SELinux体系针对用户、进程、目录和文件提供了预设的保护策略，以及管理工具

 

#### • SELinux的运行模式

– enforcing（强制）、permissive（宽松）

– disabled（彻底禁用）

– 任何模式变成disabled，都需要重启系统

#### • 切换运行模式

– 临时切换：setenforce  1|0

– 固定配置：/etc/selinux/config 文件

![img](./04image/007.png) 

 

虚拟机A：

```shell
[root@svr7 /]# getenforce   #查看当前系统SELinux模式

Enforcing

[root@svr7 /]# setenforce  0  #临时设置当前SELinux模式

[root@svr7 /]# getenforce

Permissive

[root@svr7 /]# vim /etc/selinux/config

SELINUX=permissive
```

虚拟机B：同上

 

### 七、 系统故障修复

#### **etc\fstab\书写错误：**

##### 1.输入root的密码（输入的内容不显示）

![img](./04image/008.png) 

##### 2.继续编辑/etc/fstab内容进行修复

![img](./04image/009.png) 

 

####  **遗忘root的密码（破解系统root的密码）**

前提：必须是服务器的管理者，涉及重启服务器

破解密码思路

##### 1)**重启系统**,进入 恢复模式

```
[root@A ~]# reboot 
```

开启虚拟机A，在此界面按e键

![img](./04image/010.png) 

找到linux16该行，在该行的最后，空格输入 rd.break  console=tty0

![img](./04image/011.png) 

![img](./04image/012.png) 

按 **ctrl + x** 启动，会看到switch_root#

 

##### 2)以可写方式重新挂载 /,并切换到此环境    

```
switch_root# mount -o remount,rw  /sysroot #让根目录下所有数据，可以读也可以写入
sh-4.4# chroot /sysroot  #切换环境，切换到硬盘操作系统的环境
```

 

##### 3)重新设置root的密码 

```
sh-4.4# echo 1  |  passwd  --stdin root
```

##### 4)如果SELinux是强制模式，才需要重设SELinux策略（其他模式不需要做此操作）

```
sh-4.4# vim  /etc/selinux/config #查看SELinux开机的运行模式

sh-4.4# touch /.autorelabel  #SELinux是强制模式，让SELinux失忆
```

##### 5)强制重启系统完成修复

```
sh-4.4# reboot -f 
```

### 八、 构建基本的Web服务

 Web服务：提供网页内容的服务

 Web服务器：提供网页内容的服务机器

 实现web服务软件：httpd  nginx  tomcat

 http协议:超文本传输协议

 

虚拟机A：

#### 1.安装httpd软件

```
[root@svr7 ~]# yum  -y  install  httpd
```

#### 2.写一个网页文件

```
[root@svr7 ~]# vim   /var/www/html/index.html

hahaxixihehelele哈哈嘻嘻         
```

#### 3.运行httpd程序

```shell
[root@svr7 ~]# > /etc/resolv.conf   #清空文件内容，加快启用

[root@svr7 ~]# /usr/sbin/httpd    #绝对路径方式运行程序

[root@svr7 ~]# pgrep -l httpd     #查看httpd进程

[root@svr7 ~]# killall httpd       #停下httpd进程

[root@svr7 ~]# pgrep -l httpd

[root@svr7 ~]# /usr/sbin/httpd    #绝对路径方式运行程序

[root@svr7 ~]# firefox http://192.168.4.7  #图形浏览器

[root@svr7 ~]# curl  http://192.168.4.7   #命令行浏览器

hahaxixihehelele哈哈嘻嘻
```

### 九、 防火墙管理

 作用：隔离，严格过滤入站，放行出站

 硬件防火墙：保护一个网络中所有机器

 软件防火墙：保护本机

 

> • 管理工具：firewall-cmd(命令)、firewall-config(图形工具)
>
> • 根据所在的网络场所区分，预设保护规则集
>
> • public：仅允许访问本机的ssh、dhcp、ping服务
>
> • trusted：允许任何访问
>
> • block：拒绝任何来访请求，有明确的回应
>
> • drop：丢弃任何来访的数据包，没有明确的回应

 

#### • 防火墙判定的规则：

#####  1.查看请求中源IP地址，查看自己所有区域，哪个区域有该源IP地址规则，则进入哪个区域

#####  2.进入默认区域（默认情况下为public）

 

#### **修改默认区域**

虚拟机A：

```shell
[root@svr7 ~]# rpm -q  firewalld  #防火墙软件

firewalld-0.4.4.4-14.el7.noarch
```

虚拟机A        

```
]# firewall-cmd  --get-default-zone   #查看默认区域
```

虚拟机B

```
[root@pc207 /]#  ping  -c  2  192.168.4.7   #成功

[root@pc207 /]#  curl  http://192.168.4.7  #失败
```

 

虚拟机A

```
]# firewall-cmd  --set-default-zone=trusted   #修改默认区域
```

虚拟机B

```
[root@pc207 /]#  ping -c 2 192.168.4.7   #成功

[root@pc207 /]#  curl  http://192.168.4.7  #成功
```



####  **区域中添加允许的协议**

虚拟机A：

```
]# firewall-cmd  --set-default-zone=public  #修改默认区域

]# firewall-cmd  --get-default-zone   #查看默认区域

]# firewall-cmd  --zone=public  --list-all  #查看区域规则

]# firewall-cmd  --zone=public  --add-service=http  #添加协议

]# firewall-cmd  --zone=public  --list-all   #查看区域规则
```

 

虚拟机B

```
[root@pc207 /]#  curl http://192.168.4.7   #访问成功

hahaxixihehelele哈哈嘻嘻

[root@pc207 /]#
```

 

 

####  **永久规则**

**–** **永久（--permanent）**

**–** **默认区域的修改，默认就是永久的**

**虚拟机A：**

```shell
]# firewall-cmd  --reload     #重新加载防火墙永久的配置

]# firewall-cmd  --zone=public  --list-all   #查看区域规则

]# firewall-cmd  --permanent --zone=public  --add-service=http     #永久规则

 

]# firewall-cmd  --zone=public  --list-all  #查看区域规则 

]# firewall-cmd  --reload   #重新加载防火墙永久的配置 

]# firewall-cmd  --zone=public  --list-all   #查看区域规则 
```

 

####  **单独拒绝192.168.4.207所有访问，允许其他机器**

##### 防火墙判定的规则：

 1.查看请求中源IP地址，查看自己所有区域，哪个区域有该源IP地址规则，则进入哪个区域

 2.进入默认区域（默认情况下为public）

虚拟机A

.添加源IP地址规则

```shell
]# firewall-cmd --zone=block   --add-source=192.168.4.207

]# firewall-cmd --get-default-zone  #查看默认区域

public
```

虚拟机B

```
]# curl http://192.168.4.7  #访问失败
```



### 课后习题：

#### 案例1：设置SELinux运行模式

为虚拟机A、虚拟机B 配置SELinux 

 1）确保 SELinux 处于宽松模式（permissive） 

 2）在每次重新开机后，此设置必须仍然有效

```shell
虚拟机A
[root@svr7 ~]# setenforce  0  #当前关闭SELinux
[root@svr7 ~]# getenforce     #查看当前SELinux运行模式
Permissive
[root@svr7 ~]# vim  /etc/selinux/config  
SELINUX=permissive
虚拟机B同上
```



#### 案例2：实现虚拟机A 的Web服务

 1）利用httpd软件搭建Web服务，页面显示内容为 小蝌蚪找妈妈 

```shell
[root@svr7 ~]# yum -y install httpd
[root@svr7 ~]# vim /var/www/html/index.html
小蝌蚪找妈妈
[root@svr7 ~]# systemctl  restart  httpd
```



#### 案例3：实现虚拟机A 的防火墙配置

 1）修改虚拟机A防火墙配置，明确拒绝所有客户端访问(默认区域修改block)

```
[root@svr7 ~]# firewall-cmd --set-default-zone=block
```

 2）在虚拟机B上,测试能否访问虚拟机A 的Web服务

```
[root@pc207 ~]# curl http://192.168.4.7
```

 3）在虚拟机B上,测试能否 ping通 虚拟机A

```
[root@pc207 ~]# ping -c 2 192.168.4.7
```



#### 案例4：实现虚拟机A 的防火墙配置

 1）修改虚拟机A防火墙配置，将默认区域修改为public

```
[root@svr7 ~]# firewall-cmd --get-default-zone  #查看默认区域
[root@svr7 ~]# firewall-cmd --set-default-zone=public  #修改默认区域
```

 2）在虚拟机B上,测试能否访问虚拟机A 的Web服务

```
[root@pc207 ~]# curl http://192.168.4.7
```

 3）在虚拟机B上,测试能否 ping通 虚拟机A

```
[root@svr7 ~]# ping -c 2 192.168.4.7
```



#### 案例5：实现虚拟机A的防火墙配置

 1）修改虚拟机A防火墙配置，将默认区域修改为public

```
[root@svr7 ~]# firewall-cmd --get-default-zone   #查看默认区域
[root@svr7 ~]# firewall-cmd --set-default-zone=public  #修改默认区域
```

 2）修改虚拟机A永久的防火墙配置，在public区域中添加http协议

```
[root@svr7 ~]# firewall-cmd --permanent --zone=public --add-service=http
[root@svr7 ~]# firewall-cmd --reload  #重新加载防火墙所有永久配置
[root@svr7 ~]# firewall-cmd  --zone=public  --list-all
```

 3）在虚拟机B上,测试能否访问虚拟机A 的Web服务

```
[root@pc207 ~]# curl http://192.168.4.7
```



