## 03：case语句、函数及中断控制、字符串处理



\------------------------------------------------------------------------------------------

 

### **case分支**

**功能类似if，不如if强大，语句比较精简**

基本语法格式：

```shell
case 调用变量名 in			//如果调用的变量内容与下面某个模式一致，就执行模式下面的指令

模式1)				//这里的模式可以有很多

	执行指令;;		//指令需要用双分号结尾，如果一个模式有多个指令，那只需在该模式的最后一条指令后加双分号即可

模式2)

	执行指令;;

…

*)			//如果上述模式都没有被匹配，那就匹配这个

	执行指令

esac
```

\------------------------------------------------

**测试case分支的实际应用**

```shell
#!/bin/bash

case $1 in		//使用执行脚本后的第1个位置变量作为匹配对象

t|T|tt)			//如果$1是t或者T或者tt，都可以算匹配

  touch $2;;	 //此处是创建文件的命令，后面是第二个位置变量的参数

m|M|mm)

  mkdir $2;;

r)

  rm -rf $2;;

*)

  echo "请输入t|m|r"

esac
```

\-------------------------------------------------

### **部署nginx服务**

**httpd是之前使用过的网站服务，除此之外nginx也可以实现搭建网站的任务**

 

**1，将lnmp_soft.tar.gz软件包从真实主机拖拽到虚拟机的管理员家目录，然后释放**

```shell
[root@svr7 ~]# tar -xf lnmp_soft.tar.gz  //释放到当前目录]() 

[root@svr7 ~]# cd lnmp_soft/   //然后到释放的目录中

[root@svr7 lnmp_soft]# cp nginx-1.17.6.tar.gz /opt   //将nginx拷贝到opt下
```

 

**2，安装nginx**

**由于nginx是源码包，所以需要源码编译安装**

**编写部署nginx服务的脚本思路：**

​	**1）安装依赖 gcc(编译工具)，pcre-devel，openssl-devel(后两个都是nginx所需依赖包)**

​	**2）释放nginx-1.17.6.tar.gz**

​	**3）进入nginx-1.17.6目录**

​	**4）configure   配置**

​	**5）make   编译**

​	**6）make install  安装**

 

**将上述过程编写成部署nginx服务的脚本**

```shell
#!/bin/bash

yum -y install gcc pcre-devel openssl-devel &> /dev/null   //安装依赖软件包

tar -xf nginx-1.17.6.tar.gz		//释放tar包

cd nginx-1.17.6   //进入nginx目录

./configure    //配置

make    //编译

make install   //安装
```

 

写完后保存退出，并执行脚本

```shell
Systemctl  stop  httpd   //之后关闭其他网站服务

/usr/local/nginx/sbin/nginx   //开启nginx服务

/usr/local/nginx/sbin/nginx -s stop   //关闭nginx服务

然后使用浏览器访问192.168.4.7可以看到欢迎界面则成功

提示：不要忘记关闭防火墙

systemctl stop firewalld
```

 

\--------------------------------------------------------------------------------------

### **编写脚本，使用case分支控制nginx服务**

```shell
#!/bin/bash

case $1 in		//使用执行脚本后的第1个位置变量作为匹配对象

s|start|kai)  //如果$1是s或start或kai ，那么就执行以下指令

  netstat -ntulp | grep -q nginx  //查询有没有开启nginx服务, -q是不输出查询结果

  [ $? -eq 0 ] && echo "nginx已经开启" && exit   //判断如果开了nginx就退出

  /usr/local/nginx/sbin/nginx;;   //如果没开nginx就开启

stop|guan)   //如果$1是stop或guan，那么就执行以下指令

  /usr/local/nginx/sbin/nginx -s stop;;   //关闭nginx

restart|cq)   //如果$1是restart或cq，那么就执行以下指令

  /usr/local/nginx/sbin/nginx -s stop  //关闭nginx

  /usr/local/nginx/sbin/nginx;;   //开启nginx，此处相当于重启nginx

status|cx)   //如果$1是status或cx，那么就执行以下指令

  netstat -ntulp | grep -q nginx  //查询有没有开启nginx服务, -q是不输出查询结果

  [ $? -eq 0 ] && echo "nginx正在运行中。。"  || echo "nginx未开启";;  //根据查询结果输出nginx正在运行或者nginx未开启的提示

*)

  echo "start|stop|restart"   //如果没有匹配任何模式就是喊出使用该脚本的提示，告诉使用者$1应该敲啥，而不能随意敲。

esac   //结尾，固定语法，不能少
```

 

**netstat命令可以查看系统中启动的端口信息，该命令常用选项如下：**

-n以数字格式显示端口号

-t显示TCP连接的端口

-u显示UDP连接的端口

-l显示服务正在监听的端口信息，如httpd启动后，会一直监听80端口

-p显示监听端口的服务名称是什么（也就是程序名称）



\----------------------------------------------------------------------------------------------

### **如果想在nginx中修改输出文字的颜色，可以使用下列方式**

echo -e "\033[32mABCD\033[0m"   //-e选项可以激活后面特殊字符的作用，相当于使用echo的扩展功能，\033[32m代表设置颜色为绿色，ABCD是输入内容，\033[0m代表还原颜色。

### **函数，可以将公共的语句块使用一个函数名来定义，方便后期反复调用，达到精简脚本，增加可读性的目的**

```shell
#!/bin/bash

a() {      //定义函数

echo abc

echo xyz

}

a  //调用函数，相当于执行上述两个echo任务

a  //可以反复调用
```

\---------------------------------------------------------------------------------

```shell
#!/bin/bash

a() {         //定义函数

echo -e "\033[$1m$2\033[0m"   //输出不同颜色的文本内容，并加入位置变量

}

a 31 ABCD   //调用时函数后面可以写位置变量的内容，31就是$1  ABCD是$2

a 32 XUEY 
```

\---------------------------------------------------------------------------------

### **循环的控制：**

**通常，在执行循环任务中途如果想退出可以用exit指令，但该指令不但会退出循环，连同脚本也会一并退出，此时可以使用break与continue指令更精细的控制循环。**

 

```
exit   可以终止循环，但同时退出脚本，如果循环之后还有任务则无法执行**

break  终止循环，继续执行循环之后的任务**

continue  终止当前循环，继续执行下一次循环
```

 

**练习：**

编写脚本，可以为用户进行整数求和，如果输入0则退出并显示

之前求和结果

```shell
#!/bin/bash

x=0		//先定义了一个x，表示用户给的整数之和，一个都没给时就是0

while :

do

read -p "请输入一个整数求和，0是结束" n  //问用户要数字

[ -z $n ] && continue		//如果n是空值，就重新循环

[ $n -eq 0 ] && break	//如果n是0，就退出循环

let x+=n   //将x+n

done

echo "整数之和是$x"  //喊出结果
```

 

\------------------------------------------------------

**linux中很多地方都需要这样或着那样的去使用、管理、操作字符，多掌握在linux中字符的控制方法直接决定能否写好脚本与更好的控制linux系统。**

 

**1，** **字符串的截取**

```shell
x=abcdef

${变量名:截取的位置:截取的位数}

echo ${x:1:2}  //截取bc，位置是从0开始计算，所以要从1开始

才能截取第二个字符b

echo ${x:1:1}  //从第2个字符截取，截取1位

echo ${x:0:2}  //从第1个字符截取，截取2位

echo ${x::2}  //效果同上，如果从第1位开始截取的话，0可以省略不写
```

**编写脚本，可以获取随机的8位字符（将来可以作为为用户配置随机密码时使用）**

```shell
#!/bin/bash

x=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789   //定义变量

for i in {1..8}   //循环8次

do

n=$[RANDOM%62]  //得到0-61之间的随机数

a=${x:n:1}  //随机截取一个x中的字符存储到变量a中

pass=$pass$a  //将每次获取的随机字符存储到变量pass中

done

echo $pass  //循环完8次之后，喊出最终结果，就得到了1个8位的字符
```

\-------------------------------------------------------------------------

**请创建30个10位的密码，存放到一个文件中**

```shell
#!/bin/bash

x=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789

for j in {1..30}		//将得到10位随机字符的过程执行30次

do

  for i in {1..10}   //将得到1个随机字符的过程执行10次

  do

    n=$[RANDOM%62]  //得到0-61之间的随机数

    a=${x:n:1}   //随机截取一个x中的字符存储到变量a中

    pass=$pass$a   //将每次获取的随机字符存储到变量pass中

  done

echo $pass >> /opt/pass.txt  //将每次得到的10位字符追加保存到pass.txt中

  pass=  //清空pass变量，避免下次叠加

done
```

 



#### **练习：**

1，简述Linux服务脚本中的case分支结构。

```
case 变量名 in 
模式1) 
执行指令 ;; 
模式2)
执行指令 ;; 
*)
执行指令
esac
```

2，简述定义一个Shell函数的任意一种方法。

```
函数名() {
执行指令
}
```

3，简述Shell环境常见的中断及退出控制指令。

```shell
break：跳出当前所在的循环，执行循环之后的语句。
continue:跳过循环内余下的语句，执行下一次循环。
exit:退出脚本
```

4，使用 while 循环,统计 1+2+3+4...+100的结果。

提示:可以用一个独立的变量a存放求和的值。

```shell
#!/bin/bash
a=0
for i in {1..100}
do
let a+=i
done
echo $a
```

5，编写脚本,通过 3 个 read 命令读取用户输入的三个任意数字,脚本对输入的三个数字求和输出。

```shell
#!/bin/bash
read -p "请输入数字" num1
read -p "请输入数字" num2
read -p "请输入数字" num3
echo $[num1+num2+num3]
```

6，判断当前系统启动的进程数量,如果进程数量超过 100 个,则发送邮件给 root 报警。

```shell
#!/bin/bash
num=`ps aux | wc -l`
[ $num -gt 100 ] && echo "进程超过100啦～" | mail -s Warning root
```

7， 编写脚本,测试当前用户对/etc/passwd 文件是否具有读、写、执行的权限,让脚本执行结果类似下面的效果。

```shell
#!/bin/bash
file=/etc/passwd
[ -r /etc/passwd ] && echo "当前用户对$file 有读权限" || echo "当前用户对$file 没有读权限"
[ -w /etc/passwd ] && echo "当前用户对$file 有写权限" || echo "当前用户对$file 没有写权限"
[ -x /etc/passwd ] && echo "当前用户对$file 有执行权限" || echo "当前用户对$file 没有执行权限"
```



