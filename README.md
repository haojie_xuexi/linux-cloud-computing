# linux-cloud-computing



------

云计算（Linux/Python）笔记



**.md**文件推荐使用[Typora](https://typora.io/)这个软件打开查看



------



## 目录



------



### ADMIN

#### 01：云计算介绍、TCP/IP协议及配置

#### 02：Linux系统简介、安装Linux系统、Linux基本操作

#### 03：命令行基础、目录及文件管理、文本内容操作

#### 04：归档及压缩、重定向与管道操作、find精确查找、vim高级使用

#### 05：RPM软件包管理、Yum软件包仓库、命令补充

#### 06：用户管理、组账号管理、计划任务



------



### ENGINEER

#### 01：基本权限和归属、附加权限、ACL策略管理

#### 02：磁盘空间管理、交换空间

#### 03：逻辑卷管理、VDO、RAID磁盘阵列、进程管理

#### 04：配置Linux网络、源码编译安装。自定义yum仓库、日志管理

#### 05：SELinux、系统故障修复、防火墙策略管理、服务管理



------



### SERVICES

#### 01：KVM构建及管理、virsh控制工具、镜像管理、虚拟机快建技术

#### 02：Web基础应用、NFS服务基础、触发挂载

#### 03：DNS服务基础、特殊解析、DNS子域授权、DNS主从架构

#### 04：缓存DNS、Split分离解析、电子邮件通信、Web服务器项目实战

#### 05：批量装机环境、配置PXE引导、Kickstart自动应答、Cobbler装机平台

#### 06：rsync同步操作、inotify实时同步、数据库服务基础、管理表数据



------



### NETWORK

#### 01：计算机网络、网络通信参考模型、交换机命令行、交换机命令行配置、数据链路层解析

#### 02：VlAN技术及应用、TRUNK、网络层解析

#### 03：OSPF 、传输层、ACL

#### 04：NAT 、VRRP

#### 05：综合项目、网络升级

#### 

------



### SHELL

#### 01：Shell 概述、编写及执行脚本、Shell变量、数值运算

#### 02：条件测试、if选择结构、循环结构

#### 03：case语句、函数及中断控制、字符串处理

#### 04：正则表达式、sed基本用法、sed文本块处理

#### 05：sed高级应用、awk基本用法

#### 06：awk高级应用、综合案例



------



### OPERATION

#### 01：Nginx安装、用户认证、Nginx虚拟主机、HTTPS加密网站

#### 02：部署LNMP、Nginx+FastCGI、Nginx高级技术

#### 03：Nginx代理服务器、Nginx优化

#### 04：Session与Cookie、部署memcached、Session共享

#### 05：Tomcat服务器、Tomcat应用案例、Varnish代理服务器

#### 06：版本控制、Git基础、Cit进阶、RPM打包

#### 07：VPN服务器、systemd



------



### AUTOMATION

#### 01：ansible基础、Ansible ad-hoc

#### 02：sudo提权、Ansible配置、Ansible Playbook、Ansible进阶

#### 03：Ansible进阶、Ansible Vault、Ansible Roles、综合练习



------



### CLUSRER

#### 01：集群及LVS简介、LVS-NAT集群、LVS-DR集群

#### 02：Keepalived热备、Keepalived+LVS、HAProxy服务器

#### 03：Ceph概述、部署Ceph集群、Ceph块存储

#### 04：块存储应用案例、分布式文件系统、对象存储



------



### PROJECT1

#### 01：服务器硬件、部署LNMP动态网站

#### 02：网站架构演变、LNP+Mariadb数据库分离、Web服务器集群

#### 03：Keepalived高可用、部署Ceph分布式存储

#### 04：部署Git版本控制系统、优化web服务器



------



### SECURITY

#### 01：监控概述、Zabbix基础、Zabbix监控服

#### 02：Zabbix报警机制、Zabbix进阶操作、监控案例

#### 03：Linux基本防护、用户切换与提权、SSH访问控制、SElinux安全防护

#### 04：加密与解密、AIDE入侵检测系统、扫描与抓包

#### 05：系统审计、服务安全、Linux安全之打补丁

#### 06：iptables防火墙、filter表控制、扩展匹配、nat表典型应用



------



### RDBMS1

#### 01：数据库服务概述、构建MySQL服务、数据库基本管理、MySQL数据类型

#### 02：表结构、MySQL键值

#### 03：数据导入导出、管理表记录、匹配条件、MySQL管理工具

#### 04：用户授权、完全备份、增量备份

#### 05：percona软件介绍、innobackupex 备份与恢复



------



### RDBMS2

#### 01：MySQL主从同步、主从同步模式

#### 02：数据读写分离、MySQL多实例

#### 03：数据分片概述、部署MyCAT服务、测试配置

#### 04：MHA集群概述、部署MHA集群

#### 05：PXC、MySQL存储引擎



------



### NOSQL

#### 01：NoSQL概述、部署Redis服务、部署LNMP+Redis

#### 02：穿件集群、管理集群

#### 03：主从复制、持久化、数据类型



------



### PROJECT2

#### 01：项目概述、部署数据库服务mysql、部署共享存储服务NFS、配置网站服务、测试配置、部署监控服务Zabbix

#### 02：项目概述、升级网站运行平台、部署缓存服务、数据迁移、部署集群



------



### CLOUD

#### 01：虚拟化、Virsh管理、公有云基础知识

#### 02：私有云简介、私有云环境准备、搭建私有云

#### 03：私有云管理一、私有云管理二、虚拟化网络、配置云主机、计算机节点扩容

#### 04：华为云服务、华为云管理、华为云案例

#### 05：Linux容器基础、Linux容器管理、Docker命令行

#### 06：自定义镜像、发布容器服务、微服务案例、私有镜像仓库

#### 07：Kubernetes master概述、Kubernetes master、Kubernetes node 

#### 08：Kubernets管理一、Kubernets管理二、控制器与集群调度

#### 09：Kubernetes服务管理、Kubernets存储卷、Kubernetes发布服务ingress

#### 10：Kubernetes dashboard、Kubernets 监控prometheus、 Kubernetes HPA 集群



------



### APCHITECTURE

#### 01：部署Elasticsearch、Elasticsearch、kibana

#### 02：Logstash、Logstash插件、Web日志实时分析

#### 03：Hadoop大数据、部署Hadoop、Hadoop集群

#### 04：数据分析、节点管理、搭建NFS网关服务

#### 05：Zookeeper高可用集群、分布式消息队列Kafka、搭建高可用Hadoop集群



------



### PROJECT3

#### 01：项目概述、跳板机、模板机的制作、企业云平台搭建Web集群

#### 02：MySQL集群、负载均衡及高可用

#### 03：Redis集群部署、Ceph分布式文件系统集群

#### 04：企业监控系统部署、搭建Harbor私有仓库

#### 05：日志分析平台构建



------
# [中文文档翻译网站：一译](http://yiyibooks.cn)

------




# 关于Typora

打开软件后，在上面的菜单栏中找到`主题`，可以选择`Night或其他`

![image-20201116194037216](./README-image/image-01.png)



在菜单栏中找到`文件`，选择`偏好设置`

![image-20201116194255372](./README-image/image-02.png)

在`偏好设置中` 选择`外观`,然后在右边找到`侧边栏`,把对勾打上

![image-20201116194513283](./README-image/image-03.png)

在上面的菜单栏中找到`视图`,把`大纲`打钩



![image-20201116194814018](./README-image/image-04.png)

在左边就可以看到大纲了

![image-20201116193946110](./README-image/image-05.png)



